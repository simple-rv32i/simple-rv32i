--! @file verificationutils.vhd
--! @brief Simulated 36kbits bram memory and led gpo with synchronous wishbone slave interface
--!
--! @author Markus Wagner
--!
--! @copyright Markus Wagner, 2022
--!
--! Permission is hereby granted, free of charge, to any person obtaining a copy
--! of this software and associated documentation files (the "Software"), to
--! deal in the Software without restriction, including without limitation the
--! rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
--! sell copies of the Software, and to permit persons to whom the Software is
--! furnished to do so, subject to the following conditions:
--!
--! The above copyright notice and this permission notice shall be included in
--! all copies or substantial portions of the Software.
--!
--! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
--! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
--! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
--! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
--! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
--! FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
--! IN THE SOFTWARE.
--!
library vunit_lib;
context vunit_lib.vunit_context;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

--! @brief Simulated 36kbits bram memory and led gpo with synchronous wishbone slave interface
--!
--! @details TODO...
entity vcwbmemory is
    generic(
        --! path to memory content string
        loadfile : string
    );
    port(
        rst, clk:   in  std_logic;
        --! wishbone address (byte address)
        adr:        in  std_logic_vector(31 downto 0);
        --! wishbone data in (mosi)
        data_i:     in  std_logic_vector(31 downto 0);
        --! wishbone data out (miso)
        data_o:     out std_logic_vector(31 downto 0);
        --! wishbone write enable
        we:         in  std_logic;
        --! select used bytes on data signals
        sel:        in  std_logic_vector(3 downto 0);
        --! wishbone bus strobe
        stb:        in  std_logic;
        --! wishbone bus acknoledge
        ack:        out std_logic;
        --! wishbone bus cycle
        cyc:        in  std_logic;
        --! export logic led values from gpo registers
        gpo:        out std_logic_vector(3 downto 0)
    );
end vcwbmemory;


architecture behavioral of vcwbmemory is
    type t_memory is array (0 to 4095) of std_logic_vector (7 downto 0);
    type t_gpo_reg is array(0 to 3) of std_logic_vector(7 downto 0);
    signal mem: t_memory;
    signal valid_addr_range, bram_adr: std_logic;
    signal gpo_reg : t_gpo_reg := (others => (others => '0'));
begin
    gpo <= gpo_reg(0)(3 downto 0);
    bram_adr <= valid_addr_range and not adr(12);
    addr_status_flags: process(adr)
    begin
        if adr(31 downto 13) = B"000_0000_0000_0000_0000" then
            valid_addr_range <= '1';
        else
            valid_addr_range <= '0';
        end if;
    end process;

    wbinterface_proc: process(clk)
        file infile : text open read_mode is loadfile;
        variable firstrun : boolean := true;
        variable dataline : line;
        variable read_ok  : boolean;
        variable length   : natural;
        variable byteline : std_logic_vector(7 downto 0);
    begin

        if rising_edge(clk) then
            ack <= '0';
            data_o <= X"XXXXXXXX";
            if rst = '1' then
                gpo_reg <= (others => (others => '0'));
                if firstrun = true then
                    firstrun := false;
                    readline(infile, dataline);
                    read(dataline, length);
                    for pageidx in 0 to length - 1 loop
                        readline(infile, dataline);
                        bread(dataline, byteline);
                        mem(pageidx) <= byteline;
                    end loop;
                end if;
                data_o <= X"UUUUUUUU";
            elsif (stb and cyc) = '1' then
                if we = '1' then
                    if bram_adr then
                        if sel(3) = '1' then
                            mem(to_integer(unsigned(adr)) + 0)(7 downto 0) <= data_i(31 downto 24);
                        end if;
                        if sel(2) = '1' then
                            mem(to_integer(unsigned(adr)) + 1)(7 downto 0) <= data_i(23 downto 16);
                        end if;
                        if sel(1) = '1' then
                            mem(to_integer(unsigned(adr)) + 2)(7 downto 0) <= data_i(15 downto 8);
                        end if;
                        if sel(0) = '1' then
                            mem(to_integer(unsigned(adr)) + 3)(7 downto 0) <= data_i(7 downto 0);
                        end if;
                    elsif valid_addr_range then
                        if sel(3) = '1' then
                            gpo_reg(to_integer(unsigned(adr(1 downto 0))) + 0)(7 downto 0) <= data_i(31 downto 24);
                        end if;
                        if sel(2) = '1' then
                            gpo_reg(to_integer(unsigned(adr(1 downto 0))) + 1)(7 downto 0) <= data_i(23 downto 16);
                        end if;
                        if sel(1) = '1' then
                            gpo_reg(to_integer(unsigned(adr(1 downto 0))) + 2)(7 downto 0) <= data_i(15 downto 8);
                        end if;
                        if sel(0) = '1' then
                            gpo_reg(to_integer(unsigned(adr(1 downto 0))) + 3)(7 downto 0) <= data_i(7 downto 0);
                        end if;
                    end if;
                elsif we = '0' then
                    if bram_adr then
                        if sel(3) = '1' then
                            data_o(31 downto 24) <= mem(to_integer(unsigned(adr)) + 0);
                        end if;
                        if sel(2) = '1' then
                            data_o(23 downto 16) <= mem(to_integer(unsigned(adr)) + 1);
                        end if;
                        if sel(1) = '1' then
                            data_o(15 downto 8) <= mem(to_integer(unsigned(adr)) + 2);
                        end if;
                        if sel(0) = '1' then
                            data_o(7 downto 0) <= mem(to_integer(unsigned(adr)) + 3);
                        end if;
                    elsif valid_addr_range then
                        if sel(3) = '1' then
                            data_o(31 downto 24) <= gpo_reg(to_integer(unsigned(adr(1 downto 0))) + 0);
                        end if;
                        if sel(2) = '1' then
                            data_o(23 downto 16) <= gpo_reg(to_integer(unsigned(adr(1 downto 0))) + 1);
                        end if;
                        if sel(1) = '1' then
                            data_o(15 downto 8) <= gpo_reg(to_integer(unsigned(adr(1 downto 0))) + 2);
                        end if;
                        if sel(0) = '1' then
                            data_o(7 downto 0) <= gpo_reg(to_integer(unsigned(adr(1 downto 0))) + 3);
                        end if;
                    end if;
                else
                    assert false report "invalid we state";
                end if;
                ack <= '1';
            end if;
        end if;
    end process;


end;