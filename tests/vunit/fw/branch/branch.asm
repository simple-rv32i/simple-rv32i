                .text
                .globl main
main:
                # === beq ===
                la a5, 43
                la a7, 43
                beq a5, a7, beqsucc
                j errloop
beqsucc:
                la a5, 52
                beq a5, a7, errloop
                la a5, 42
                beq a5, a7, errloop
                la a5, 53 + 0x100
                beq a5, a7, errloop
                la s2, 0x00000001
                # === bne ===
                la a5, 43
                la a7, 43
                bne a5, a7, errloop
                la a5, 52
                bne a5, a7, bnesucc1
                j errloop
bnesucc1:
                la a5, 42
                bne a5, a7, bnesucc2
                j errloop
bnesucc2:
                la a5, 53 + 0x100
                bne a5, a7, bnesucc3
                j errloop
bnesucc3:
                la s3, 0x00000002
                # === blt ===
                la a4, 123
                la a5, 123
                blt a4, a5, errloop
                blt a5, a4, errloop
                la a4, 122
                blt a5, a4, errloop
                blt a4, a5, bltsucc1
                j errloop
                la a4, -1
                blt a5, a4, errloop
                la a4, -123
                la a5, -123
                blt a4, a5, errloop
                blt a5, a4, errloop
                la a4, -124
                blt a5, a4, errloop
bltsucc1:
                la s4, 0x00000003
                # === bge ===
                la a3, 1000
                la a4, 1000
                bge a3, a4, bgesucc1
                j errloop
bgesucc1:
                la a3, 999
                bge a3, a4, errloop
                bge a4, a3, bgesucc2
                j errloop
bgesucc2:
                la a3, -1
                bge a3, a4, errloop
                bge a4, a3, bgesucc3
                j errloop
bgesucc3:
                la a4, 0
                bge a3, a4, errloop
                la a3, -1962
                la a4, -1962
                bge a3, a4, bgesucc4
                j errloop
bgesucc4:
                la a3, -65530
                bge a3, a4, errloop
                la a4, -2000
                bge a3, a4, errloop
bgesucc5:
                la s5, 0x00000004
                # === bltu ===
                la a3, 0xfffffffc
                la a4, 0xfffffffc
                bltu a3, a4, errloop
                la a3, 0xfffffffd
                bltu a3, a4, errloop
                bltu a4, a3, bltusucc1
                j errloop
bltusucc1:
                la a3, 0xfffffffb
                bltu a4, a3, errloop
                bltu a3, a4, bltusucc2
                j errloop
bltusucc2:
                la a3, 0
                bltu a4, a3, errloop
                bltu a3, a4, bltusucc3
                j errloop
bltusucc3:
                la a4, 12
                bltu a4, a3, errloop
                bltu a3, a4, bltusucc4
                j errloop
bltusucc4:
                la a3, 12
                bltu a3, a4, errloop
                la s6, 0x00000005
                # === bgeu ===
                la a3, 0xfffffff9
                la a4, 0xfffffff9
                bgeu a3, a4, bgeusucc1
                j errloop
bgeusucc1:
                la a3, 0xfffffff8
                bgeu a3, a4, errloop
                bgeu a4, a3, bgeusucc2
                j errloop
bgeusucc2:
                la a3, 0
                bgeu a3, a4, errloop
                la a3, 1024
                bgeu a3, a4, errloop
                bgeu a4, a3, bgeusucc3
                j errloop
bgeusucc3:
                la a4, 12
                bgeu a4, a3, errloop
                bgeu a3, a4, bgeusucc4
                j errloop
bgeusucc4:
                la s7, 0x00000006
endloop:
                j endloop
errloop:
                j errloop
.section .data