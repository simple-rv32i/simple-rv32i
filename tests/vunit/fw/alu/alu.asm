                .text
                .globl main
main:
                # load memory pointers
                la t3, alusrc
                la t4, alu_test_results
                # add 1
                lw a0, 0(t3)
                lw a1, 4(t3)
                add a2, a0, a1
                sw a2, 0(t4)
                # add 2
                lw a0, 8(t3)
                lw a1, 12(t3)
                add a2, a0, a1
                sw a2, 4(t4)
                # add 3
                lw a0, 16(t3)
                lw a1, 20(t3)
                add a2, a0, a1
                sw a2, 8(t4)
                # sub 0
                lw a0, 24(t3)
                lw a1, 28(t3)
                sub a2, a0, a1
                sw a2, 12(t4)
                # xor
                lw a0, 32(t3)
                lw a1, 36(t3)
                xor a2, a0, a1
                sw a2, 16(t4)
                # or
                lw a0, 40(t3)
                lw a1, 44(t3)
                or a2, a0, a1
                sw a2, 20(t4)
                # and
                lw a0, 48(t3)
                lw a1, 52(t3)
                or a2, a0, a1
                sw a2, 24(t4)
                # sll 1
                lw a0, 56(t3)
                lw a1, 60(t3)
                sll a2, a0, a1
                sw a2, 28(t4)
                # sll 2
                lw a0, 64(t3)
                lw a1, 68(t3)
                sll a2, a0, a1
                sw a2, 32(t4)
                # sll 3
                lw a0, 72(t3)
                lw a1, 76(t3)
                sll a2, a0, a1
                sw a2, 36(t4)
                # sll 4
                lw a0, 80(t3)
                lw a1, 84(t3)
                sll a2, a0, a1
                sw a2, 40(t4)
                # srl 1
                lw a0, 88(t3)
                lw a1, 92(t3)
                srl a2, a0, a1
                sw a2, 44(t4)
                # srl 2
                lw a0, 96(t3)
                lw a1, 100(t3)
                srl a2, a0, a1
                sw a2, 48(t4)
                # srl 3
                lw a0, 104(t3)
                lw a1, 108(t3)
                srl a2, a0, a1
                sw a2, 52(t4)
                # srl 4
                lw a0, 112(t3)
                lw a1, 116(t3)
                srl a2, a0, a1
                sw a2, 56(t4)
                # sra 1
                lw a0, 120(t3)
                lw a1, 124(t3)
                sra a2, a0, a1
                sw a2, 60(t4)
                # sra 2
                lw a0, 128(t3)
                lw a1, 132(t3)
                sra a2, a0, a1
                sw a2, 64(t4)
                # sra 3
                lw a0, 136(t3)
                lw a1, 140(t3)
                sra a2, a0, a1
                sw a2, 68(t4)
                # sra 4
                lw a0, 144(t3)
                lw a1, 148(t3)
                sll a2, a0, a1
                sw a2, 72(t4)
                ## restart register index
                la t3, aluldsrc
                la t4, alu_ldtest_results
                # slt 1
                lw a0, 0(t3)
                lw a1, 4(t3)
                slt a2, a0, a1
                sw a2, 0(t4)
                # slt 2
                lw a0, 8(t3)
                lw a1, 12(t3)
                slt a2, a0, a1
                sw a2, 4(t4)
                # slt 3
                lw a0, 16(t3)
                lw a1, 20(t3)
                slt a2, a0, a1
                sw a2, 8(t4)
                # slt 4
                lw a0, 24(t3)
                lw a1, 28(t3)
                slt a2, a0, a1
                sw a2, 12(t4)
                # slt 5
                lw a0, 32(t3)
                lw a1, 36(t3)
                slt a2, a0, a1
                sw a2, 16(t4)
                # slt 6
                lw a0, 40(t3)
                lw a1, 44(t3)
                slt a2, a0, a1
                sw a2, 20(t4)
                # sltu 1
                lw a0, 48(t3)
                lw a1, 52(t3)
                sltu a2, a0, a1
                sw a2, 24(t4)
                # sltu 2
                lw a0, 56(t3)
                lw a1, 60(t3)
                sltu a2, a0, a1
                sw a2, 28(t4)
                # sltu 3
                lw a0, 64(t3)
                lw a1, 68(t3)
                sltu a2, a0, a1
                sw a2, 32(t4)
                # sltu 4
                lw a0, 72(t3)
                lw a1, 76(t3)
                sltu a2, a0, a1
                sw a2, 36(t4)
                # sltu 5
                lw a0, 80(t3)
                lw a1, 84(t3)
                sltu a2, a0, a1
                sw a2, 40(t4)
                # sltu 6
                lw a0, 88(t3)
                lw a1, 92(t3)
                sltu a2, a0, a1
                sw a2, 44(t4)
                
aluend:
                j aluend
.section .data
.global alu_test_results
.global alusrc

alusrc:
                        # add
                        .word 0x00000000, 0x00000000
                        .word 0xFFFFFFFF, 0x00000001
                        .word 0x14000000, 0x42000000
                        # sub
                        .word 0x00000002, 0x00000001
                        # xor
                        .word 0xffaa00aa, 0xffaa0055
                        # and
                        .word 0xffaa00aa, 0xffaa0055
                        # or
                        .word 0xffaa00aa, 0xffaa0055
                        # sll
                        .word 0xff123456, 25
                        .word 0x0000000f, 32
                        .word 0x00123456, 15
                        .word 0x00123456, 0xffffffff
                        # srl
                        .word 0xff123456, 23
                        .word 0xff123456, 0xffffffff
                        .word 0x01245678, 11
                        .word 0x01245678, 0xffffffff
                        # sra
                        .word 0xff123456, 30
                        .word 0xff123456, 0xffffffff
                        .word 0x01245678, 11
                        .word 0x01245678, 0xffffffff
                        # slt
aluldsrc:
                        .word -937, -936
                        .word -936, -936
                        .word -935, -937
                        .word 147, 148
                        .word 148, 148
                        .word 149, 148
                        # sltu
                        .word -937, -936
                        .word -936, -936
                        .word -935, -937
                        .word 147, 148
                        .word 148, 148
                        .word 149, 148
alu_test_results:
                        # add
                        .word 0xffffffff, 0xffffffff, 0xffffffff
                        # sub
                        .word 0xffffffff
                        # xor
                        .word 0xffffffff
                        # or
                        .word 0xffffffff
                        # and
                        .word 0xffffffff
                        # sll
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                        # srl
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                        # sra
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
alu_ldtest_results:
                        # slt
                        .word 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff
                        # sltu
                        .word 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff