                .text
                .globl main
main:
                la a0, results
                lui a1, 0
                sw a1, 0(a0)
                lui a1, 1048575
                sw a1, 4(a0)
                lui a1, 1357
                sw a1, 8(a0)
                # auipc
                auipc a1, 0
                sw a1, 12(a0)
                auipc a1, 75243
                sw a1, 16(a0)
                auipc a1, 1048575
                sw a1,20(a0)

endloop:        j endloop
.section .data
results:
                .word 0xffffffff, 0xffffffff, 0xffffffff
                .word 0xffffffff, 0xffffffff, 0xffffffff