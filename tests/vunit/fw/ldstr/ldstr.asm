                .text
                .globl main
main:
                # load test (check via sw command, if register contained right content)
                la a0, ldsection1
                la a1, strsection1
                li a2, 0
                # lw
                lw a2, 0(a0)
                sw a2, 0(a1)
                # lh
                lh a2, 4(a0)
                sw a2, 4(a1)
                # lhu
                lhu a2, 8(a0)
                sw a2, 8(a1)
                # lb
                lb a2, 12(a0)
                sw a2, 12(a1)
                # lbu
                lbu a2, 16(a0)
                sw a2, 16(a1)
                # load non aligned
                la a0, ldsection1
                la a1, strsection2
                li a2, 0
                # lw
                lw a2, 1(a0)
                sw a2, 1(a1)
                # lh
                lh a2, 5(a0)
                sw a2, 5(a1)
                # lhu
                lhu a2, 9(a0)
                sw a2, 9(a1)
                # lb
                lb a2, 11(a0)
                sw a2, 11(a1)
                # lbu
                lbu a2, 16(a0)
                # store test
                la a1, strsection3
                li a2, 0x12345678
                # sw
                sw a2, 0(a1)
                li a3, 0
                lw a3, 0(a1)
                # sh
                sh a2, 4(a1)
                li a3, 0
                lw a3, 4(a1)
                # sb
                sb a2, 8(a1)
                li a3, 0
                lw a3, 8(a1)
                # store test unaligned
                la a1, strsection4
                li a2, 0x12345678
                # sw
                sw a2, 1(a1)
                li a3, 0
                lw a3, 1(a1)
                # sh
                sh a2, 5(a1)
                li a3, 0
                lw a3, 5(a1)
                # sb
                sb a2, 9(a1)
                li a3, 0
                lw a3, 9(a1)
endloop:        j endloop
.section .data
ldsection1:
                .byte 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08
                .byte 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10
strsection1:
                .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
strsection2:
                .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
strsection3:
                .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
strsection4:
                .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                .byte 0xff