                .text
                .globl main
main:
                # load memory pointers
                la t3, alusrc
                la t4, alu_test_results
                # addi 1
                lw a0, 0(t3)
                addi a2, a0, 0
                sw a2, 0(t4)
                # addi 2
                lw a0, 4(t3)
                addi a2, a0, 1
                sw a2, 4(t4)
                # addi 3
                lw a0, 8(t3)
                addi a2, a0, 12
                sw a2, 8(t4)
                # addi 4
                lw a0, 12(t3)
                addi a2, a0, -12
                sw a2, 12(t4)
                # bitwise ops
                lw a0, 16(t3)
                # xori 1
                xori a2, a0, -1
                sw a2, 16(t4)
                # xori 2
                xori a2, a0, 0x0cb
                sw a2, 20(t4)
                # xori 3
                xori a2, a0, 0x234
                sw a2, 24(t4)
                # ori 1
                ori a2, a0, -1
                sw a2, 28(t4)
                # ori 2
                ori a2, a0, 0x0cb
                sw a2, 32(t4)
                # ori 3
                ori a2, a0, 0x234
                sw a2, 36(t4)
                # andi 1
                andi a2, a0, -1
                sw a2, 40(t4)
                # andi 2
                andi a2, a0, 0x0cb
                sw a2, 44(t4)
                # andi 3
                andi a2, a0, 0x234
                sw a2, 48(t4)
                # === slli ====
                la t3, alu_slli_src
                la t4, alu_slli_res
                # slli 1
                lw a0, 0(t3)
                slli a2, a0, 0
                sw a2, 0(t4)
                # slli 2
                lw a0, 4(t3)
                slli a2, a0, 1
                sw a2, 4(t4)
                # slli 3
                lw a0, 8(t3)
                slli a2, a0, 31
                sw a2, 8(t4)
                # slli 4
                lw a0, 12(t3)
                slli a2, a0, 17
                sw a2, 12(t4)
                # === srli ====
                la t3, alu_srlai_src
                la t4, alu_srli_res
                # srli 1
                lw a0, 0(t3)
                srli a2, a0, 0
                sw a2, 0(t4)
                # srli 2
                lw a0, 4(t3)
                srli a2, a0, 1
                sw a2, 4(t4)
                # srli 3
                lw a0, 8(t3)
                srli a2, a0, 9
                sw a2, 8(t4)
                # srli 4
                lw a0, 12(t3)
                srli a2, a0, 31
                sw a2, 12(t4)
                # srli 5
                lw a0, 16(t3)
                srli a2, a0, 12
                sw a2, 16(t4)
                 # srli 6
                lw a0, 20(t3)
                srli a2, a0, 0
                sw a2, 20(t4)
                # srli 7
                lw a0, 24(t3)
                srli a2, a0, 1
                sw a2, 24(t4)
                # srli 8
                lw a0, 28(t3)
                srli a2, a0, 9
                sw a2, 28(t4)
                # srli 9
                lw a0, 32(t3)
                srli a2, a0, 31
                sw a2, 32(t4)
                # srli 10
                lw a0, 36(t3)
                srli a2, a0, 12
                sw a2, 36(t4)
                # === srai ====
                la t3, alu_srlai_src
                la t4, alu_srai_res
                # srai 1
                lw a0, 0(t3)
                srai a2, a0, 0
                sw a2, 0(t4)
                # srai 2
                lw a0, 4(t3)
                srai a2, a0, 1
                sw a2, 4(t4)
                # srai 3
                lw a0, 8(t3)
                srai a2, a0, 9
                sw a2, 8(t4)
                # srai 4
                lw a0, 12(t3)
                srai a2, a0, 31
                sw a2, 12(t4)
                # srai 5
                lw a0, 16(t3)
                srai a2, a0, 12
                sw a2, 16(t4)
                 # srai 6
                lw a0, 20(t3)
                srai a2, a0, 0
                sw a2, 20(t4)
                # srai 7
                lw a0, 24(t3)
                srai a2, a0, 1
                sw a2, 24(t4)
                # srai 8
                lw a0, 28(t3)
                srai a2, a0, 9
                sw a2, 28(t4)
                # srai 9
                lw a0, 32(t3)
                srai a2, a0, 31
                sw a2, 32(t4)
                # srai 10
                lw a0, 36(t3)
                srai a2, a0, 12
                sw a2, 36(t4)
                # === slti ====
                la t3, alu_slti_src
                la t4, alu_slti_res
                # slti 1
                lw a0, 0(t3)
                slti a2, a0, -1
                sw a2, 0(t4)
                # slti 2
                lw a0, 4(t3)
                slti a2, a0, 0
                sw a2, 4(t4)
                # slti 3
                lw a0, 8(t3)
                slti a2, a0, 1
                sw a2, 8(t4)
                # slti 4
                lw a0, 12(t3)
                slti a2, a0, 10
                sw a2, 12(t4)
                # slti 5
                lw a0, 16(t3)
                slti a2, a0, -29
                sw a2, 16(t4)
                # slti 6
                lw a0, 20(t3)
                slti a2, a0, -30
                sw a2, 20(t4)
                # slti 7
                lw a0, 24(t3)
                slti a2, a0, -31
                sw a2, 24(t4)
                # slti 8
                lw a0, 28(t3)
                slti a2, a0, -40
                sw a2, 28(t4)
                # slti 9
                lw a0, 32(t3)
                slti a2, a0, 29
                sw a2, 32(t4)
                # slti 10
                lw a0, 36(t3)
                slti a2, a0, 30
                sw a2, 36(t4)
                # slti 11
                lw a0, 40(t3)
                slti a2, a0, 31
                sw a2, 40(t4)
                # slti 12
                lw a0, 44(t3)
                slti a2, a0, 128
                sw a2, 44(t4)
                # === sltiu ====
                la t3, alu_sltiu_src
                la t4, alu_sltiu_res
                # sltiu 1
                lw a0, 0(t3)
                sltiu a2, a0, 12
                sw a2, 0(t4)
                # sltiu 2
                lw a0, 4(t3)
                sltiu a2, a0, 0
                sw a2, 4(t4)
                # sltiu 3
                lw a0, 8(t3)
                sltiu a2, a0, 1
                sw a2, 8(t4)
                # sltiu 4
                lw a0, 12(t3)
                sltiu a2, a0, 10
                sw a2, 12(t4)
                # sltiu 5
                lw a0, 16(t3)
                sltiu a2, a0, 39
                sw a2, 16(t4)
                # sltiu 6
                lw a0, 20(t3)
                sltiu a2, a0, 40
                sw a2, 20(t4)
                # sltiu 7
                lw a0, 24(t3)
                sltiu a2, a0, 41
                sw a2, 24(t4)
                # sltiu 8
                lw a0, 28(t3)
                sltiu a2, a0, 128
                sw a2, 28(t4)
aluend:
                j aluend
.section .data
.global alu_test_results
.global alusrc

alusrc:
                        # add
                        .word 0x00000000, 0xffffffff, 42, 42
                        # bitwise ops
                        .word 0xffff1234
alu_slli_src:
                        # slli
                        .word 0xffffffff, 0x789ABCDE, 0xffffffff, 0x789ABCDE
alu_srlai_src:
                        # srli / srai
                        .word 0x87654321, 0x87654321, 0x87654321, 0x87654321, 0x87654321
                        .word 0x78123456, 0x78123456, 0x78123456, 0x78123456, 0x78123456,
alu_slti_src:
                        # slti
                        .word 0x00000000, 0x00000000, 0x00000000, 0x00000000
                        .word -30, -30, -30, -30
                        .word 30, 30, 30, 30
alu_sltiu_src:
                        # slti
                        .word 0x00000000, 0x00000000, 0x00000000, 0x00000000
                        .word 40, 40, 40, 0xfedcba98
alu_test_results:
                        # add
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                        # bitwise ops
                        .word 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff
alu_slli_res:
                        # slli
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
alu_srli_res:
                        # srli
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
alu_srai_res:
                        # srai
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
alu_slti_res:
                        # slti
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
alu_sltiu_res:
                        # sltiu
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                        .word 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff