                .text
                .globl main
main:
                la a7, results
                jal target1
                j errloop
                # dummy instructions
                xori a0, zero, 1
                xori a1, zero, 2
                xori a2, zero, 3
                xori a3, zero, 4
target1:
                li a6, 1
                sw a6, 0(a7)
                la a0, target2
                jalr s2, a0, 0
                j errloop
                # dummy instructions
                xori a0, zero, 1
                xori a1, zero, 2
                xori a2, zero, 3
                xori a3, zero, 4
target2:
                li a6, 2
                sw a6, 4(a7)
                la a0, beforetarget3
                jalr s3, a0, 4
                # dummy instructions
                xori a0, zero, 1
                xori a1, zero, 2
                xori a2, zero, 3
                xori a0, zero, 1
                xori a1, zero, 2
                xori a2, zero, 3
                xori a0, zero, 1
                xori a1, zero, 2
                xori a2, zero, 3
beforetarget3:  xori t0, zero, 0
                j target4
                # dummy instructions
                xori a0, zero, 1
                xori a1, zero, 2
                xori a2, zero, 3
                xori a3, zero, 4
                xori a0, zero, 1
                xori a1, zero, 2
                xori a2, zero, 3
                xori a3, zero, 4
                j errloop
target4:
                li a6, 3
                sw a6, 8(a7)
endloop:        j endloop
errloop:        j errloop

.section .data
results:
                .word 0xffffffff, 0xffffffff, 0xffffffff