puts "cleaning view"
set displaylist [ gtkwave::getDisplayedSignals ]
puts "$displaylist"
set num_deleted [ gtkwave::deleteSignalsFromListIncludingDuplicates $displaylist ]
puts "setting up view"
set label_stimuli [list]
lappend label_stimuli "stimuli"
set num_added [ gtkwave::addCommentTracesFromList $label_stimuli ]
set stimuli_traces [list]
lappend stimuli_traces "top.tb_alu.a"
lappend stimuli_traces "top.tb_alu.b"
lappend stimuli_traces "top.tb_alu.inst"
lappend stimuli_traces "top.tb_alu.secondcycle"
set num_added [ gtkwave::addSignalsFromList $stimuli_traces ]

set label_uut [list]
lappend label_uut "UUT"
set num_added [ gtkwave::addCommentTracesFromList $label_uut ]

set uut_traces [list]
lappend uut_traces "top.tb_alu.branch"
lappend uut_traces "top.tb_alu.y"
set num_added [ gtkwave::addSignalsFromList $uut_traces ]

puts "unzooming time"
set max_time [ gtkwave::getMaxTime ]
puts "$max_time"
gtkwave::setZoomRangeTimes 0 $max_time