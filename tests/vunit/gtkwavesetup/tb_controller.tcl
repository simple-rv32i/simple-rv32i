puts "cleaning view"
set displaylist [ gtkwave::getDisplayedSignals ]
puts "$displaylist"
set num_deleted [ gtkwave::deleteSignalsFromListIncludingDuplicates $displaylist ]
puts "setting up view"
set label_stimuli [list]
lappend label_stimuli "stimuli"
set num_added [ gtkwave::addCommentTracesFromList $label_stimuli ]
set stimuli_traces [list]
lappend stimuli_traces "top.tb_controller.clk"
lappend stimuli_traces "top.tb_controller.rst"
lappend stimuli_traces "top.tb_controller.ack"
lappend stimuli_traces "top.tb_controller.branchflag"
lappend stimuli_traces "top.tb_controller.instruction"
set num_added [ gtkwave::addSignalsFromList $stimuli_traces ]

set label_dut [list]
lappend label_dut "UUT"
set num_added [ gtkwave::addCommentTracesFromList $label_dut ]
set dut_traces [list]

set uut_traces [list]
lappend uut_traces "top.tb_controller.uut.curr_ctrl_state"
lappend uut_traces "top.tb_controller.uut.next_ctrl_state"
lappend uut_traces "top.tb_controller.we"
lappend uut_traces "top.tb_controller.sel"
lappend uut_traces "top.tb_controller.stb"
lappend uut_traces "top.tb_controller.cyc"
lappend uut_traces "top.tb_controller.c_adr_pc_not_alu"
lappend uut_traces "top.tb_controller.c_alu_a_not_pc"
lappend uut_traces "top.tb_controller.c_alu_b"
lappend uut_traces "top.tb_controller.c_alu_pc_not_branch"
lappend uut_traces "top.tb_controller.c_ir_load"
lappend uut_traces "top.tb_controller.c_pc_not_alu"
lappend uut_traces "top.tb_controller.c_regsortmux"
lappend uut_traces "top.tb_controller.ld_reg"
lappend uut_traces "top.tb_controller.pc_load"
set num_added [ gtkwave::addSignalsFromList $uut_traces ]

puts "unzooming time"
set max_time [ gtkwave::getMaxTime ]
puts "$max_time"
gtkwave::setZoomRangeTimes 0 $max_time
