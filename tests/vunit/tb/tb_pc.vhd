library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.verificationutils.all;


entity TB_PC is
  generic (runner_cfg : string);
end entity;


architecture TB of TB_PC is
    signal clk, rst, load: std_logic := '0';
    signal data_in, data_out: std_logic_vector(31 downto 0);
begin
    clk <= not clk after CLK_PERIOD / 2;
    uut: entity work.pc port map(
        rst => rst,
        clk => clk,
        load => load,
        add_in => data_in,
        add_out => data_out
    );
    main: process
    begin
        test_runner_setup(runner, runner_cfg);
        set_stop_level(failure);
        while test_suite loop
            rst <= '1';
            wait_clk_cycles(clk, 1);
            rst <= '0';
            wait_clk_cycles(clk, 1);
            if run("pc_load") then
                check(data_out = X"0000_0000", "wrong reset value");
                data_in <= X"1234_5678";
                wait_clk_cycles(clk, 1);
                load <= '1';
                check(data_out = X"0000_0000", "data should not be load, but are");
                wait_clk_cycles(clk, 1);
                load <= '0';
                check(data_out = X"1234_5678", "data should be load, but are not");
                data_in <= X"FFFF_FFFF";
                wait_clk_cycles(clk, 5);
                check(data_out = X"1234_5678", "data should be kept, but are not");
                load <= '1';
                wait_clk_cycles(clk, 1);
                check(data_out = X"FFFF_FFFC", "data should be load, but are not");
                data_in <= X"0000_0000";
                wait_clk_cycles(clk, 1);
                check(data_out = X"0000_0000", "data should be load, but are not");
                data_in <= X"5555_5555";
                wait_clk_cycles(clk, 1);
                check(data_out = X"5555_5554", "data should be load, but are not");
                data_in <= X"AAAA_AAAA";
                wait_clk_cycles(clk, 1);
                check(data_out = X"AAAA_AAA8", "data should be load, but are not");
                load <= '0';
            elsif run("rst") then
                data_in <= X"FFFF_FFFF";
                load <= '1';
                wait_clk_cycles(clk, 1);
                load <= '0';
                wait_clk_cycles(clk, 3);
                check(data_out = X"FFFF_FFFC", "prepare test failed");
                rst <= '1';
                wait_clk_cycles(clk, 1);
                check(data_out = X"0000_0000", "reset failed");
                wait_clk_cycles(clk, 1);
                check(data_out = X"0000_0000", "reset hold failed");
                rst <= '0';
                wait_clk_cycles(clk, 1);
                check(data_out = X"0000_0000", "after reset failed");
                wait_clk_cycles(clk, 5);
                check(data_out = X"0000_0000", "after reset failed");
            elsif run("rst_load") then
                data_in <= X"89AB_CDEF";
                load <= '1';
                wait_clk_cycles(clk, 1);
                wait_clk_cycles(clk, 3);
                check(data_out = X"89AB_CDEC", "prepare test failed");
                rst <= '1';
                wait_clk_cycles(clk, 1);
                check(data_out = X"0000_0000", "reset failed");
                wait_clk_cycles(clk, 1);
                check(data_out = X"0000_0000", "reset hold failed");
                rst <= '0';
                wait_clk_cycles(clk, 1);
                check(data_out = X"89AB_CDEC", "after reset failed");
                wait_clk_cycles(clk, 5);
                check(data_out = X"89AB_CDEC", "after reset failed");
            end if;
            wait_clk_cycles(clk, 1);
        end loop;
        test_runner_cleanup(runner);
    end process;
end;