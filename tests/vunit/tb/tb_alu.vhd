library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.verificationutils.all;


entity TB_ALU is
	generic (runner_cfg : string);
end entity;


architecture TESTBENCH of TB_ALU is

	component ALU is
		port (
				 a:    in std_logic_vector(31 downto 0);
				 b:    in std_logic_vector(31 downto 0);
				 inst:  in std_logic_vector (16 downto 0);
				 secondCycle: in std_logic;
				 y: out std_logic_vector (31 downto 0);
				 branch:   out std_logic
			 );
	end component;

	-- these 2 constants are not required by vunit itself
	constant CLK_PERIOD : time := 10 ns;
	constant short_delay: time := 0.5 ns;

	-- OP-Code constants
	constant OPC_NIM: std_logic_vector(6 downto 0) := "0110011";
	constant OPC_IM: std_logic_vector(6 downto 0) := "0010011";
	constant OPC_LD: std_logic_vector(6 downto 0) := "0000011";
	constant OPC_STR: std_logic_vector(6 downto 0) := "0100011";
	constant OPC_BR: std_logic_vector(6 downto 0) := "1100011";
	constant OPC_JAL: std_logic_vector(6 downto 0) := "1101111";
	constant OPC_JALR: std_logic_vector(6 downto 0) := "1100111";
	constant OPC_AUIPC: std_logic_vector(6 downto 0) := "0010111";

	-- Not immediat arithmetic instructions: func7 and func3 constants
	constant INST_ADD: std_logic_vector(9 downto 0) := "0000000000"; --0x00 0x0
	constant INST_SUB: std_logic_vector(9 downto 0) := "0100000000"; --0x20 0x0
	constant INST_XOR: std_logic_vector(9 downto 0) := "0000000100"; --0x00 0x4
	constant INST_OR: std_logic_vector(9 downto 0) := "0000000110"; --0x00 0x6
	constant INST_AND: std_logic_vector(9 downto 0) := "0000000111"; --0x00 0x7
	constant INST_SLL: std_logic_vector(9 downto 0) := "0000000001"; --0x00 0x1
	constant INST_SRL: std_logic_vector(9 downto 0) := "0000000101"; --0x00 0x5
	constant INST_SRA: std_logic_vector(9 downto 0) := "0100000101"; --0x20 0x5
	constant INST_SLT: std_logic_vector(9 downto 0) := "0000000010"; --0x00 0x2
	constant INST_SLTU: std_logic_vector(9 downto 0) := "0000000011"; --0x00 0x3

	-- Immediat arithmetic instructions: 
	constant INST_ADDI: std_logic_vector(2 downto 0) := "000"; --0x0
	constant INST_XORI: std_logic_vector(2 downto 0) := "100"; --0x4
	constant INST_ORI: std_logic_vector(2 downto 0) := "110"; --0x6
	constant INST_ANDI: std_logic_vector(2 downto 0) := "111"; --0x7
	constant INST_SLLI: std_logic_vector(9 downto 0) := "0000000001"; --imm[5:11]=0x00, 0x1
	constant INST_SRLI: std_logic_vector(9 downto 0) := "0000000101"; --imm[5:11]=0x00, 0x5
	constant INST_SRAI: std_logic_vector(9 downto 0) := "0100000101"; --imm[5:11]=0x20, 0x5
	constant INST_SLTI: std_logic_vector(2 downto 0) := "010"; -- 0x2
	constant INST_SLTIU: std_logic_vector(2 downto 0) := "011"; --0x3
	constant INST_FILLER7: std_logic_vector(6 downto 0) := "1111111"; --
	constant INST_FILLER3: std_logic_vector(2 downto 0) := "111";

	-- Load instructions
	constant INST_LDB: std_logic_vector(2 downto 0) := "000"; --0x0
	constant INST_LDHW: std_logic_vector(2 downto 0) := "001"; --0x1
	constant INST_LDW: std_logic_vector(2 downto 0) := "010"; --0x2
	constant INST_LDBU: std_logic_vector(2 downto 0) := "100"; --0x4
	constant INST_LDHWU: std_logic_vector(2 downto 0) := "101"; --0x5

	-- Store instructions
	constant INST_STRB: std_logic_vector(2 downto 0) := "000"; -- 0x0
	constant INST_STRHW: std_logic_vector(2 downto 0) := "001"; -- 0x1
	constant INST_STRW: std_logic_vector(2 downto 0) := "010"; --0x2

	-- Branch instructions
	constant INST_BEQ: std_logic_vector(2 downto 0) := "000"; --0x0
	constant INST_BNE: std_logic_vector(2 downto 0) := "001"; --0x1
	constant INST_BLT: std_logic_vector(2 downto 0) := "100"; --0x4
	constant INST_BGE: std_logic_vector(2 downto 0) := "101"; --0x5
	constant INST_BLTU: std_logic_vector(2 downto 0) := "110"; --0x6
	constant INST_BGEU: std_logic_vector(2 downto 0) := "111"; --0x7

	-- Jump instructions
	constant INST_JALR: std_logic_vector(2 downto 0) := "000"; --0x0


	-- clk ist not required by vunit
	signal a : std_logic_vector(31 downto 0);
	signal b : std_logic_vector(31 downto 0);
	signal y: std_logic_vector(31 downto 0);
	signal secondCycle: std_logic;
	signal inst: std_logic_vector(16 downto 0);
	signal branch: std_logic;
	signal expected_value: std_logic_vector(31 downto 0);

	for UUT: ALU use entity WORK.ALU(RTL);

	begin

		UUT: ALU port map(
							 a => a,
							 b => b,
							 inst => inst,
							 secondCycle => secondCycle,
							 branch => branch,
							 y => y
						 );

		-- this process is mandetory
		main: process
		begin
			test_runner_setup(runner, runner_cfg);
			set_stop_level(failure);
			while test_suite loop
				if run("ADD") then
					-- testlogic for ADD instruction
					inst <= INST_ADD & OPC_NIM;
					a <= (31 downto 16 => '0', others => '1');
					wait for short_delay;
					b <= not a;
					expected_value <= (others => '1'); --TODO is there a better way?
					wait for short_delay;
					check(y = expected_value, "add half word lower failed");

					a <= (31 downto 16 => '1', others => '0');
					wait for short_delay;
					b <= not a;
					wait for short_delay;
					check(y = expected_value, "add half word upper failed");

					a <= (0 => '1', others => '0');
					b <= (31 => '0', others => '1');
					expected_value <= (31 => '1', others => '0');
					wait for short_delay;
					check(y = expected_value, "add one to filp msb failed");

					a <= (0 => '1', others => '0');
					b <= (31 => '1', others => '0');
					expected_value <= (31 => '1', 0 => '1', others => '0');
					wait for short_delay;
					check(y = expected_value, "add one to msb failed");

					a <= (0 => '1', others => '0');
					b <= (others => '1');
					expected_value <= (others => '0');
					wait for short_delay;
					check(y = expected_value, "add one to cause overflow failed");

				elsif run("SUB") then
					inst <= INST_SUB & OPC_NIM;
					a <= std_logic_vector(to_signed(16#40000000#,a'length));
					b <= std_logic_vector(to_signed(16#3ABCDEF0#,b'length));
					expected_value <= std_logic_vector(to_signed((16#40000000# - 16#3ABCDEF0#), expected_value'length));
					wait for short_delay;
					check(y = expected_value, "sub two positiv failed");

					a <= std_logic_vector(to_signed(16#400F00F0#,a'length));
					b <= std_logic_vector(to_signed(-16#00BFDEF0#,b'length));
					expected_value <= std_logic_vector(to_signed((16#400F00F0# -(- 16#00BFDEF0#)), expected_value'length));
					wait for short_delay;
					check(y = expected_value, "positiv - negativ failed");	

					a <= std_logic_vector(to_signed(-16#400F00F0#,a'length));
					b <= std_logic_vector(to_signed(16#00BFDEF0#,b'length));
					expected_value <= std_logic_vector(to_signed(((-16#400F00F0#) - 16#00BFDEF0#), expected_value'length));
					wait for short_delay;
					check(y = expected_value, "negativ - positiv failed");

					a <= std_logic_vector(to_signed(-16#000F00F0#,a'length));
					b <= std_logic_vector(to_signed(-16#00BF0EF0#,b'length));
					expected_value <= std_logic_vector(to_signed(((-16#000F00F0#) - (-16#00BF0EF0#)), expected_value'length));
					wait for short_delay;
					check(y = expected_value, "negativ - negativ failed");

				elsif run("XOR") then
					inst <= INST_XOR & OPC_NIM;

					a <= (others => '1');
					b <= (others => '1');
					expected_value <= (others => '0');
					wait for short_delay;
					check(y = expected_value, "a xor a is not 0");

					a <= (others => '0');
					b <= (others => '0');
					expected_value <= (others => '0');
					wait for short_delay;
					check(y = expected_value, "0 xor 0 is not 0");

					a <= std_logic_vector(to_unsigned(16#2AAAAAAA#,a'length)); --msb must not be 1, otherwiese bond check fail
					b <= std_logic_vector(to_unsigned(16#55555555#,b'length));
					expected_value <= (31 => '0',others => '1');
					wait for short_delay;
					check(y = expected_value, "xor to max value failed");

				elsif run("OR") then
					inst <= INST_OR & OPC_NIM;

					a <= (others => '1');
					b <= (others => '0');
					expected_value <= (others => '1');
					wait for short_delay;
					check(y = expected_value, "max or 0 failed");

					a <= (others => '0');
					b <= (others => '0');
					expected_value <= (others => '0');
					wait for short_delay;
					check(y = expected_value, "0 or 0 failed");

				elsif run("AND") then
					inst <= INST_AND & OPC_NIM;

					a <= (others => '1');
					b <= (others => '0');
					expected_value <= (others => '0');
					wait for short_delay;
					check(y = expected_value, "max and 0 failed");

					a <= (others => '1');
					b <= (others => '1');
					expected_value <= (others => '1');
					wait for short_delay;
					check(y = expected_value, "max and max failed");

				elsif run("SLL") then
					inst <= INST_SLL & OPC_NIM;

					a <= std_logic_vector(to_unsigned(16#00000055#,a'length));
					b <= std_logic_vector(to_unsigned(24,b'length));
					expected_value <= std_logic_vector(to_unsigned(16#55000000#,expected_value'length));
					wait for short_delay;
					check(y = expected_value,"shift left logical failed");
				
				elsif run("SRL") then
					inst <= INST_SRL & OPC_NIM;

					a <= std_logic_vector(to_unsigned(16#55000000#,a'length));
					b <= std_logic_vector(to_unsigned(24,b'length));
					expected_value <= std_logic_vector(to_unsigned(16#00000055#,expected_value'length));
					wait for short_delay;
					check(y = expected_value,"shift right logical failed");

				elsif run("SRA") then
					inst <= INST_SRA & OPC_NIM;

					a <= std_logic_vector(to_unsigned(16#55000000#,a'length));
					b <= std_logic_vector(to_unsigned(24,b'length));
					expected_value <= std_logic_vector(to_unsigned(16#00000055#,expected_value'length));
					wait for short_delay;
					check(y = expected_value,"positiv shift right arithmetic failed");

					a <= (31 downto 24 => "10101010", others => '0');
					b <= std_logic_vector(to_unsigned(24,b'length));
					expected_value <= (7 downto 0 => "10101010", others => '1');
					wait for short_delay;
					check(y = expected_value,"negativ shift right arithmetic failed");
				elsif run("SLT") then
					inst <= INST_SLT & OPC_NIM;

					a <= std_logic_vector(to_unsigned(16#1234#,b'length));
					b <= std_logic_vector(to_unsigned(16#ABCD#,a'length));
					expected_value <= std_logic_vector(to_unsigned(1,expected_value'length));
					wait for short_delay;
					check(y = expected_value, "a < b for two positiv failed");

					a <= std_logic_vector(to_unsigned(16#ABCD#,a'length));
					b <= std_logic_vector(to_unsigned(16#1234#,b'length));
					expected_value <= std_logic_vector(to_unsigned(0,expected_value'length));
					wait for short_delay;
					check(y = expected_value, "a > b for two positiv failed");
					
					a <= std_logic_vector(to_signed(-16#ABCD#,a'length));
					b <= std_logic_vector(to_unsigned(16#1234#,b'length));
					expected_value <= std_logic_vector(to_unsigned(1,expected_value'length));
					wait for short_delay;
					check(y = expected_value, "a < b for a = negativ, b = positiv failed");
					
					a <= std_logic_vector(to_unsigned(16#ABCD#,a'length));
					b <= std_logic_vector(to_signed(-16#1234#,b'length));
					expected_value <= std_logic_vector(to_unsigned(0,expected_value'length));
					wait for short_delay;
					check(y = expected_value, "a > b for a = positiv, b = negativ failed");

					a <= std_logic_vector(to_signed(-16#ABCD#,a'length));
					b <= std_logic_vector(to_signed(-16#1234#,b'length));
					expected_value <= std_logic_vector(to_unsigned(1,expected_value'length));
					wait for short_delay;
					check(y = expected_value, "a < b for two negativ failed");

					a <= std_logic_vector(to_signed(-16#1234#,b'length));
					b <= std_logic_vector(to_signed(-16#ABCD#,a'length));
					expected_value <= std_logic_vector(to_unsigned(0,expected_value'length));
					wait for short_delay;
					check(y = expected_value, "a > b for two negative failed");

				elsif run("SLTU") then
					inst <= INST_SLTU & OPC_NIM;

					a <= std_logic_vector(to_signed(-16#1#,a'length));
					b <= std_logic_vector(to_signed(-16#1234#,b'length));
					expected_value <= std_logic_vector(to_unsigned(0,expected_value'length));
					wait for short_delay;
					check(y = expected_value, "a < b for two negativ failed");

				elsif run("ADDI") then
					inst <= INST_FILLER7 & INST_ADDI & OPC_IM;

					a <= (31 downto 12 => '1', others => '0');
					b <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					expected_value <= (others => '1');
					wait for short_delay;
					check(y = expected_value, "add imm to max failed failed");

					a <= std_logic_vector(to_unsigned(16#FFF#, a'length));
					b <= (others => '0');
					expected_value <= std_logic_vector(to_unsigned(16#FFF#, a'length));
					wait for short_delay;
					check(y = expected_value, "add 0 failed");

				elsif run("XORI") then
					inst <= INST_FILLER7 & INST_XORI & OPC_IM;

					a <= std_logic_vector(to_unsigned(16#FFF#, a'length));
					b <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					expected_value <= (others => '0');
					wait for short_delay;
					check(y = expected_value, "xor to 0 failed");

					a <= std_logic_vector(to_unsigned(16#AAA#, a'length));
					b <= std_logic_vector(to_unsigned(16#555#, b'length));
					expected_value <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					wait for short_delay;
					check(y = expected_value, "xor to full failed");

				elsif run("ORI") then
					inst <= INST_FILLER7 & INST_ORI & OPC_IM;

					a <= std_logic_vector(to_unsigned(16#0#, a'length));
					b <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					expected_value <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					wait for short_delay;
					check(y = expected_value, "0 or imm full failed");

					a <= std_logic_vector(to_unsigned(16#FFF#, a'length));
					b <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					expected_value <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					wait for short_delay;
					check(y = expected_value, "full or imm full failed");

					a <= std_logic_vector(to_unsigned(16#AAA#, a'length));
					b <= std_logic_vector(to_unsigned(16#555#, b'length));
					expected_value <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					wait for short_delay;
					check(y = expected_value, "or to full failed");


				elsif run("ANDI") then
					inst <= INST_FILLER7 & INST_ANDI & OPC_IM;

					a <= std_logic_vector(to_unsigned(16#0#, a'length));
					b <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					expected_value <= std_logic_vector(to_unsigned(16#0#, b'length));
					wait for short_delay;
					check(y = expected_value, "0 or imm full failed");

					a <= std_logic_vector(to_unsigned(16#FFF#, a'length));
					b <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					expected_value <= std_logic_vector(to_unsigned(16#FFF#, b'length));
					wait for short_delay;
					check(y = expected_value, "full or imm full failed");

					a <= std_logic_vector(to_unsigned(16#AAA#, a'length));
					b <= std_logic_vector(to_unsigned(16#555#, b'length));
					expected_value <= std_logic_vector(to_unsigned(16#0#, b'length));
					wait for short_delay;
					check(y = expected_value, "or to full failed");

				elsif run ("SLLI") then
					inst <= INST_SLLI & OPC_IM;

					a <= std_logic_vector(to_unsigned(16#DD#, a'length));
					b <= std_logic_vector(to_unsigned(24, b'length));
					expected_value <= (31 downto 24 => X"DD", others => '0');
					wait for short_delay;
					check(y = expected_value, "shift left logical immediate failed");
				elsif run("SRLI") then
					inst <= INST_SRLI & OPC_IM;
					a <= std_logic_vector(to_unsigned(16#77000000#, a'length));
					b <= std_logic_vector(to_unsigned(24, b'length));
					expected_value <= X"0000_0077";
					wait for short_delay;
					check(y = expected_value, "shift right logical immediate failed");

				elsif run ("SRAI") then 
					inst <= INST_SRAI & OPC_IM;

					a <= (31 => '1', others => '0');
					b <= std_logic_vector(to_unsigned(31,b'length));
					expected_value <= (others => '1');
					wait for short_delay;
					check(y = expected_value, "negativ shift failed");
				
					a <= (31 => '0', others => '1');
					b <= std_logic_vector(to_unsigned(30,b'length));
					expected_value <= (0 => '1', others => '0');
					wait for short_delay;
					check(y = expected_value, "positiv shift failed");
					wait for short_delay;

				elsif run ("SLTI") then
					inst <= INST_FILLER7 & INST_SLTI & OPC_IM;

					a <= std_logic_vector(to_unsigned(16#55555555#, a'length));
					b <= std_logic_vector(to_unsigned(16#ABC#, b'length));
					expected_value <= std_logic_vector(to_unsigned(0, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "two positiv where a > b failed");

					a <= std_logic_vector(to_unsigned(16#123#, a'length));
					b <= std_logic_vector(to_unsigned(16#ABC#, b'length));
					expected_value <= std_logic_vector(to_unsigned(1, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "two positiv where a < b failed");

					a <= std_logic_vector(to_signed(-16#55555555#, a'length));
					b <= std_logic_vector(to_unsigned(16#ABC#, b'length));
					expected_value <= std_logic_vector(to_unsigned(1, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "negativ-positiv where a < b failed");

					a <= std_logic_vector(to_unsigned(16#55555555#, a'length));
					b <= std_logic_vector(to_signed(-16#ABC#, b'length));
					expected_value <= std_logic_vector(to_unsigned(0, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "positiv-negativ where a > b failed");

					a <= std_logic_vector(to_signed(-16#55555555#, a'length));
					b <= std_logic_vector(to_signed(-16#ABC#, b'length));
					expected_value <= std_logic_vector(to_unsigned(1, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "two negativ where a < b failed");

					a <= std_logic_vector(to_signed(-16#123#, a'length));
					b <= std_logic_vector(to_signed(-16#ABC#, b'length));
					expected_value <= std_logic_vector(to_unsigned(0, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "two negativ where a > b failed");

				elsif run("SLTIU") then
					inst <= INST_FILLER7 & INST_SLTIU & OPC_IM;

					a <= std_logic_vector(to_unsigned(16#345#,a'length));
					b <= std_logic_vector(to_unsigned(16#DEF#,b'length));	
					expected_value <= std_logic_vector(to_unsigned(1, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "two positiv where a < b failed");

					a <= std_logic_vector(to_unsigned(16#DEF#,a'length));
					b <= std_logic_vector(to_unsigned(16#789#,b'length));	
					expected_value <= std_logic_vector(to_unsigned(0, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "two positiv where a > b failed");

					a <= std_logic_vector(to_signed(-16#321#,a'length));
					b <= std_logic_vector(to_unsigned(16#DEF#,b'length));	
					expected_value <= std_logic_vector(to_unsigned(0, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "negativ-positiv where -a > b failed");

					a <= std_logic_vector(to_unsigned(16#345#,a'length));
					b <= std_logic_vector(to_signed(-16#DEF#,b'length));	
					expected_value <= std_logic_vector(to_unsigned(1, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "positiv-negativ where a < -b failed");

					a <= std_logic_vector(to_signed(-16#1#,a'length));
					b <= std_logic_vector(to_signed(-16#DEF#,b'length));	
					expected_value <= std_logic_vector(to_unsigned(0, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "two negativ where -a > -b failed");
						
					a <= std_logic_vector(to_signed(-16#345#,a'length));
					b <= std_logic_vector(to_signed(-16#1#,b'length));	
					expected_value <= std_logic_vector(to_unsigned(1, expected_value'length));
					wait for short_delay;
					check(y = expected_value, "two negativ where -a < -b failed");

				elsif run ("LDB") then
					inst <= INST_FILLER7 & INST_LDB & OPC_LD;

					a <= X"5555_5000";
					b <= X"0000_0ABC";
					expected_value <= X"5555_5ABC";
					wait for short_delay;
					check(y = expected_value, "add positiv offset failed");

					a <= X"5555_5ABC";
					b <= std_logic_vector(to_signed(-16#ABC#,b'length));
					expected_value <= X"5555_5000";
					wait for short_delay;
					check(y = expected_value, "add negativ offset failed");
				
				elsif run ("LDHW") then
					inst <= INST_FILLER7 & INST_LDHW & OPC_LD;

					a <= X"0123_5123";
					b <= X"0000_0431";
					expected_value <= X"0123_5554";
					wait for short_delay;
					check(y = expected_value, "add positiv offset failed");

					a <= X"0123_5123";
					b <= std_logic_vector(to_signed(-16#123#,b'length));
					expected_value <= X"0123_5000";
					wait for short_delay;
					check(y = expected_value, "add negativ offset failed");

				elsif run("LDW") then
					inst <= INST_FILLER7 & INST_LDW & OPC_LD;

					a <=X"7654_3210";
					b <=X"0000_0321";
					expected_value <= X"7654_3531";
					wait for short_delay;
					check (y = expected_value, "add positiv offset failed");

					a <=X"7313_1140";
					b <= std_logic_vector(to_signed(-16#101#,b'length));
					expected_value <= X"7313_103F";
					wait for short_delay;
					check (y = expected_value, "add negativ offset failed");

				elsif run("LDBU") then
					inst <= INST_FILLER7 & INST_LDBU & OPC_LD;

					a <= X"3FED_8451";
					b <= X"0000_0149";
					expected_value <= X"3FED_859A";
					wait for short_delay;
					check(y = expected_value, "add positiv offset failed");

					a <= X"3FED_8451";
					b <= std_logic_vector(to_signed(-16#241#,b'length));
					expected_value <= X"3FED_8210";
					wait for short_delay;
					check(y = expected_value, "add negativ offset failed");

				elsif run("LDHWU") then
					inst <= INST_FILLER7 & INST_LDHWU & OPC_LD;

					a <= X"0101_2222";
					b <= X"0000_0ACE";
					expected_value <= X"0101_2CF0";
					wait for short_delay;
					check(y = expected_value, "add positiv offset failed");

					a <= X"0000_F000";
					b <= std_logic_vector(to_signed(-16#FFF#, b'length));
					expected_value <= X"0000_E001";
					wait for short_delay;
					check(y = expected_value, "add negativ offset failed");
				
				elsif run("STRB") then
					inst <= INST_FILLER7 & INST_STRB & OPC_STR;

					a <= X"5555_5000";
					b <= X"0000_0ABC";
					expected_value <= X"5555_5ABC";
					wait for short_delay;
					check(y = expected_value, "add positiv offset failed");

					a <= X"5555_5ABC";
					b <= std_logic_vector(to_signed(-16#ABC#,b'length));
					expected_value <= X"5555_5000";
					wait for short_delay;
					check(y = expected_value, "add negativ offset failed");
				
				elsif run ("STRHW") then
					inst <= INST_FILLER7 & INST_STRHW & OPC_STR;

					a <= X"0123_5123";
					b <= X"0000_0431";
					expected_value <= X"0123_5554";
					wait for short_delay;
					check(y = expected_value, "add positiv offset failed");

					a <= X"0123_5123";
					b <= std_logic_vector(to_signed(-16#123#,b'length));
					expected_value <= X"0123_5000";
					wait for short_delay;
					check(y = expected_value, "add negativ offset failed");
				
				elsif run("STRW") then
					inst <= INST_FILLER7 & INST_STRW & OPC_STR;

					a <=X"7654_3210";
					b <=X"0000_0321";
					expected_value <= X"7654_3531";
					wait for short_delay;
					check (y = expected_value, "add positiv offset failed");

					a <=X"7313_1140";
					b <= std_logic_vector(to_signed(-16#101#,b'length));
					expected_value <= X"7313_103F";
					wait for short_delay;
					check (y = expected_value, "add negativ offset failed");

				elsif run("BEQ") then
					inst <= INST_FILLER7 & INST_BEQ & OPC_BR;

					secondCycle <= '1';
					a <= X"ABCD_EF12";
					b <= X"ABCD_EF12";
					wait for short_delay;
					check(branch = '0', "branch not 0 when secCyc 1");
					secondCycle <= '0';
					wait for short_delay;
					check(branch = '1', "branch equal where a=b failed");
					b <= not a;
					wait for short_delay;
					check(branch = '0',"branch equal where a != b failed");

				elsif run("BNE") then
					inst <= INST_FILLER7 & INST_BNE & OPC_BR;

					secondCycle <= '1';
					a <= X"1234_5678";
					b <= not a;
					wait for short_delay;
					check(branch = '0', "branch not 0 when secCyc 1");
					secondCycle <= '0';
					wait for short_delay;
					check(branch = '1', "branch not equal where a != b failed");
					b <= a;
					wait for short_delay;
					check(branch = '0', "branch not equal where a = b failed");

				elsif run("BLT") then
					inst <= INST_FILLER7 & INST_BLT & OPC_BR;

					secondCycle <= '1';
					a <= X"0AAA_AAAA";
					b <= X"0F00_0012";
					wait for short_delay;
					check(branch = '0', "branch not 0 when secCyc 1");
					secondCycle <= '0';
					b <= a;
					wait for short_delay;
					check(branch = '0', "blt where a = b failed");
					b <= X"09AA_A123";
					wait for short_delay;
					check(branch = '0', "blt where a > b failed");
					b <= X"FAAA_A0A0";
					wait for short_delay;
					check(branch = '0', "blt where a > -b failed");
					a <= X"0000_1234";
					b <= X"0102_8973";
					wait for short_delay;
					check(branch = '1', "blt where a < b failed");
					a <= X"FFFF_FFFF";
					wait for short_delay;
					check(branch = '1', "blt where -a < b failed");
					b <= std_logic_vector(to_signed(-16#ABC#, b'length));
					wait for short_delay;
					check(branch = '0', "blt where -a > -b failed");
					a <= std_logic_vector(to_signed(-16#DEF#, a'length));
					wait for short_delay;
					check(branch = '1', "blt where -a < -b failed");

				elsif run("BGE") then
					inst <= INST_FILLER7 & INST_BGE & OPC_BR;

					secondCycle <= '1';
					a <= X"0AAA_AAAA";
					b <= X"0AAA_AAAA";
					wait for short_delay;
					check(branch = '0', "branch not 0 when secCyc 1");
					secondCycle <= '0';
					wait for short_delay;
					check(branch = '1', "bge where a = b failed");
					b <= X"09AA_A123";
					wait for short_delay;
					check(branch = '1', "bge where a > b failed");
					b <= X"FAAA_A0A0";
					wait for short_delay;
					check(branch = '1', "bge where a > -b failed");
					a <= X"0000_1234";
					b <= X"0102_8973";
					wait for short_delay;
					check(branch = '0', "bge where a < b failed");
					a <= X"FFFF_FFFF";
					wait for short_delay;
					check(branch = '0', "bge where -a < b failed");
					b <= std_logic_vector(to_signed(-16#ABC#, b'length));
					wait for short_delay;
					check(branch = '1', "bge where -a > -b failed");
					a <= std_logic_vector(to_signed(-16#DEF#, a'length));
					wait for short_delay;
					check(branch = '0', "bge where -a < -b failed");

				elsif run("BLTU") then
					inst <= INST_FILLER7 & INST_BLTU & OPC_BR;
					
					secondCycle <= '1';
					a <= X"0AAA_AAAA";
					b <= X"0F89_1653";
					wait for short_delay;
					check(branch = '0', "branch not 0 when secCyc 1");
					secondCycle <= '0';
					b <= a;
					wait for short_delay;
					check(branch = '0', "bltu where a = b failed");
					b <= X"09AA_A123";
					wait for short_delay;
					check(branch = '0', "bltu where a > b failed");
					b <= X"FAAA_A0A0";
					wait for short_delay;
					check(branch = '1', "bltu where a < -b failed");
					a <= X"0000_1234";
					b <= X"0102_8973";
					wait for short_delay;
					check(branch = '1', "bltu where a < b failed");
					a <= X"FFFF_FFFF";
					wait for short_delay;
					check(branch = '0', "bltu where -a > b failed");
					b <= std_logic_vector(to_signed(-16#ABC#, b'length));
					wait for short_delay;
					check(branch = '0', "bltu where -a > -b failed");
					a <= std_logic_vector(to_signed(-16#DEF#, a'length));
					b <= X"FFFF_FFFF";
					wait for short_delay;
					check(branch = '1', "bltu where -a < -b failed");

				elsif run("BGEU") then
					inst <= INST_FILLER7 & INST_BGEU & OPC_BR;
					
					secondCycle <= '1';
					a <= X"0AAA_AAAA";
					b <= X"0AAA_AAAA";
					wait for short_delay;
					check(branch = '0', "branch not 0 when secCyc 1");
					secondCycle <= '0';
					wait for short_delay;
					check(branch = '1', "bgeu where a = b failed");
					b <= X"09AA_A123";
					wait for short_delay;
					check(branch = '1', "bgeu where a > b failed");
					b <= X"FAAA_A0A0";
					wait for short_delay;
					check(branch = '0', "bgeu where a < -b failed");
					a <= X"0000_1234";
					b <= X"0102_8973";
					wait for short_delay;
					check(branch = '0', "bgeu where a < b failed");
					a <= X"FFFF_FFFF";
					wait for short_delay;
					check(branch = '1', "bgeu where -a > b failed");
					b <= std_logic_vector(to_signed(-16#ABC#, b'length));
					wait for short_delay;
					check(branch = '1', "bgeu where -a > -b failed");
					a <= std_logic_vector(to_signed(-16#DEF#, a'length));
					b <= X"FFFF_FFFF";
					wait for short_delay;
					check(branch = '0', "bgeu where -a < -b failed");

				elsif run("JAL") then
					inst <= INST_FILLER7 & INST_FILLER3 & OPC_JAL;

					a <= X"0123_4567";
					b <= X"0000_0123";
					expected_value <= X"0123_468A";
					wait for short_delay;
					check(y = expected_value, "add positiv offset failed");

					b <= std_logic_vector(to_signed(-16#567#,b'length));
					expected_value <= X"0123_4000";
					wait for short_delay;
					check(y = expected_value, "add negativ offset failed");

				elsif run("JALR") then
					inst <= INST_FILLER7 & INST_JALR & OPC_JALR;

					a <= X"5FFF_4222";
					b <= X"0000_0DCD";
					expected_value <= X"5FFF_4FEF";
					wait for short_delay;
					check(y = expected_value, "add positiv offset failed");

					b <= std_logic_vector(to_signed(-16#210#,b'length));
					expected_value <= X"5FFF_4012";
					wait for short_delay;
					check(y = expected_value, "add negativ offset failed");

				elsif run("AUIPC") then
					inst <= INST_FILLER7 & INST_FILLER3 & OPC_AUIPC;

					a <= X"0FFF_F000";
					b <= X"5000_0000";
					expected_value <= X"5FFF_F000";
					wait for short_delay;
					check(y = expected_value, "add positiv offset failed");


					a <= X"0FFF_F4CA";
					b <= std_logic_vector(shift_left(to_signed(-16#0_FFFF#,b'length),12));
					expected_value <= X"0000_04CA";
					wait for short_delay;
					check(y = expected_value, "add negativ offset failed");

					a <= X"FF00_1234";
					b <= std_logic_vector(shift_left(to_unsigned(16#00CF_4#,b'length),12));
					expected_value <= X"FFCF_5234";
					wait for short_delay;
					check(y = expected_value, "test if pc is unsigned; add positiv offset");

					b <= std_logic_vector(shift_left(to_signed(-16#FF00_1#,b'length),12));
					expected_value <= X"0000_0234";
					wait for short_delay;
					check(y = expected_value, "test if pc is unsigned; add negativ offset");

				end if;
			end loop;
			test_runner_cleanup(runner); -- Simulation ends here
		end process;
end architecture;
