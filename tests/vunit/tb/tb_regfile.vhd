library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity TB_REGFILE is
  generic (runner_cfg : string);
end entity;


architecture TESTBENCH of TB_REGFILE is
    constant CLK_PERIOD : time := 10 ns;
    constant short_delay: time := 0.5 ns;

    signal clk : std_logic := '0';
    signal sel_rd, sel_rs1, sel_rs2: std_logic_vector(4 downto 0) := (others => '0');
    signal data_in: std_logic_vector(31 downto 0) := (others => '0');
    signal load: std_logic := '0';
    signal rs1, rs2: std_logic_vector(31 downto 0);

    component REGFILE is
        port (
    	clk: in std_logic;
    	sel_rd: in std_logic_vector (4 downto 0);   
    	sel_rs1: in std_logic_vector (4 downto 0);    
    	sel_rs2: in std_logic_vector (4 downto 0);      
    	data_in: in std_logic_vector (31 downto 0);      
    	load: in std_logic;                               	    
    	rs1: out std_logic_vector (31 downto 0);       
    	rs2: out std_logic_vector (31 downto 0)         
        );
    end component;

    for DUT: REGFILE use entity WORK.REGFILE(RTL);
    
    procedure wait_clk_cycles(signal clk_in: in std_logic; n: in natural) is
    begin
        for index in 0 to n - 1 loop
            wait until rising_edge(clk_in);
        end loop;
    end procedure;
    
begin
    DUT: REGFILE port map(        
        clk => clk,
    	sel_rd  => sel_rd,
    	sel_rs1 => sel_rs1,
    	sel_rs2 => sel_rs2,    
    	data_in => data_in,    
    	load => load,            	    
    	rs1 => rs1,      
    	rs2 => rs2
    );
    clk <= not clk after CLK_PERIOD / 2;
    main: process
    begin
        test_runner_setup(runner, runner_cfg);
        set_stop_level(failure);
        while test_suite loop
            data_in <= X"00000000";
            sel_rd <= "00000";
            sel_rs1 <= "00000";
            sel_rs2 <= "00000";
            load <= '0';
            wait_clk_cycles(clk, 4);
            if run("load_test") then
                sel_rd <= "00001";
                sel_rs1 <= "00001";
                sel_rs2 <= "00001";
            	load <= '1';
            	wait until rising_edge(clk);
            	wait for short_delay;
            	check(rs1 = X"00000000" and rs1 = X"00000000", "load_test: load zero failed");
            	load <= '0';
            	wait until rising_edge(clk);
            	wait for short_delay;
            	data_in <= X"0000008E";
            	wait until rising_edge(clk);
            	wait for short_delay;
            	check(rs1 = X"00000000" and rs1 = X"00000000", "load_test: not load failed");
            	wait until rising_edge(clk);
            	wait for short_delay;
            	load <= '1';
            	wait until rising_edge(clk);
            	wait for short_delay;
            	check(rs1 = X"0000008E" and rs1 = X"0000008E", "load_test: load valuefailed");
				wait until rising_edge(clk);
            	wait for short_delay;
            	load <= '0';
				wait until rising_edge(clk);
            elsif run("load_mx") then 
            	load <= '1';
            	wait until rising_edge(clk);
            	wait for short_delay;
            	check(rs1 = X"00000000", "load_mx: load zero failed");
				for index_rd in 0 to 31 loop
                    load <= '1';
                    data_in <= X"00000000";
                    sel_rd <= std_logic_vector(to_unsigned(index_rd,5));
                    wait until rising_edge(clk);
                end loop;
				for index_rs in 0 to 31 loop
					sel_rs1 <= std_logic_vector(to_unsigned(index_rs,5));
					sel_rs2 <= std_logic_vector(to_unsigned(index_rs,5));
					wait for short_delay;
					check(rs1 = X"00000000" and rs2 = X"00000000", "register init failed");
				end loop;
				wait until rising_edge(clk);
				sel_rd <= "01010";
                sel_rs1 <= "01010";
                data_in <= X"00AFFE00";
                wait until rising_edge(clk);
				wait for short_delay;
				check(rs1 = X"00AFFE00", "test1 AFFE failed");
                sel_rd <= "00011";
                sel_rs2 <= "00011";
                data_in <= X"00BEEF00";
                wait until rising_edge(clk);
				wait for short_delay;
				check(rs1 = X"00AFFE00", "test1 BEEF failed");
				sel_rd <= "10010";
                sel_rs1 <= "10010";
                data_in <= X"00BEEF00";
                wait until rising_edge(clk);
				wait for short_delay;
				check(rs1 = X"00BEEF00", "test2 BEEF failed");
                sel_rd <= "11011";
                sel_rs2 <= "11011";
                data_in <= X"00AFFE00";
                wait until rising_edge(clk);
                wait for short_delay;
				check(rs2 = X"00AFFE00", "test2 AFFE failed");     	
                check(rs1 = X"00BEEF00", "test2 independend BEEF failed");
            elsif run("reg_x0") then
                sel_rs1 <= "00000";
                wait_clk_cycles(clk, 1);
                check(rs1 = X"0000_0000", "x0 not 0 at startup");
                sel_rd <= "00000";
                data_in <= X"1234_5678";
                wait_clk_cycles(clk, 1);
                check(rs1 = X"0000_0000", "x0 should not be writable");
            end if;
        end loop;
        test_runner_cleanup(runner);
    end process;
end architecture;
