library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.verificationutils.all;


entity TB_ADD4 is
	generic (runner_cfg : string);
end entity;

architecture TESTBENCH of TB_ADD4 is
	-- these 2 constants are not required by vunit itself
	--constant CLK_PERIOD : time := 10 ns;
	constant short_delay: time := 4 ns;
	--		constant address_length: integer:= 32;

	-- clk ist not required by vunit
	--signal clk : std_logic := '0';
	--signal stimulus1, stimulus2: std_logic := 'X';
	--signal signal_under_test: std_logic;

	component ADD4 is
		port (
				 pc_in:  in std_logic_vector (31 downto 0);
				 add_out: out std_logic_vector (31 downto 0)
			 );
	end component;

	signal pc_in: std_logic_vector(31 downto 0);
	signal add_out: std_logic_vector(31 downto 0);
	signal test: std_logic_vector(31 downto 0);

	for UUT: ADD4 use entity WORK.ADD4(RTL); 

	begin
		UUT: ADD4 port map(
							  pc_in => pc_in,
							  add_out => add_out
						  );
		-- this process is mandetory
		main: process
		begin
			test_runner_setup(runner, runner_cfg);
			set_stop_level(failure);
			while test_suite loop
				if run("test_first_inc") then
					-- testlogic and checks of 1st testcase
					pc_in <= (others => '0');
					wait for short_delay;
					check(add_out = std_logic_vector(to_unsigned(4,add_out'length)), "First increment failed");
					wait for short_delay;
				elsif run("second_test") then
					pc_in <= (2 => '1', others => '0');
					wait for short_delay;
					check(add_out = std_logic_vector(to_unsigned(8,add_out'length)), "Second failed");
					wait for short_delay;
				elsif run("test_last_inc") then
					info("add_out: " & integer'image(add_out'length));
					pc_in <= (2 => '0', others => '1');
					test <= (others => '1');
					wait for short_delay;
					check(add_out = test, "Second failed");
					wait for short_delay;
				end if;
			end loop;
			test_runner_cleanup(runner); -- Simulation ends here
		end process;
end architecture;
