library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.verificationutils.all;
use work.rv32i.all;
use work.simplerv32i_package.all;


entity TB_REGSORTMUX is
  generic (runner_cfg : string);
end entity;


architecture TB of TB_REGSORTMUX is
    signal data, instr, pc, alu, result: std_logic_vector(31 downto 0);
    signal sel: regsortmux_sel;
begin
    uut: entity work.regsortmux port map(
        sel => sel,
        wb_data => data,
        instr => instr,
        pc => pc,
        alu => alu,
        outword => result
    );
    main: process
        procedure check_mux(
            stim_sel: regsortmux_sel;
            expected_res: std_logic_vector(31 downto 0)
        ) is
        begin
            sel <= stim_sel;
            wait for 0.5 ns;
            check(result = expected_res, "wrong result");
        end procedure;
    begin
        test_runner_setup(runner, runner_cfg);
        set_stop_level(failure);
        while test_suite loop
            wait for CLK_PERIOD;
            if run("selalu") then
                alu <= X"12_34_56_78";
                check_mux(rsm_sel_alu, X"12_34_56_78");
            elsif run("sel_lui") then
                instr <= X"FF_FF_FF_FF";
                check_mux(rsm_sel_lui, X"FF_FF_F0_00");
                instr <= X"12_34_56_78";
                check_mux(rsm_sel_lui, X"12_34_50_00");
            elsif run("sel_ld_w") then
                data <= X"12_34_56_78";
                check_mux(rsm_sel_ld_w, X"78_56_34_12");
            elsif run("sel_ld_h") then
                data <= X"12_C4_56_78";
                check_mux(rsm_sel_ld_h, X"FF_FF_C4_12");
                data <= X"12_34_56_78";
                check_mux(rsm_sel_ld_h, X"00_00_34_12");
            elsif run("sel_ld_b") then
                data <= X"12_34_56_78";
                check_mux(rsm_sel_ld_b, X"00_00_00_12");
                data <= X"C2_34_56_78";
                check_mux(rsm_sel_ld_b, X"FF_FF_FF_C2");
            elsif run("sel_ld_hu") then
                data <= X"12_C4_56_78";
                check_mux(rsm_sel_ld_hu, X"00_00_C4_12");
                data <= X"12_34_56_78";
                check_mux(rsm_sel_ld_hu, X"00_00_34_12");
            elsif run("sel_ld_bu") then
                data <= X"12_34_56_78";
                check_mux(rsm_sel_ld_bu, X"00_00_00_12");
                data <= X"C2_34_56_78";
                check_mux(rsm_sel_ld_bu, X"00_00_00_C2");
            end if;
        end loop;
        wait for CLK_PERIOD;
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;