library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity TB_IR is
  generic (runner_cfg : string);
end entity;


architecture TESTBENCH of TB_IR is

    component IR is
        port (
            clk:    in std_logic;
            load:   in std_logic;
            ir_in:  in std_logic_vector (31 downto 0);
            ir_out: out std_logic_vector (31 downto 0)
        );
    end component;
    
    constant CLK_PERIOD : time := 10 ns;
    constant small_delay: time := 0.5 ns;
    
    signal clk : std_logic := '0';
    signal load: std_logic := '0';
    signal ir_in, ir_out: std_logic_vector(31 downto 0);

    for DUT: IR use entity WORK.IR(RTL);
    
    procedure wait_clk_cycles(signal clk_in: in std_logic; n: in natural) is
    
    begin
        for idx in 0 to n - 1 loop
            wait until rising_edge(clk_in);
        end loop;
    end procedure;

begin
    clk <= not clk after CLK_PERIOD / 2;
    
    DUT: IR port map(
        clk => clk,
        load => load,
        ir_in => ir_in,
        ir_out => ir_out
    );

    testrunner: process
    begin
        test_runner_setup(runner, runner_cfg);
        while test_suite loop
            if run("load") then
                wait until rising_edge(clk);
                ir_in <= X"00000000";
                load <= '1';
                wait until rising_edge(clk);
                check(ir_out = X"00000000", "set to zero failed");
                ir_in <= X"0000BEEF";
                wait until rising_edge(clk);
                load <= '0';
                wait for small_delay;
                check(ir_out = X"0000BEEF", "set value failed");
                info("load: " & to_string(load));
            elsif run("hold") then
                wait until rising_edge(clk);
                ir_in <= X"00000000";
                load <= '1';
                wait until rising_edge(clk);
                wait for small_delay;
                check(ir_out = X"00000000", "set to zero failed");
                ir_in <= X"0000BEEF";
                wait until rising_edge(clk);
                wait for small_delay;
                check(ir_out = X"0000BEEF", "set value failed");
                load <= '0';
                wait until rising_edge(clk);
                wait for small_delay;
                check(ir_out = X"0000BEEF", "hold value failed");
                ir_in <= X"0000AFFE";
                wait_clk_cycles(clk, 20);
                wait for small_delay;
                check(ir_out = X"0000BEEF", "hold value for multiple cycles failed");
            elsif run("multiple_load_hold") then
                wait until rising_edge(clk);
                ir_in <= X"00000000";
                load <= '1';
                wait until rising_edge(clk);
                wait for small_delay;
                check(ir_out = X"00000000", "set to zero failed");
                ir_in <= X"0000BEEF";
                wait until rising_edge(clk);
                wait for small_delay;
                check(ir_out = X"0000BEEF", "set value failed");
                load <= '0';
                wait until rising_edge(clk);
                wait for small_delay;
                check(ir_out = X"0000BEEF", "hold value failed");
                ir_in <= X"0000AFFE";
                wait_clk_cycles(clk, 20);
                wait for small_delay;
                check(ir_out = X"0000BEEF", "hold value for multiple cycles failed");
                load <= '1';
                wait until rising_edge(clk);
                wait for small_delay;
                check(ir_out = X"0000AFFE", "new value failed");
                wait_clk_cycles(clk, 7);
                wait for small_delay;
                check(ir_out = X"0000AFFE", "load on failed");
                wait until rising_edge(clk);
            end if;
        end loop;
        test_runner_cleanup(runner);
    end process;
end architecture;

