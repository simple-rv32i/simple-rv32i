library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.verificationutils.all;


entity TB_DEMO is
  generic (runner_cfg : string);
end entity;


architecture TESTBENCH of TB_DEMO is
    -- these 2 constants are not required by vunit itself
    constant CLK_PERIOD : time := 10 ns;
    constant short_delay: time := 0.5 ns;

    -- clk ist not required by vunit
    signal clk : std_logic := '0';
    signal stimulus1, stimulus2: std_logic := 'X';
    signal signal_under_test: std_logic;
begin
    -- clock is not required
    clk <= not clk after CLK_PERIOD / 2;
    dut_simulation_process: process(clk)
    begin
        if rising_edge(clk) then
            signal_under_test <= stimulus1 xor stimulus2;
        end if;
    end process;
    -- this process is mandetory
    main: process
    begin
        test_runner_setup(runner, runner_cfg);
        while test_suite loop
            if run("testcase_1") then
                -- testlogic and checks of 1st testcase
                stimulus1 <= '0';
                stimulus2 <= '0';
                wait_clk_cycles(clk, 1);
                check(signal_under_test = '0', "xor operation failed");
                wait_clk_cycles(clk, 1);
            elsif run("testcase_2") then
                -- testlogic and checks of 2nd testcase
                wait_clk_cycles(clk, 1);
                stimulus1 <= '0';
                wait_clk_cycles(clk, 1);
                stimulus2 <= '0';
                wait_clk_cycles(clk, 1);
                check(signal_under_test = '0', "1) xor operation failed");
                stimulus1 <= '1';
                wait for 0.1 ns;
                check(signal_under_test = '0', "2) xor not a synchronous operation");
                wait_clk_cycles(clk, 1);
                check(signal_under_test = '1', "3) xor operation failed");
                stimulus1 <= '0';
                stimulus2 <= '1';
                wait_clk_cycles(clk, 1);
                check(signal_under_test = '1', "4) xor operation failed");
                stimulus1 <= '1';
                stimulus2 <= '1';
                wait_clk_cycles(clk, 1);
                check(signal_under_test = '0', "5) xor operation failed");
                wait_clk_cycles(clk, 2);
            end if;
        end loop;
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;
