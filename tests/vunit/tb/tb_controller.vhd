library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.verificationutils.all;
use work.rv32i.all;
use work.simplerv32i_package.all;


entity TB_CONTROLLER is
  generic (runner_cfg : string);
end entity;


architecture TESTBENCH of TB_CONTROLLER is
    type instr_array is array (positive range <>) of std_logic_vector(31 downto 0);
    type sel_array is array (positive range <>) of std_logic_vector(3 downto 0);
    type alub_sel_array is array (positive range <>) of alub_sel;
    type regsortmux_sel_array is array(positive range <>) of regsortmux_sel;
    -- clk ist not required by vunit
    signal clk, rst, branchflag, ack : std_logic := '0';
    signal instruction: std_logic_vector(31 downto 0);
    signal we, stb, cyc, c_adr_pc_not_alu, c_alu_a_not_pc, c_alu_pc_not_branch,
        c_ir_load, c_pc_not_alu, ld_reg, pc_load: std_logic;
    signal c_alu_b: alub_sel;
    signal c_regsortmux: regsortmux_sel;
    signal sel : std_logic_vector(3 downto 0);
begin
    uut: entity work.controller port map(
        clk => clk,
        rst => rst,
        instr => instruction,
        branchflag => branchflag,
        wb_we => we,
        wb_sel => sel,
        wb_stb => stb,
        wb_cyc => cyc,
        wb_ack => ack,
        c_adr_pc_not_alu => c_adr_pc_not_alu,
        c_alu_a_not_pc => c_alu_a_not_pc,
        c_alu_b => c_alu_b,
        c_alu_pc_not_branch => c_alu_pc_not_branch,
        c_ir_load => c_ir_load,
        c_pc_not_alu => c_pc_not_alu,
        c_regsortmux => c_regsortmux,
        ld_reg => ld_reg,
        pc_load => pc_load
    );
    clk <= not clk after CLK_PERIOD / 2;
    main: process
        procedure run_controller_cycle (
            cycles: natural;
            stim_instruction: instr_array;
            stim_branchflag: std_logic_vector;
            stim_ack: std_logic_vector;
            assert_we: std_logic_vector;
            assert_sel: sel_array;
            assert_stb: std_logic_vector;
            assert_cyc: std_logic_vector;
            assert_c_adr_pc_not_alu: std_logic_vector;
            assert_c_alu_a_not_pc: std_logic_vector;
            check_c_alu_b: std_logic_vector;
            assert_c_alu_b: alub_sel_array;
            assert_c_alu_pc_not_branch: std_logic_vector;
            assert_c_ir_load: std_logic_vector;
            assert_c_pc_not_alu: std_logic_vector;
            check_c_regsortmux: std_logic_vector;
            assert_c_regsortmux: regsortmux_sel_array;
            assert_ld_reg: std_logic_vector;
            assert_pc_load: std_logic_vector
        ) is
        begin
            for idx in 0 to cycles - 1 loop
                info("===== check cycle " & integer'image(idx) & " =====");
                ack <= stim_ack(idx);
                branchflag <= stim_branchflag(idx);
                instruction <= stim_instruction(idx + 1);
                wait until rising_edge(clk);
                if assert_we(idx) /= 'X' then
                    check(assert_we(idx) = we, "wrong we");
                else
                    info("ignoring we in this cycle");
                end if;
                for inner_idx in 0 to 3 loop
                    if assert_sel(idx + 1)(inner_idx) /= 'X' then
                        check(assert_sel(idx + 1)(inner_idx) = sel(inner_idx), "wrong sel(");
                    else
                        info("ignoring sel(" & natural'image(inner_idx) & ") in this cycle");
                    end if;
                end loop;
                -- TODO: WB_SEL, INSTRUCTION
                if assert_stb(idx) /= 'X' then
                    check(assert_stb(idx) = stb, "wrong stb, should be " & std_logic'image(assert_stb(idx)) & ", but is " & std_logic'image(stb));
                else
                    info("ignoring stb in this cycle");
                end if;
                if assert_cyc(idx) /= 'X' then
                    check(assert_cyc(idx) = cyc, "wrong cyc, should be " & std_logic'image(assert_cyc(idx)) & ", but is " & std_logic'image(cyc));
                else
                    info("ignoring cyc in this cycle");
                end if;
                if assert_c_adr_pc_not_alu(idx) /= 'X' then
                    check(assert_c_adr_pc_not_alu(idx) = c_adr_pc_not_alu, "wrong c_adr_pc_not_alu, should be " & std_logic'image(assert_c_adr_pc_not_alu(idx)) & ", but is " & std_logic'image(c_adr_pc_not_alu));
                else
                    info("ignoring c_adr_pc_not_alu in this cycle");
                end if;
                if assert_c_alu_a_not_pc(idx) /= 'X' then
                    check(assert_c_alu_a_not_pc(idx) = c_alu_a_not_pc, "wrong c_alu_a_not_pc, should be " & std_logic'image(assert_c_alu_a_not_pc(idx)) & ", but is " & std_logic'image(c_alu_a_not_pc));
                else
                    info("ignoring c_alu_a_not_pc in this cycle");
                end if;
                if check_c_alu_b(idx) = '1' then
                    check(assert_c_alu_b(idx + 1) = c_alu_b, "wrong c_alu_b");
                else
                    info("ignoring c_alu_b in this cycle");
                end if;
                if assert_c_alu_pc_not_branch(idx) /= 'X' then
                    check(assert_c_alu_pc_not_branch(idx) = c_alu_pc_not_branch, "wrong c_alu_pc_not_branch, should be " & std_logic'image(assert_c_alu_pc_not_branch(idx)) & ", but is " & std_logic'image(c_alu_pc_not_branch));
                else
                    info("ignoring c_alu_pc_not_branch in this cycle");
                end if;
                if assert_c_ir_load(idx) /= 'X' then
                    check(assert_c_ir_load(idx) = c_ir_load, "wrong c_ir_load, should be " & std_logic'image(assert_c_ir_load(idx)) & ", but is " & std_logic'image(c_ir_load));
                else
                    info("ignoring c_ir_load in this cycle");
                end if;
                if assert_c_pc_not_alu(idx) /= 'X' then
                    check(assert_c_pc_not_alu(idx) = c_pc_not_alu, "wrong c_pc_not_alu, should be " & std_logic'image(assert_c_pc_not_alu(idx)) & ", but is " & std_logic'image(c_pc_not_alu));
                else
                    info("ignoring c_pc_not_alu in this cycle");
                end if;
                if check_c_regsortmux(idx) = '1' then
                    check(assert_c_regsortmux(idx + 1) = c_regsortmux, "wrong c_regsortmux");
                else
                    info("ignoring c_regsortmux in this cycle");
                end if;
                if assert_ld_reg(idx) /= 'X' then
                    check(assert_ld_reg(idx) = ld_reg, "wrong ld_reg, should be" & std_logic'image(assert_ld_reg(idx)) & "but is " & std_logic'image(ld_reg));
                else
                    info("ignoring ld_reg in this cycle");
                end if;
                if assert_pc_load(idx) /= 'X' then
                    check(assert_pc_load(idx) = pc_load, "wrong pc_load, should be " & std_logic'image(assert_pc_load(idx)) & ", but is " & std_logic'image(pc_load));
                else
                    info("ignoring pc_load in this cycle");
                end if;
                wait until falling_edge(clk);
            end loop;
            info("===== end controller check procedure =====");
        end procedure;
        variable tc_instr: std_logic_vector(31 downto 0);
    begin
        test_runner_setup(runner, runner_cfg);
        set_stop_level(failure);
        while test_suite loop
            if run("fetch_instr") then
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    2,
                    -- === stimuli ===
                    -- instruction
                    (X"XXXX_XXXX", X"0000_0000", X"0000_0000"),
                    "00",        -- branchflag
                    "01",        -- ack
                    -- === assert values ===
                    "00",        -- we
                    -- sel
                    ("1111", "1111"),
                    "11",        -- stb
                    "11",        -- cyc
                    "11",        -- c_adr_pc_not_alu
                    "XX",        -- c_alu_a_not_pc
                    "00",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2),
                    "XX",        -- c_alu_pc_not_branch
                    "01",        -- c_ir_load
                    "XX",        -- c_pc_not_alu
                    "00",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu),
                    "00",        -- ld_reg
                    "00"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("alu") then
                tc_instr := FUNC7_ADD & "01000" & "00000" & FUNC3_ADD & "00010" & OPC_ALU;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    5,
                    -- === stimuli ===
                    -- instruction
                    (X"XXXX_XXXX", tc_instr, tc_instr, X"XXXX_XXXX", X"XXXX_XXXX", tc_instr),
                    "XXXXX",        -- branchflag
                    "01000",        -- ack
                    -- === assert values ===
                    "00000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "XXXX", "1111"),
                    "11001",        -- stb
                    "11001",        -- cyc
                    "11XX1",        -- c_adr_pc_not_alu
                    "XXX1X",        -- c_alu_a_not_pc
                    "00010",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_rs2),
                    "XXXXX",        -- c_alu_pc_not_branch
                    "01000",        -- c_ir_load
                    "XXX1X",        -- c_pc_not_alu
                    "00010",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu),
                    "00010",        -- ld_reg
                    "00010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("alui") then
                tc_instr := B"0100_0101_1111" & "00000" & FUNC3_ADD & "00010" & OPC_ALUI;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    5,
                    -- === stimuli ===
                    -- instruction
                    (X"XXXX_XXXX", tc_instr, tc_instr, X"XXXX_XXXX", X"XXXX_XXXX", tc_instr),
                    "XXXXX",        -- branchflag
                    "01000",        -- ack
                    -- === assert values ===
                    "00000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "XXXX", "1111"),
                    "11001",        -- stb
                    "11001",        -- cyc
                    "11XX1",        -- c_adr_pc_not_alu
                    "XXX1X",        -- c_alu_a_not_pc
                    "00010",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_i, alub_sel_rs2),
                    "XXXXX",        -- c_alu_pc_not_branch
                    "01000",        -- c_ir_load
                    "XXX1X",        -- c_pc_not_alu
                    "00010",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu),
                    "00010",        -- ld_reg
                    "00010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("load_w") then
                tc_instr := FUNC7_ADD & "01000" & "00000" & FUNC3_LW & "00010" & OPC_LOAD;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    7,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, X"XXXX_XXXX",X"XXXX_XXXX",X"XXXX_XXXX", tc_instr),
                    "XXXXXXX",        -- branchflag
                    "0100100",        -- ack
                    -- === assert values ===
                    "0000000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "1111", "1111", "XXXX", "1111"),
                    "1101101",        -- stb
                    "1101101",        -- cyc
                    "11X00X1",        -- c_adr_pc_not_alu
                    "XXX11XX",        -- c_alu_a_not_pc
                    "0001100",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_i, alub_sel_imm_i, alub_sel_rs2),
                    "XXXXXXX",        -- c_alu_pc_not_branch
                    "0100000",        -- c_ir_load
                    "XXXXX1X",        -- c_pc_not_alu
                    "0001100",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_ld_w, rsm_sel_ld_w, rsm_sel_alu, rsm_sel_alu),
                    "0000100",        -- ld_reg
                    "0000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("load_h") then
                tc_instr := FUNC7_ADD & "01000" & "00000" & FUNC3_LH & "00010" & OPC_LOAD;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    7,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, X"XXXX_XXXX",X"XXXX_XXXX",X"XXXX_XXXX", tc_instr),
                    "XXXXXXX",        -- branchflag
                    "0100100",        -- ack
                    -- === assert values ===
                    "0000000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "11XX", "11XX", "XXXX", "1111"),
                    "1101101",        -- stb
                    "1101101",        -- cyc
                    "11X00X1",        -- c_adr_pc_not_alu
                    "XXX11XX",        -- c_alu_a_not_pc
                    "0001100",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_i, alub_sel_imm_i, alub_sel_rs2),
                    "XXXXXXX",        -- c_alu_pc_not_branch
                    "0100000",        -- c_ir_load
                    "XXXXX1X",        -- c_pc_not_alu
                    "0001100",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_ld_h, rsm_sel_ld_h, rsm_sel_alu, rsm_sel_alu),
                    "0000100",        -- ld_reg
                    "0000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("load_hu") then
                tc_instr := FUNC7_ADD & "01000" & "00000" & FUNC3_LHU & "00010" & OPC_LOAD;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    7,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, X"XXXX_XXXX",X"XXXX_XXXX",X"XXXX_XXXX", tc_instr),
                    "XXXXXXX",        -- branchflag
                    "0100100",        -- ack
                    -- === assert values ===
                    "0000000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "11XX", "11XX", "XXXX", "1111"),
                    "1101101",        -- stb
                    "1101101",        -- cyc
                    "11X00X1",        -- c_adr_pc_not_alu
                    "XXX11XX",        -- c_alu_a_not_pc
                    "0001100",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_i, alub_sel_imm_i, alub_sel_rs2),
                    "XXXXXXX",        -- c_alu_pc_not_branch
                    "0100000",        -- c_ir_load
                    "XXXXX1X",        -- c_pc_not_alu
                    "0001100",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_ld_hu, rsm_sel_ld_hu, rsm_sel_alu, rsm_sel_alu),
                    "0000100",        -- ld_reg
                    "0000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("load_b") then
                tc_instr := FUNC7_ADD & "01000" & "00000" & FUNC3_LB & "00010" & OPC_LOAD;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    7,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, X"XXXX_XXXX",X"XXXX_XXXX",X"XXXX_XXXX", tc_instr),
                    "XXXXXXX",        -- branchflag
                    "0100100",        -- ack
                    -- === assert values ===
                    "0000000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "1XXX", "1XXX", "XXXX", "1111"),
                    "1101101",        -- stb
                    "1101101",        -- cyc
                    "11X00X1",        -- c_adr_pc_not_alu
                    "XXX11XX",        -- c_alu_a_not_pc
                    "0001100",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_i, alub_sel_imm_i, alub_sel_rs2),
                    "XXXXXXX",        -- c_alu_pc_not_branch
                    "0100000",        -- c_ir_load
                    "XXXXX1X",        -- c_pc_not_alu
                    "0001100",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_ld_b, rsm_sel_ld_b, rsm_sel_alu, rsm_sel_alu),
                    "0000100",        -- ld_reg
                    "0000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("load_bu") then
                tc_instr := FUNC7_ADD & "01000" & "00000" & FUNC3_LBU & "00010" & OPC_LOAD;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    7,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, X"XXXX_XXXX",X"XXXX_XXXX",X"XXXX_XXXX", tc_instr),
                    "XXXXXXX",        -- branchflag
                    "0100100",        -- ack
                    -- === assert values ===
                    "0000000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "1XXX", "1XXX", "XXXX", "1111"),
                    "1101101",        -- stb
                    "1101101",        -- cyc
                    "11X00X1",        -- c_adr_pc_not_alu
                    "XXX11XX",        -- c_alu_a_not_pc
                    "0001100",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_i, alub_sel_imm_i, alub_sel_rs2),
                    "XXXXXXX",        -- c_alu_pc_not_branch
                    "0100000",        -- c_ir_load
                    "XXXXX1X",        -- c_pc_not_alu
                    "0001100",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_ld_bu, rsm_sel_ld_bu, rsm_sel_alu, rsm_sel_alu),
                    "0000100",        -- ld_reg
                    "0000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("store_w") then
                tc_instr := FUNC7_ADD & "01000" & "00000" & FUNC3_SW & "00010" & OPC_STORE;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    7,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, X"XXXX_XXXX",X"XXXX_XXXX",X"XXXX_XXXX", tc_instr),
                    "XXXXXXX",        -- branchflag
                    "0100100",        -- ack
                    -- === assert values ===
                    "0001100",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "1111", "1111", "XXXX", "1111"),
                    "1101101",        -- stb
                    "1101101",        -- cyc
                    "11X00X1",        -- c_adr_pc_not_alu
                    "XXX11XX",        -- c_alu_a_not_pc
                    "0001100",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_s, alub_sel_imm_s, alub_sel_rs2),
                    "XXXXXXX",        -- c_alu_pc_not_branch
                    "0100000",        -- c_ir_load
                    "XXXXX1X",        -- c_pc_not_alu
                    "0000000",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_ld_bu, rsm_sel_ld_bu, rsm_sel_alu, rsm_sel_alu),
                    "0000000",        -- ld_reg
                    "0000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("store_h") then
                tc_instr := FUNC7_ADD & "01000" & "00000" & FUNC3_SH & "00010" & OPC_STORE;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    7,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, X"XXXX_XXXX",X"XXXX_XXXX",X"XXXX_XXXX", tc_instr),
                    "XXXXXXX",        -- branchflag
                    "0100100",        -- ack
                    -- === assert values ===
                    "0001100",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "1100", "1100", "XXXX", "1111"),
                    "1101101",        -- stb
                    "1101101",        -- cyc
                    "11X00X1",        -- c_adr_pc_not_alu
                    "XXX11XX",        -- c_alu_a_not_pc
                    "0001100",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_s, alub_sel_imm_s, alub_sel_rs2),
                    "XXXXXXX",        -- c_alu_pc_not_branch
                    "0100000",        -- c_ir_load
                    "XXXXX1X",        -- c_pc_not_alu
                    "0000000",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_ld_bu, rsm_sel_ld_bu, rsm_sel_alu, rsm_sel_alu),
                    "0000000",        -- ld_reg
                    "0000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("store_b") then
                tc_instr := FUNC7_ADD & "01000" & "00000" & FUNC3_SB & "00010" & OPC_STORE;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    7,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, X"XXXX_XXXX",X"XXXX_XXXX",X"XXXX_XXXX", tc_instr),
                    "XXXXXXX",        -- branchflag
                    "0100100",        -- ack
                    -- === assert values ===
                    "0001100",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "1000", "1000", "XXXX", "1111"),
                    "1101101",        -- stb
                    "1101101",        -- cyc
                    "11X00X1",        -- c_adr_pc_not_alu
                    "XXX11XX",        -- c_alu_a_not_pc
                    "0001100",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_s, alub_sel_imm_s, alub_sel_rs2),
                    "XXXXXXX",        -- c_alu_pc_not_branch
                    "0100000",        -- c_ir_load
                    "XXXXX1X",        -- c_pc_not_alu
                    "0000000",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_ld_bu, rsm_sel_ld_bu, rsm_sel_alu, rsm_sel_alu),
                    "0000000",        -- ld_reg
                    "0000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("no_branch") then
                tc_instr := "0000000" & "01000" & "00000" & FUNC3_BGE & "00010" & OPC_BRANCH;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    6,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, tc_instr, tc_instr, tc_instr),
                    "XXX0XX",        -- branchflag
                    "010000",        -- ack
                    -- === assert values ===
                    "000000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "XXXX", "XXXX", "1111"),
                    "110001",        -- stb
                    "110001",        -- cyc
                    "11XXX1",        -- c_adr_pc_not_alu
                    "XXX1XX",        -- c_alu_a_not_pc
                    "000100",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_rs2),
                    "XXX0XX",        -- c_alu_pc_not_branch
                    "010000",        -- c_ir_load
                    "XXXX1X",        -- c_pc_not_alu
                    "000000",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu),
                    "000000",        -- ld_reg
                    "000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("branch") then
                tc_instr := "0000000" & "01000" & "00000" & FUNC3_BGE & "00010" & OPC_BRANCH;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    6,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, tc_instr, tc_instr, tc_instr),
                    "XXX1XX",        -- branchflag
                    "010000",        -- ack
                    -- === assert values ===
                    "000000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "XXXX", "XXXX", "1111"),
                    "110001",        -- stb
                    "110001",        -- cyc
                    "11XXX1",        -- c_adr_pc_not_alu
                    "XXX10X",        -- c_alu_a_not_pc
                    "000110",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_b, alub_sel_rs2),
                    "XXX01X",        -- c_alu_pc_not_branch
                    "010000",        -- c_ir_load
                    "XXXX0X",        -- c_pc_not_alu
                    "000000",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu),
                    "000000",        -- ld_reg
                    "000010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("jal") then
                tc_instr := B"0100_0101_1111_0000_1011" & "00010" & OPC_JAL;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    5,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, tc_instr, tc_instr, tc_instr),
                    "XXXXX",        -- branchflag
                    "01000",        -- ack
                    -- === assert values ===
                    "00000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "XXXX", "1111"),
                    "11001",        -- stb
                    "11001",        -- cyc
                    "11XX1",        -- c_adr_pc_not_alu
                    "XXX0X",        -- c_alu_a_not_pc
                    "00010",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_j, alub_sel_rs2),
                    "XXXXX",        -- c_alu_pc_not_branch
                    "01000",        -- c_ir_load
                    "XXX0X",        -- c_pc_not_alu
                    "00010",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_pc, rsm_sel_alu),
                    "00010",        -- ld_reg
                    "00010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("jalr") then
                tc_instr := B"0100_0101_1111" & "00000" & FUNC3_JALR & "00010" & OPC_JALR;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    5,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, tc_instr, tc_instr, tc_instr),
                    "XXXXX",        -- branchflag
                    "01000",        -- ack
                    -- === assert values ===
                    "00000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "XXXX", "1111"),
                    "11001",        -- stb
                    "11001",        -- cyc
                    "11XX1",        -- c_adr_pc_not_alu
                    "XXX1X",        -- c_alu_a_not_pc
                    "00010",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_i, alub_sel_rs2),
                    "XXXXX",        -- c_alu_pc_not_branch
                    "01000",        -- c_ir_load
                    "XXX0X",        -- c_pc_not_alu
                    "00010",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu),
                    "00010",        -- ld_reg
                    "00010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("lui") then
                tc_instr := B"0100_0101_1111_0000_1011" & "00010" & OPC_LUI;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    5,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, tc_instr, tc_instr, tc_instr),
                    "XXXXX",        -- branchflag
                    "01000",        -- ack
                    -- === assert values ===
                    "00000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "XXXX", "1111"),
                    "11001",        -- stb
                    "11001",        -- cyc
                    "11XX1",        -- c_adr_pc_not_alu
                    "XXXXX",        -- c_alu_a_not_pc
                    "00000",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_rs2),
                    "XXXXX",        -- c_alu_pc_not_branch
                    "01000",        -- c_ir_load
                    "XXX1X",        -- c_pc_not_alu
                    "00010",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_lui, rsm_sel_alu),
                    "00010",        -- ld_reg
                    "00010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            elsif run("auipc") then
                tc_instr := B"0100_0101_1111_0000_1011" & "00010" & OPC_AUIPC;
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                run_controller_cycle(
                    5,
                    -- === stimuli ===
                    -- instruction
                    (tc_instr, tc_instr, tc_instr, tc_instr, tc_instr, tc_instr),
                    "XXXXX",        -- branchflag
                    "01000",        -- ack
                    -- === assert values ===
                    "00000",        -- we
                    -- sel
                    ("1111", "1111", "XXXX", "XXXX", "1111"),
                    "11001",        -- stb
                    "11001",        -- cyc
                    "11XX1",        -- c_adr_pc_not_alu
                    "XXX0X",        -- c_alu_a_not_pc
                    "00010",        -- check_c_alu_b
                    -- c_alu_b
                    (alub_sel_rs2, alub_sel_rs2, alub_sel_rs2, alub_sel_imm_u, alub_sel_rs2),
                    "XXXXX",        -- c_alu_pc_not_branch
                    "01000",        -- c_ir_load
                    "XXX1X",        -- c_pc_not_alu
                    "00010",        -- check_c_regsortmux
                    -- c_regsortmux
                    (rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu, rsm_sel_alu),
                    "00010",        -- ld_reg
                    "00010"         -- pc_load
                );
                wait_clk_cycles(clk, 1);
            end if;
        end loop;
        test_runner_cleanup(runner); -- Simulation ends here
    end process;
end architecture;
