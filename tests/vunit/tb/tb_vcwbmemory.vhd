library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.verificationutils.all;


entity TB_VCWBMEMORY is
    generic (runner_cfg : string);
end entity;


architecture TESTBENCH of TB_VCWBMEMORY is
    -- these 2 constants are not required by vunit itself
    constant CLK_PERIOD : time := 10 ns;
    constant short_delay: time := 0.5 ns;

    -- clk ist not required by vunit
    signal clk,rst : std_logic := '0';
    signal we, stb, ack, cyc : std_logic;
    signal adr, data_i, data_o : std_logic_vector(31 downto 0);
    signal sel, gpo : std_logic_vector(3 downto 0);
begin
    uut: entity work.vcwbmemory generic map (
        loadfile => "tests/vunit/memory_vcwbmemory.txt"
    )
    port map (
        clk => clk,
        rst => rst,
        adr => adr,
        data_i => data_i,
        data_o => data_o,
        we => we,
        sel => sel,
        stb => stb,
        ack => ack,
        cyc => cyc,
        gpo => gpo
    );
    clk <= not clk after CLK_PERIOD / 2;
    -- this process is mandetory
    main: process
    begin
        test_runner_setup(runner, runner_cfg);
        while test_suite loop
            set_stop_level(failure);
            adr <= X"XXXXXXXX";
            data_i <= X"XXXXXXXX";
            we <= 'X';
            sel <= "XXXX";
            stb <= '0';
            cyc <= '0';
            if run("read_first_byte") then
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                adr <= std_logic_vector(to_unsigned(4, 32));
                we <= '0';
                stb <= '1';
                stb <= '1';
                cyc <= '1';
                sel <= "1111";
                wait until falling_edge(clk);
                check(ack = '1', "ack missing");
                check(data_o = X"04050607", "wrong data output");
                sel <= X"X";
                cyc <= '0';
                stb <= '0';
                wait_clk_cycles(clk, 2);
            -- use zeros instead of don't cares for adr, data_i
            elsif run("read_first_byte_zeros") then
                we <= '0';
                sel <= "0000";
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                adr <= std_logic_vector(to_unsigned(4, 32));
                we <= '0';
                stb <= '1';
                stb <= '1';
                cyc <= '1';
                sel <= "1111";
                wait until falling_edge(clk);
                check(ack = '1', "ack missing");
                check(data_o = X"04050607", "wrong data output");
                sel <= X"X";
                cyc <= '0';
                stb <= '0';
                wait_clk_cycles(clk, 2);
            elsif run("read_multiple_bytes") then
                rst <= '1';
                wait_clk_cycles(clk, 1);
                for idx in 0 to 3 loop
                    info("cycle " & integer'image(idx));
                    rst <= '0';
                    cyc <= '0';
                    stb <= '0';
                    wait_clk_cycles(clk, 1);
                    check(ack = '0', "ack should be 0");
                    sel <= "1111";
                    cyc <= '1';
                    stb <= '1';
                    we <= '0';
                    adr <= std_logic_vector(to_unsigned(idx * 4, 32));
                    wait_clk_cycles(clk, 1);
                    check(ack = '1', "ack should be 1");
                    check(data_o(31 downto 24) = std_logic_vector(to_unsigned(idx * 4 + 0, 8)), "wrong read data 1");
                    check(data_o(23 downto 16) = std_logic_vector(to_unsigned(idx * 4 + 1, 8)), "wrong read data 2");
                    check(data_o(15 downto 8) = std_logic_vector(to_unsigned(idx * 4 + 2, 8)), "wrong read data 3");
                    check(data_o(7 downto 0) = std_logic_vector(to_unsigned(idx * 4 + 3, 8)), "wrong read data 4");
                    wait until rising_edge(clk);
                    cyc <= '0';
                    stb <= '0';
                    wait_clk_cycles(clk, 1);
                end loop;
                wait_clk_cycles(clk, 1);
            elsif run("write_byte") then
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                cyc <= '0';
                stb <= '0';
                wait_clk_cycles(clk, 1);
                sel <= "1111";
                we <= '1';
                cyc <= '1';
                stb <= '1';
                adr <= std_logic_vector(to_unsigned(0, 32));
                data_i <= X"AFFEBEEF";
                wait_clk_cycles(clk, 1);
                we <= '0';
                stb <= '0';
                cyc <= '0';
                wait_clk_cycles(clk, 1);
                stb <= '1';
                cyc <= '1';
                wait_clk_cycles(clk, 1);
                check(data_o = X"AFFEBEEF", "write not working");
                stb <= '0';
                cyc <= '0';
                wait_clk_cycles(clk, 2);
            elsif run("write_lower_byte") then
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                cyc <= '0';
                stb <= '0';
                wait_clk_cycles(clk, 1);
                sel <= "0001";
                we <= '1';
                cyc <= '1';
                stb <= '1';
                adr <= std_logic_vector(to_unsigned(0, 32));
                data_i <= X"AFFEBEEF";
                wait_clk_cycles(clk, 1);
                we <= '0';
                stb <= '0';
                cyc <= '0';
                wait_clk_cycles(clk, 1);
                stb <= '1';
                cyc <= '1';
                sel <= "1111";
                wait_clk_cycles(clk, 1);
                check(data_o = X"000102EF", "write not working");
                stb <= '0';
                cyc <= '0';
                wait_clk_cycles(clk, 2);
            elsif run("write_high_byte") then
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                cyc <= '0';
                stb <= '0';
                wait_clk_cycles(clk, 1);
                sel <= "1000";
                we <= '1';
                cyc <= '1';
                stb <= '1';
                adr <= std_logic_vector(to_unsigned(0, 32));
                data_i <= X"AFFEBEEF";
                wait_clk_cycles(clk, 1);
                we <= '0';
                stb <= '0';
                cyc <= '0';
                wait_clk_cycles(clk, 1);
                stb <= '1';
                cyc <= '1';
                sel <= "1111";
                wait_clk_cycles(clk, 1);
                check(data_o = X"AF010203", "write not working");
                stb <= '0';
                cyc <= '0';
                wait_clk_cycles(clk, 2);
            elsif run("write_multiple_bytes") then
                rst <= '1';
                wait_clk_cycles(clk, 1);
                for idx in 0 to 3 loop
                    info("cycle " & integer'image(idx));
                    rst <= '0';
                    cyc <= '0';
                    stb <= '0';
                    wait_clk_cycles(clk, 1);
                    check(ack = '0', "ack should be 0");
                    sel <= "1111";
                    cyc <= '1';
                    stb <= '1';
                    we <= '1';
                    adr <= std_logic_vector(to_unsigned(idx * 4, 32));
                    data_i <= std_logic_vector(to_unsigned(16#FF# - idx, 16)) &
                            std_logic_vector(to_unsigned(16#99# - idx, 16));
                    wait_clk_cycles(clk, 1);
                    check(ack = '1', "ack should be 1");
                    wait until rising_edge(clk);
                    cyc <= '0';
                    stb <= '0';
                    wait_clk_cycles(clk, 1);
                end loop;
                wait_clk_cycles(clk, 1);
                for idx in 0 to 3 loop
                    info("cycle " & integer'image(idx));
                    rst <= '0';
                    cyc <= '0';
                    stb <= '0';
                    wait_clk_cycles(clk, 1);
                    check(ack = '0', "ack should be 0");
                    sel <= "1111";
                    cyc <= '1';
                    stb <= '1';
                    we <= '0';
                    adr <= std_logic_vector(to_unsigned(idx * 4, 32));
                    wait_clk_cycles(clk, 1);
                    check(ack = '1', "ack should be 1");
                    check(data_o = std_logic_vector(to_unsigned(16#FF# - idx, 16))
                        & std_logic_vector(to_unsigned(16#99# - idx, 16))
                        , "wrong data in memory");
                    wait until rising_edge(clk);
                    cyc <= '0';
                    stb <= '0';
                    wait_clk_cycles(clk, 1);
                end loop;
            elsif run("gpo") then
                rst <= '1';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                wait_clk_cycles(clk, 1);
                check(gpo = "0000", "wrong gpo reset value");
                adr <= X"0000_1000";
                sel <= "1111";
                we <= '0';
                stb <= '1';
                cyc <= '1';
                wait until falling_edge(clk);
                check(ack = '1', "ack missing");
                check(gpo = "0000", "wrong gpo output");
                check(data_o = X"00000000", "wrong gpo read");
                stb <= '0';
                cyc <= '0';
                wait until falling_edge(clk);
                data_i <= X"ffff_ffff";
                sel <= "1111";
                stb <= '1';
                cyc <= '1';
                we <= '1';
                wait until falling_edge(clk);
                check(ack = '1', "ack missing");
                check(gpo = "1111", "wrong gpo output");
                data_i <= X"ffff_ffff";
                sel <= "0000";
                stb <= '0';
                cyc <= '0';
                we <= '0';
                wait until falling_edge(clk);
                wait until falling_edge(clk);
                data_i <= X"ffff_ffff";
                sel <= "1111";
                stb <= '1';
                cyc <= '1';
                we <= '0';
                wait until falling_edge(clk);
                check(ack = '1', "ack missing");
                check(gpo = "1111", "wrong gpo output");
                check(data_o = X"ffff_ffff", "wrong gpo read");
                sel <= "0000";
                stb <= '0';
                cyc <= '0';
                we <= '0';
                wait until falling_edge(clk);
                wait until falling_edge(clk);
                data_i <= X"0C00_0000";
                sel <= "1000";
                stb <= '1';
                cyc <= '1';
                we <= '1';
                wait until falling_edge(clk);
                check(ack = '1', "ack missing");
                check(gpo = X"C", "wrong gpo output");
                sel <= "0000";
                stb <= '0';
                cyc <= '0';
                we <= '0';
                wait until falling_edge(clk);
                data_i <= X"ffff_ffff";
                sel <= "1111";
                stb <= '1';
                cyc <= '1';
                we <= '0';
                wait until falling_edge(clk);
                check(ack = '1', "ack missing");
                check(gpo = "1100", "wrong gpo output");
                check(data_o = X"0cff_ffff", "wrong gpo read");
                sel <= "0000";
                stb <= '0';
                cyc <= '0';
                we <= '0';
                wait_clk_cycles(clk, 2);
                adr <= X"0000_0000";
                sel <= "1111";
                stb <= '1';
                cyc <= '1';
                we <= '0';
                wait until falling_edge(clk);
                check(ack = '1', "ack missing");
                check(gpo = "1100", "wrong gpo output");
                check(data_o = X"00010203", "wrong memory read");
                sel <= "0000";
                stb <= '0';
                cyc <= '0';
                we <= '0';
                wait_clk_cycles(clk, 2);
                rst <= '0';
                cyc <= '0';
                stb <= '0';
                wait_clk_cycles(clk, 1);
                check(ack = '0', "ack should be 0");
                sel <= "1111";
                cyc <= '1';
                stb <= '1';
                we <= '1';
                data_i <= X"3300_0000";
                wait_clk_cycles(clk, 5);
                check(gpo = "1100", "wrong gpo output");
            end if;
        end loop;
        test_runner_cleanup(runner);
    end process;
end;