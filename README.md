# Simple RV32I Implementation

This project implements a simple RISC-V CPU, which implements all RV32I instructions as of version 2.2 with the excaption of `ecall` and `ebreak`, in vhdl. See the [specification Rev 20191213](https://github.com/riscv/riscv-isa-manual/releases/download/Ratified-IMAFDQC/riscv-spec-20191213.pdf) for further details.

## Project structure

| Directory                   | Purpose        |
|-----------------------------|----------------|
| `cliinterface/`             | subcommand source files of rv32i |
| `src/`                      | cpu source files |
| `src/hw/`                   | cpu hdl sources |
| `src/fw/`                   | firmware/ software |
| `tests/`                    | tests |
| `tests/verification`        | verification components (like memory) and utilities |
| `tests/vunit/`              | all files neccessary for tests with vunit |
| `tests/vunit/tb/`           | all VHDL testbenches |
| `tests/vunit/gtkwavesetup/` | tcl scripts to set up signals in gtkwave for individual unit tests |
| `tests/vunit/fw/`           | contains firmware for complete cpu tests |

---

## Command line interface

The central command line interface, executed with `./rv32i <subcommand> <-options>`, `python rv32i <subcommand> <-options>` or `python3 rv32i <subcommand> <-options>` in the root directory of this repository provides an interface for all relevant action like executing tests or generating the complete documentation from sources.

### Prerequisites

The repository contains the command line utility `rv32i`, writting in python, that provides an interface for  many common operations like unit tests, documentation generation, FPGA-project generation and synthesis. In order to uses this script you have to install its dependencies, as specified in `requirements.txt`. You can use `pip` to automatically install all dependencies:

```sh
pip install -r requirements.txt
```

Additional the unit tests and the vhdl simulation of the simulator rely on `ghdl`, `gtkwave`, the `riscv64-unknown-elf` gcc toolchain and a custom RISC-V simulator [simple-rv32i-simulator](https://r-n-d.informatik.hs-augsburg.de:8080/sose2022-hwsys-risc-v/simple-rv32i-simulator), which all must be installed and available in the path variable.

Further `rv32i genblocks` needs besides `ghdl` `ghdl-yosys-plugin` and `netlistsvg`.

### Usage

The utility provides subcommands for different operations, i.e.:

* `test`: run unit tests
* `doc`: generate further documentation
* `genblocks`: generates svg block diagrams of all `src/hw/*.vhd(l)` files. The results are stored in `doc/generated/blockdiagrams/`
* `runfw`: run arbitrary firmware in `.bin` file in a vhdl simulation with ghdl

See its build in help for more information:

```sh
./rv32i --help
```

---

## Documentation

The following documentations are available after individual generation with `./rv32i doc`:

* Doxygen of vhdl files (reuqires doxygen to be available on the host system) in [./doc/generated/cpu-doxygen/](./doc/generated/cpu-doxygen/html/index.html)
* Example timing diagrams [./doc/controller.md](./doc/controller.md) of the controller
* TODO: LaTeX reports

---

## CPU structure

The CPU consists of the following components, which are described in the documentation in detail:

* Controller
* ALU
* Register file
* instruction register
* program counter register
* program counter incrementor (called add4)
* several multiplexers

## Testing

All vhdl based tests are implemented using the [VUnit library](https://vunit.github.io). VUnit provides a Python library for preparing, selecting and starting tests, as well, as evaluating whether individual tests have succeeded or failed. VUnit brings its own standalone cli interface based on argparse. In this repository the vunit pyhton library is used within the main `./rv32i` Python script and all arguments passed to the `test` subcommand are passed on to VUnit's own argument parser. VUnit automatically detects one of the following VHDL simulators:

* `ghdl` with `gtkwave` as waveform viewer
* `ModelSim`
* `QuestaSim`
* `Riviera PRO`
* `Active-HDL`

All tests in this repository were implemented and run with `ghdl/gtkwave`, but except from slecting certrain signals for the gui viewer at startup, other simulators should work as well.

The testbench itself is implemented in VHDL, but must meet certain requirements, in order to be detected by VUnit:

* The testbench entity name must start with `tb_` (or `TB_`).
* Testbench file must be registers from the python interface (all *.vhd files in `src/` `tests/verification` and `tests/vunit/tb/` are automatically added, when calling `./rv32i test`).
* The individual testcases are defined in a single process in the testbench architecture with the `run(...)` function.
* For further details see the vunit documentation or start with the [testbench template](./tests/vunit/tb/tb_vunit_demo.vhd).

The following common commands are used to interface with the tests:

```sh
# list all tests
./rv32i test --list
# run all tests
./rv32i test
# run tests parallel (i.e. in 4 processes)
./rv32i test -p 4
# run a selecting of tests, that matches a certain expression (i.e. all alu tests)
./rv32i test *.tb_alu.*
# run a certain test and start gtkwave
./rv32i test tblib.tb_alu.OR --gui
# use --clean to recompile all libraries before running tests
./rv32i test --clean
```

In general, this repository implements two kinds of tests, unit tests for individual components of the cpu and tests, that run compiled binaries on the complete cpu for verification of the complete system.

### Component tests

For each component, except from some multiplexers, a individual test bench is available. Each test bench implements several unit tests, testing different functionallity of the individual component. These tests are realized only in vhdl with the exception of the Python call interface of VUnit/ `./rv32i`.

### CPU verification

The verification of the complete cpu uses a different approach: The folder [./tests/vunit/fw/](./tests/vunit/fw/) contains multiple testfirmware assembly files, each within a folder with the identical name as the assembly file and define the individual verification tests. The assembly files in total cotain all instructions, that should be supported by this cpu. `rv32i` compiles these assembly files to a target executables (*.bin), and executes these binaries in mentioned `simple RV32I simulator`. This simulator logs all memory accesses (address, read/write and transferred data) into a file. After that the complete cpu vhdl implementation executes the same binary in ghdl and compares, that the memory accesses now appear in identical order (with identical address, read/write flag, and data) in the testbench.

 As a memory is necessary, a [wishbone slave memory simulation component](./tests/verification/vcwbmemory.vhd) is implemented and can be initialized with a proprietary text file. This proprietary text file is generated of the bin file automatically by `./rv32i`

 To add additional tests, the following criteria must be fulfilled, so that they are automatically detected and processed by `rv32i`:

 * The test must be located in a folder `./tests/vunit/fw/<testcasename>` and contain a `<testcasename>.asm` file
 * The testcase entry point is `main`, which therefore must be available as a **global** label in order to be linkable.
 * The testcase must end with an empty infinite loop, which the simulator can detect and stop the simulation (i.e. `endloop:     j endloop`).

 ### Running arbitrary binaries with ghdl

 You can run arbitrary binaries, by calling

 ```sh
 ./rv32i runfw -b <path to .bin>
 ```

 This works the same way, as cpu verification tests, but it can execute any suitable binary. If the binary does not contain an empty infinite loop, one can use the `-c <N>` flag, which restricts the number of cycles, that are executed in the simulator, to `N`.
