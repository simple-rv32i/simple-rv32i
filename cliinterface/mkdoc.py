import click
from shutil import which
import os
import subprocess
import logging


def wavedrom2svg(srcdir: str, destdir: str):
    import wavedrom

    for file in os.listdir(srcdir):
        if file.endswith(".wavedrom.json"):
            logging.debug("converting file %s to %s.svg ...", file, file[0:-14])
            with open(os.path.join(srcdir, file), "r") as f:
                data = f.read()
            try:
                svg = wavedrom.render(data)
            except BaseException as e:
                logging.error("conversion failed: %s", e)
                return
            svg.saveas(os.path.join(destdir, file[0:-14] + ".svg"))


@click.command()
@click.option(
    "-d",
    "--doctype",
    type=click.Choice(["cpu-doxygen", "manual", "wavedrom"], case_sensitive=False),
    required=True,
)
def doc(doctype):
    """generates documentation, output in doc/generated/"""
    os.makedirs("doc/generated", exist_ok=True)
    os.makedirs("doc/graphics/generated", exist_ok=True)
    if doctype == "cpu-doxygen":
        if which("doxygen") is not None:
            subprocess.check_call(["doxygen", "CPUDoxyfile"])
        else:
            exit("ERROR: doxygen not found")
    elif doctype == "wavedrom":
        wavedrom2svg("doc/graphics/timdiag_src", "doc/graphics/generated")
    else:
        raise Exception("not implemented yet")
