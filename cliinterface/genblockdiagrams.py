import click
import logging
from os import listdir, makedirs
from os.path import isfile, join, exists, abspath
import pathlib
import re
import shutil
import subprocess


def get_design_files(path) -> list:
    files = []
    path = abspath(path)
    if not exists(path):
        logging.error("path '%s' not found", path)
        return []
    for f in listdir(path):
        suf = pathlib.Path(f).suffix
        if isfile(join(path, f)) and (suf == ".vhd" or suf == ".vhdl"):
            files.append(join(path, f))
    return files


def get_entity_name(path):
    ghdl_ret = subprocess.run(
        ["ghdl", "-f", "--std=08", path], capture_output=True, cwd="genblocks"
    )
    lines = ghdl_ret.stdout.decode("utf-8").splitlines()
    for line in lines:
        m = re.match(r"(entity)\s([a-zA-Z]\w*)", line)
        if type(m) == re.Match:
            return m.group(2)
    return None


@click.command()
def genblocks():
    """generate block diagrams for all entities in src/hw/*.vhd"""
    design_dir = "src/hw/"
    destination_dir = "doc/generated/blockdiagrams/"
    if exists(destination_dir):
        shutil.rmtree(destination_dir)
    makedirs(destination_dir, exist_ok=True)
    makedirs("genblocks/", exist_ok=True)
    logging.info("generating block diagrams of directory %s", design_dir)
    design_files = get_design_files(design_dir)
    design_files_str = ""
    entities = []
    for f in design_files:
        logging.debug("searching file %s", f)
        ret = get_entity_name(f)
        if type(ret) == str:
            entities.append(ret)
            logging.info("found entity %s", ret)
        design_files_str += f + " "
    for entity in entities:
        yosys_str = (
            "ghdl --std=08 "
            + design_files_str
            + "-e "
            + entity
            + "; prep -top "
            + entity
            + ";"
            + " write_json -compat-int svg.json"
        )
        ret = subprocess.run(
            ["yosys", "-m", "ghdl", "-p", yosys_str],
            capture_output=True,
            cwd="genblocks",
        )
        if ret.returncode != 0:
            logging.error("=== error: ===")
            logging.error(ret.returncode)
            logging.error("--- stdout ---")
            logging.error(ret.stdout.decode("utf-8"))
            logging.error("--- stderr ---")
            logging.error(ret.stderr.decode("utf-8"))
            logging.error("=== end error ===")
            continue
        else:
            logging.info("entity %s analyzed successfully", entity)
        svg_path = join(destination_dir, entity + ".svg")
        ret = subprocess.run(
            ["netlistsvg", "svg.json", "-o", abspath(svg_path)],
            capture_output=True,
            cwd="genblocks",
        )
        if ret.returncode != 0:
            logging.error("=== error: ===")
            logging.error(ret.returncode)
            logging.error("--- stdout ---")
            logging.error(ret.stdout.decode("utf-8"))
            logging.error("--- stderr ---")
            logging.error(ret.stderr.decode("utf-8"))
            logging.error("=== end error ===")
            continue
        else:
            logging.info("generated block diagram %s", svg_path)
    shutil.rmtree("genblocks/")
    logging.info("finished gnerating block diagrams")
