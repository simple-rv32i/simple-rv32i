import click
import os
import subprocess
from vunit import VUnit


def bin2txt(binfile: str, txtfile: str):
    with open(binfile, "rb") as f:
        indata = f.read(-1)
    memorysize = len(indata)
    with open(txtfile, "w") as f:
        f.write("{}\n".format(memorysize))
        for idx in range(0, memorysize):
            f.write(
                "{:08b}\n".format(
                    indata[idx],
                )
            )


def build_test_fw(fwdir: str, fwname: str):
    for file in os.listdir(os.path.join(fwdir, fwname)):
        if os.path.isfile(os.path.join(fwdir, fwname, file)):
            if file[-2:] == ".o" or file[-4:] == ".elf" or file[-4:] == ".bin":
                print("deleting file: {}".format(os.path.join(fwdir, fwname, file)))
                os.remove(os.path.join(fwdir, fwname, file))
    archparams = ["-march=rv32i", "-mabi=ilp32"]
    subprocess.check_call(
        ["riscv64-unknown-elf-as"]
        + archparams
        + ["-c", "-Wall", "../startup.asm", "-o" "startup.o"],
        cwd=os.path.join(fwdir, fwname),
    )
    objectfiles = ["startup.o"]
    for file in os.listdir(os.path.join(fwdir, fwname)):
        if os.path.isfile(os.path.join(fwdir, fwname, file)):
            if file[-4:] == ".asm":
                objectfiles += [file[0:-4] + ".o"]
                print("assembling file: {}".format(file))
                subprocess.check_call(
                    ["riscv64-unknown-elf-as"]
                    + archparams
                    + ["-c", "-Wall", file, "-o", file[0:-4] + ".o"],
                    cwd=os.path.join(fwdir, fwname),
                )
            if file[-2:] == ".c":
                objectfiles += [file[0:-2] + ".o"]
                print("compiling file: {}".format(file))
                subprocess.check_call(
                    ["riscv64-unknown-elf-gcc"]
                    + archparams
                    + [
                        "-c",
                        "-Wall",
                        "-O0",
                        "-nostdlib",
                        file,
                        "-o",
                        file[0:-2] + ".o",
                    ],
                    cwd=os.path.join(fwdir, fwname),
                )
    subprocess.check_call(
        ["riscv64-unknown-elf-gcc"]
        + archparams
        + [
            "-Wall",
            "-O0",
            "-nostdlib",
            "-T../simmem.ld",
            "-Wl,--gc-sections",
        ]
        + objectfiles
        + ["-o", fwname + ".elf"],
        cwd=os.path.join(fwdir, fwname),
    )
    subprocess.check_call(
        [
            "riscv64-unknown-elf-objcopy",
            "-S",
            "-O",
            "binary",
            fwname + ".elf",
            fwname + ".bin",
        ],
        cwd=os.path.join(fwdir, fwname),
    )
    bin2txt(
        os.path.join(fwdir, fwname, fwname + ".bin"),
        os.path.join(fwdir, fwname, fwname + ".mem.txt"),
    )
    subprocess.check_call(
        [
            "rv32i-sim",
            "--silent",
            "-e",
            os.path.join(fwdir, fwname, fwname + ".elf"),
            "-t",
            os.path.join(fwdir, fwname, fwname + ".trace.txt"),
        ]
    )


@click.command(
    context_settings=dict(
        ignore_unknown_options=True,
    )
)
@click.option("-h", "--help", is_flag=True)
@click.option("--version", is_flag=True)
@click.argument("vunitparams", nargs=-1, type=click.UNPROCESSED)
def test(help, version, vunitparams):
    """Wrapper around vunit, see rv32i test --help for more information"""
    params = vunitparams
    if version is True:
        params = ("--version",) + params
    if help is True:
        params = ("--help",) + params
        click.echo("This is a wrapper around vunits standard interface.")
        click.echo("\tsee the following help page for <vunit params> but use")
        click.echo('\t"rv32i test <vunit params>" instead of')
        click.echo('\t"rv32i <vunit params>"')
        click.echo("========== 'vunit --help' output ==========\n")
    else:
        testfwdir = "tests/vunit/fw/"
        testfirmware_filenames = []
        for file in os.listdir(testfwdir):
            if os.path.isdir(os.path.join(testfwdir, file)):
                testfirmware_filenames += [file]
                build_test_fw(testfwdir, file)
    vu = VUnit.from_argv(argv=params)
    lib = vu.add_library("tblib")
    lib.add_source_files("tests/verification/*.vhd")
    lib.add_source_files("tests/vunit/tb/*.vhd")
    lib.add_source_files("src/hw/*.vhd")
    # register tcl waveform setup
    try:
        cputestbench = lib.test_bench("tb_riscy_cpu")
        cputestbench.set_sim_option(
            "ghdl.gtkwave_script.gui",
            "tests/vunit/gtkwavesetup/tb_riscy_cpu.tcl",
        )
        for testcase in cputestbench.get_tests():
            if testcase.name == "check_firmware":
                for filename in testfirmware_filenames:
                    testcase.add_config(
                        name=filename,
                        generics=dict(
                            loadfile=os.path.join(
                                testfwdir, filename, filename + ".mem.txt"
                            ),
                            checkfile=os.path.join(
                                testfwdir, filename, filename + ".trace.txt"
                            ),
                        ),
                    )
    finally:
        pass
    for testcasename in ["tb_controller", "tb_alu"]:
        try:
            testbench = lib.test_bench(testcasename)
            testbench.set_sim_option(
                "ghdl.gtkwave_script.gui",
                "tests/vunit/gtkwavesetup/" + testcasename + ".tcl",
            )
        finally:
            pass
    vu.main()
