import click
import os
from pathlib import Path
import subprocess
from vunit import VUnit


def bin2txt(binfile: str, txtfile: str):
    with open(binfile, "rb") as f:
        indata = f.read(-1)
    memorysize = len(indata)
    with open(txtfile, "w") as f:
        f.write("{}\n".format(memorysize))
        for idx in range(0, memorysize):
            f.write(
                "{:08b}\n".format(
                    indata[idx],
                )
            )


@click.command(
    context_settings=dict(
        ignore_unknown_options=True,
    )
)
@click.option("-h", "--help", is_flag=True)
@click.option("--version", is_flag=True)
@click.option(
    "-b",
    "--binfile",
    required=True,
    type=click.Path(exists=True),
)
@click.option("-c", "--count", type=int, default=-1)
@click.argument("vunitparams", nargs=-1, type=click.UNPROCESSED)
def runfw(help, version, binfile, count, vunitparams):
    """Run bin file on rv32i in ghdl, see rv32i runfw --help for more information"""
    params = vunitparams
    workingdir = "utils/temp/"
    if version is True:
        params = ("--version",) + params
    if help is True:
        params = ("--help",) + params
        click.echo("This is a wrapper around vunits standard interface.")
        click.echo("\tsee the following help page for <vunit params> but use")
        click.echo('\t"rv32i test <vunit params>" instead of')
        click.echo('\t"rv32i <vunit params>"')
        click.echo("interfacing firmware simulation")
        click.echo("\t-b, --bin        <FILE> path to firmware bin file")
        click.echo("\t-c, --cycles     <INT> if > 0, only run INT cycles and stop")
        click.echo("========== 'vunit --help' output ==========\n")
    else:
        Path(workingdir).mkdir(parents=True, exist_ok=True)
        bin2txt(binfile, os.path.join(workingdir, "firmware.mem.txt"))
        simargs = [
            "rv32i-sim",
            "--silent",
            "-b",
            binfile,
            "-t",
            os.path.join(workingdir, "firmware.trace.txt"),
        ]
        if count > 0:
            simargs += ["-c", "{}".format(count)]
        subprocess.check_call(simargs)
    vu = VUnit.from_argv(argv=params + ("-o", os.path.join(workingdir, "vunit_out")))
    lib = vu.add_library("tblib")
    lib.add_source_files("tests/verification/*.vhd")
    lib.add_source_files("utils/tb/*.vhd")
    lib.add_source_files("src/hw/*.vhd")
    # register tcl waveform setup
    try:
        cputestbench = lib.test_bench("tb_runfwbin")
        cputestbench.set_sim_option(
            "ghdl.gtkwave_script.gui",
            "utils/tb_runfwtraces.tcl",
        )
        for testcase in cputestbench.get_tests():
            if testcase.name == "check_firmware":
                testcase.add_config(
                    name="firmware",
                    generics=dict(
                        loadfile=os.path.join(workingdir, "firmware.mem.txt"),
                        checkfile=os.path.join(workingdir, "firmware.trace.txt"),
                    ),
                )
    finally:
        pass
    vu.main()
