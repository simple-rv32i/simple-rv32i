library ieee;
use ieee.std_logic_1164.all;


package rv32i is
    -- opcode
    constant OPC_ALU    : std_logic_vector(6 downto 0) := "0110011";
    constant OPC_ALUI   : std_logic_vector(6 downto 0) := "0010011";
    constant OPC_LOAD   : std_logic_vector(6 downto 0) := "0000011";
    constant OPC_STORE  : std_logic_vector(6 downto 0) := "0100011";
    constant OPC_BRANCH : std_logic_vector(6 downto 0) := "1100011";
    constant OPC_JAL    : std_logic_vector(6 downto 0) := "1101111";
    constant OPC_JALR   : std_logic_vector(6 downto 0) := "1100111";
    constant OPC_LUI    : std_logic_vector(6 downto 0) := "0110111";
    constant OPC_AUIPC  : std_logic_vector(6 downto 0) := "0010111";
    constant OPC_E      : std_logic_vector(6 downto 0) := "1110011";

    -- func 3
    constant FUNC3_ADD      : std_logic_vector(2 downto 0) := "000";
    constant FUNC3_SUB      : std_logic_vector(2 downto 0) := "000";
    constant FUNC3_LB       : std_logic_vector(2 downto 0) := "000";
    constant FUNC3_SB       : std_logic_vector(2 downto 0) := "000";
    constant FUNC3_JALR     : std_logic_vector(2 downto 0) := "000";
    constant FUNC3_ECALL    : std_logic_vector(2 downto 0) := "000";
    constant FUNC3_EBREAK   : std_logic_vector(2 downto 0) := "000";

    constant FUNC3_SLL      : std_logic_vector(2 downto 0) := "001";
    constant FUNC3_LH       : std_logic_vector(2 downto 0) := "001";
    constant FUNC3_SH       : std_logic_vector(2 downto 0) := "001";
    constant FUNC3_BNE      : std_logic_vector(2 downto 0) := "001";

    constant FUNC3_SLT      : std_logic_vector(2 downto 0) := "010";
    constant FUNC3_LW       : std_logic_vector(2 downto 0) := "010";
    constant FUNC3_SW       : std_logic_vector(2 downto 0) := "010";

    constant FUNC3_SLTU     : std_logic_vector(2 downto 0) := "011";

    constant FUNC3_XOR      : std_logic_vector(2 downto 0) := "100";
    constant FUNC3_LBU      : std_logic_vector(2 downto 0) := "100";
    constant FUNC3_BLT      : std_logic_vector(2 downto 0) := "100";

    constant FUNC3_SRL      : std_logic_vector(2 downto 0) := "101";
    constant FUNC3_LHU      : std_logic_vector(2 downto 0) := "101";
    constant FUNC3_BGE      : std_logic_vector(2 downto 0) := "101";

    constant FUNC3_OR       : std_logic_vector(2 downto 0) := "110";
    constant FUNC3_BLTU     : std_logic_vector(2 downto 0) := "110";

    constant FUNC3_AND      : std_logic_vector(2 downto 0) := "111";
    constant FUNC3_BGEU     : std_logic_vector(2 downto 0) := "111";
    -- func 7
    constant FUNC7_DEFAULT  : std_logic_vector(6 downto 0) := B"000_0000";
    constant FUNC7_ADD      : std_logic_vector(6 downto 0) := B"000_0000";
    constant FUNC7_XOR      : std_logic_vector(6 downto 0) := B"000_0000";
    constant FUNC7_OR       : std_logic_vector(6 downto 0) := B"000_0000";
    constant FUNC7_AND      : std_logic_vector(6 downto 0) := B"000_0000";
    constant FUNC7_SLL      : std_logic_vector(6 downto 0) := B"000_0000";
    constant FUNC7_SRL      : std_logic_vector(6 downto 0) := B"000_0000";
    constant FUNC7_SLT      : std_logic_vector(6 downto 0) := B"000_0000";
    constant FUNC7_SLTU     : std_logic_vector(6 downto 0) := B"000_0000";
    constant FUNC7_SUBSRA   : std_logic_vector(6 downto 0) := B"010_0000";
    constant FUNC7_SUB      : std_logic_vector(6 downto 0) := B"010_0000";
    constant FUNC7_SRA      : std_logic_vector(6 downto 0) := B"010_0000";
end package;