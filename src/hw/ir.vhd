library ieee;
use ieee.std_logic_1164.all;


entity ir is
    port (
        clk:    in std_logic;				-- clock
        load:   in std_logic;				-- load signal
        ir_in:  in std_logic_vector (31 downto 0);	-- data input
        ir_out: out std_logic_vector (31 downto 0)	-- data output
    );
end ir;


architecture rtl of ir is
    
    signal instruction_registers: std_logic_vector(31 downto 0) := X"00000000";

begin
    ir_out <= instruction_registers;
    read: process(clk)
    begin
        if rising_edge(clk) and load = '1' then
            instruction_registers <= ir_in;
        end if;
    end process;
end;

