--! @file controller.vhd
--! @brief CPU Controller
--! @author Markus Wagner
--! @copyright See LICENSE file
library ieee;
use ieee.std_logic_1164.all;
use work.rv32i.all;
use work.simplerv32i_package.all;


--! @brief Controller of the cpu
--!
--! @details Manages all components (alu, regfile, ir, pc, pc_adder and several multiplexer)
--! of the cpu core, as necessary for the execution of all RISC V 32 Integer instructions
--! except of ebeak and ecall. The controller also implements a synchronous wishbone master,
--! which is necessary for fetching instructions, as well, as the execution of load and store
--! instructions. Wishbone and controller share the same clock.
entity controller is
    port(
        --! synchronous, active high reset
        rst:                in  std_logic;
        clk:                in  std_logic;
        instr:              in  std_logic_vector(31 downto 0);
        --! indicates whether branch condition ist met
        branchflag:         in  std_logic;
        --! wishbone write enable
        wb_we:              out std_logic;
        --! wishbone select byte(s) on dat_i/o line
        wb_sel:             out std_logic_vector(3 downto 0);
        --! wishbone strobe
        wb_stb:             out std_logic;
        --! wishbone bus cycle
        wb_cyc:             out std_logic;
        --! wishbone acknoledge (from slave)
        wb_ack:             in  std_logic;
        --! control wishbone address source mux: '1' from pc, '0' from alu
        c_adr_pc_not_alu:   out std_logic;
        --! control alu a input signal via mux: '1' from regfile, '0' from program counter
        c_alu_a_not_pc:     out std_logic;
        --! select alu b data source via mux
        c_alu_b:            out alub_sel;
        --! select alu to operate as adder or as compare unit
        c_alu_pc_not_branch:out std_logic;
        --! load ir on next rising clk edge
        c_ir_load:          out std_logic;
        --! '1': next pc should be loaded from pc + 4, '0': next pc should be loaded from alu output
        c_pc_not_alu:       out std_logic;
        --! select input for regfile
        c_regsortmux:       out regsortmux_sel;
        --! load input data into selected register on next rising clock edge
        ld_reg:             out std_logic;
        --! load program counter on next rising clock edge
        pc_load:            out std_logic
    );
end controller;


architecture rtl of controller is
    --! states of central controller fsm
    type t_ctrlstate is (
        s_start,            -- reset state
        s_fetchinstr,       -- requeset new instruction
        s_decodeinstr,      -- decode instruction after ack = '1'
        s_pcinc,            -- increment pc on next rising edge
        s_alu,              -- execute none-immediate alu instructions
        s_alui,             -- execute immediate alu instructions
        s_auipc,
        s_branch,           -- make branch compares
        s_branch_load_pc,   -- if should branch, load pc according to immediate offset
        s_jal,
        s_jalr,
        s_load_w,
        s_load_h,
        s_load_b,
        s_load_hu,
        s_load_bu,
        s_lui,
        s_store_w,
        s_store_h,
        s_store_b,
        s_handle_exception  --! excaption trap
    );
    signal curr_ctrl_state, next_ctrl_state : t_ctrlstate;
begin
    --! @brief State transfer and reset control
    state_transfer: process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                curr_ctrl_state <= s_start;
            else
                curr_ctrl_state <= next_ctrl_state;
            end if;
        end if;
    end process;
    --! @brief State transfer logic and output encoding
    state_transfer_logic: process(
        curr_ctrl_state,
        instr,
        branchflag,
        wb_ack
    )
    begin
        -- default output encoding
        wb_we <= '0';
        wb_sel <= "0000";
        wb_stb <= '0';
        wb_cyc <= '0';
        c_adr_pc_not_alu <= '1';
        c_alu_a_not_pc <= '1';
        c_alu_b <= alub_sel_rs2;
        c_alu_pc_not_branch <= '1';
        c_ir_load <= '0';
        c_pc_not_alu <= '1';
        c_regsortmux <= rsm_sel_alu;
        ld_reg <= '0';
        pc_load <= '0';
        next_ctrl_state <= curr_ctrl_state;
        case curr_ctrl_state is
            when s_start =>
                next_ctrl_state <= s_fetchinstr;
            when s_pcinc =>
                pc_load <= '1';
                next_ctrl_state <= s_fetchinstr;
            when s_fetchinstr =>
                wb_sel <= "1111";
                wb_cyc <= '1';
                wb_stb <= '1';
                if wb_ack = '1' then
                    c_ir_load <= '1';
                    next_ctrl_state <= s_decodeinstr;
                end if;
            when s_decodeinstr =>
                if instr(6 downto 0) = OPC_ALU then
                    next_ctrl_state <= s_alu;
                elsif instr(6 downto 0) = OPC_ALUI then
                    next_ctrl_state <= s_alui;
                elsif instr(6 downto 0) = OPC_LOAD then
                    if instr(14 downto 12) = FUNC3_LW then
                        next_ctrl_state <= s_load_w;
                    elsif instr(14 downto 12) = FUNC3_LH then
                        next_ctrl_state <= s_load_h;
                    elsif instr(14 downto 12) = FUNC3_LB then
                        next_ctrl_state <= s_load_b;
                    elsif instr(14 downto 12) = FUNC3_LHU then
                        next_ctrl_state <= s_load_hu;
                    elsif instr(14 downto 12) = FUNC3_LBU then
                        next_ctrl_state <= s_load_bu;
                    end if;
                elsif instr(6 downto 0) = OPC_STORE then
                    if instr(14 downto 12) = FUNC3_SW then
                        next_ctrl_state <= s_store_w;
                    elsif instr(14 downto 12) = FUNC3_SH then
                        next_ctrl_state <= s_store_h;
                    elsif instr(14 downto 12) = FUNC3_SB then
                        next_ctrl_state <= s_store_b;
                    end if;
                elsif instr(6 downto 0) = OPC_BRANCH then
                    next_ctrl_state <= s_branch;
                elsif instr(6 downto 0) = OPC_JAL then
                    next_ctrl_state <= s_jal;
                elsif instr(6 downto 0) = OPC_JALR then
                    next_ctrl_state <= s_jalr;
                elsif instr(6 downto 0) = OPC_LUI then
                    next_ctrl_state <= s_lui;
                elsif instr(6 downto 0) = OPC_AUIPC then
                    next_ctrl_state <= s_auipc;
                else
                    next_ctrl_state <= s_handle_exception;
                end if;
            when s_alu =>
                pc_load <= '1';
                ld_reg <= '1';
                next_ctrl_state <= s_fetchinstr;
            when s_alui =>
                pc_load <= '1';
                ld_reg <= '1';
                c_alu_b <= alub_sel_imm_i;
                next_ctrl_state <= s_fetchinstr;
            when s_auipc =>
                c_alu_a_not_pc <= '0';
                c_alu_b <= alub_sel_imm_u;
                ld_reg <= '1';
                pc_load <= '1';
                next_ctrl_state <= s_fetchinstr;
            when s_branch =>
                c_alu_pc_not_branch <= '0';
                c_alu_a_not_pc <= '1';
                if branchflag = '1' then
                    next_ctrl_state <= s_branch_load_pc;
                else
                    next_ctrl_state <= s_pcinc;
                end if;
            when s_branch_load_pc =>
                c_alu_a_not_pc <= '0';
                c_alu_b <= alub_sel_imm_b;
                c_pc_not_alu <= '0';
                pc_load <= '1';
                next_ctrl_state <= s_fetchinstr;
            when s_jal =>
                c_alu_a_not_pc <= '0';
                c_alu_b <= alub_sel_imm_j;
                c_pc_not_alu <= '0';
                c_regsortmux <= rsm_sel_pc;
                ld_reg <= '1';
                pc_load <= '1';
                next_ctrl_state <= s_fetchinstr;
            when s_jalr =>
                c_alu_a_not_pc <= '1';
                c_alu_b <= alub_sel_imm_i;
                c_pc_not_alu <= '0';
                c_regsortmux <= rsm_sel_alu;
                ld_reg <= '1';
                pc_load <= '1';
                next_ctrl_state <= s_fetchinstr;
            when s_load_w =>
                c_adr_pc_not_alu <= '0';    -- calc offset from register
                c_alu_b <= alub_sel_imm_i;  -- ...
                wb_cyc <= '1';              -- configure wishbone interface
                wb_stb <= '1';
                c_regsortmux <= rsm_sel_ld_w;
                wb_sel <= "1111";
                if wb_ack = '1' then
                    ld_reg <= '1';
                    next_ctrl_state <= s_pcinc;
                end if;
            when s_load_h =>
                c_adr_pc_not_alu <= '0';    -- calc offset from register
                c_alu_b <= alub_sel_imm_i;  -- ...
                wb_cyc <= '1';              -- configure wishbone interface
                wb_stb <= '1';
                c_regsortmux <= rsm_sel_ld_h;
                wb_sel <= "1111";
                if wb_ack = '1' then
                    ld_reg <= '1';
                    next_ctrl_state <= s_pcinc;
                end if;
            when s_load_b =>
                c_adr_pc_not_alu <= '0';    -- calc offset from register
                c_alu_b <= alub_sel_imm_i;  -- ...
                wb_cyc <= '1';              -- configure wishbone interface
                wb_stb <= '1';
                c_regsortmux <= rsm_sel_ld_b;
                wb_sel <= "1111";
                if wb_ack = '1' then
                    ld_reg <= '1';
                    next_ctrl_state <= s_pcinc;
                end if;
            when s_load_hu =>
                c_adr_pc_not_alu <= '0';    -- calc offset from register
                c_alu_b <= alub_sel_imm_i;  -- ...
                wb_cyc <= '1';              -- configure wishbone interface
                wb_stb <= '1';
                c_regsortmux <= rsm_sel_ld_hu;
                wb_sel <= "1111";
                if wb_ack = '1' then
                    ld_reg <= '1';
                    next_ctrl_state <= s_pcinc;
                end if;
            when s_load_bu =>
                c_adr_pc_not_alu <= '0';    -- calc offset from register
                c_alu_b <= alub_sel_imm_i;  -- ...
                wb_cyc <= '1';              -- configure wishbone interface
                wb_stb <= '1';
                c_regsortmux <= rsm_sel_ld_bu;
                wb_sel <= "1111";
                if wb_ack = '1' then
                    ld_reg <= '1';
                    next_ctrl_state <= s_pcinc;
                end if;
            when s_lui =>
                c_regsortmux <= rsm_sel_lui;
                ld_reg <= '1';
                pc_load <= '1';
                next_ctrl_state <= s_fetchinstr;
            when s_store_w =>
                c_adr_pc_not_alu <= '0';    -- calc offset from register
                c_alu_b <= alub_sel_imm_s;  -- ...
                wb_cyc <= '1';              -- configure wishbone interface
                wb_stb <= '1';
                wb_we <= '1';
                wb_sel <= "1111";
                if wb_ack = '1' then
                    next_ctrl_state <= s_pcinc;
                end if;
            when s_store_h =>
                c_adr_pc_not_alu <= '0';    -- calc offset from register
                c_alu_b <= alub_sel_imm_s;  -- ...
                wb_cyc <= '1';              -- configure wishbone interface
                wb_stb <= '1';
                wb_we <= '1';
                wb_sel <= "1100";
                if wb_ack = '1' then
                    next_ctrl_state <= s_pcinc;
                end if;
            when s_store_b =>
                c_adr_pc_not_alu <= '0';    -- calc offset from register
                c_alu_b <= alub_sel_imm_s;  -- ...
                wb_cyc <= '1';              -- configure wishbone interface
                wb_stb <= '1';
                wb_we <= '1';
                wb_sel <= "1000";
                if wb_ack = '1' then
                    next_ctrl_state <= s_pcinc;
                end if;
            when s_handle_exception =>
                next_ctrl_state <= s_handle_exception;
        end case;
    end process;
end;