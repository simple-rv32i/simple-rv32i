--! @file pc.vhd
--! @brief Program counter
--! @details Program counter for a cpu
--! @author
--! @copyright See LICENSE file
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity pc is
		port(
					clk:        in  std_logic; --clock signal
					rst:      in  std_logic; -- reset pc
					load:        in  std_logic; -- load data
					add_in:     in std_logic_vector(31 downto 0); -- input data
					add_out:     out std_logic_vector(31 downto 0) -- output data
			);
end pc;


architecture rtl of pc is

		signal add_cur: std_logic_vector(31 downto 2); -- current data

begin
		process(clk)
		begin
				if rising_edge(clk) then
						-- load
						if load = '1' then
								add_cur <= add_in(31 downto 2);
						end if;

						-- reset 
						if rst = '1' then
								add_cur <= (others => '0');
						end if;
				end if;

		end process;

		--output 
		add_out <= add_cur & "00";

end rtl;
