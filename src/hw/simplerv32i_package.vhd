library ieee;
use ieee.std_logic_1164.all;


package simplerv32i_package is
    type regsortmux_sel is (rsm_sel_alu, rsm_sel_lui, rsm_sel_ld_w,
        rsm_sel_ld_h, rsm_sel_ld_b, rsm_sel_ld_hu, rsm_sel_ld_bu, rsm_sel_pc
    );
    type alub_sel is (alub_sel_rs2, alub_sel_imm_i, alub_sel_imm_j,
            alub_sel_imm_u, alub_sel_imm_b, alub_sel_imm_s);
end package simplerv32i_package;