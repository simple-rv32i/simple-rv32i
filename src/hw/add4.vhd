library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity add4 is
		port(
					pc_in:		in std_logic_vector(31 downto 0); -- output from pc as intput
					add_out:     out std_logic_vector(31 downto 0) -- output data
			);
end add4;


architecture rtl of add4 is


begin

		add_out <= std_logic_vector(unsigned(pc_in)+to_unsigned(4,add_out'length));

end rtl;
