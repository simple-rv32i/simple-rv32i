library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.simplerv32i_package.all;

entity riscy_cpu is
    port (
        clk     : in std_logic;
        reset   : in std_logic;
        adr     : out std_logic_vector (31 downto 0);
        data_i  : in std_logic_vector (31 downto 0);
        data_o  : out std_logic_vector (31 downto 0);
        we      : out std_logic;
        sel     : out std_logic_vector(3 downto 0);
        stb     : out std_logic;
        cyc     : out std_logic;
        ack     : in  std_logic;
        dbg_ld_ir  : out std_logic;
        dbg_pc_not_alu: out std_logic;
        dbg_ld_pc: out std_logic;
        dbg_adr_pc_not_alu: out std_logic
    );
end riscy_cpu;

architecture RTL of riscy_cpu is

    component ir
        port(
            clk	    : in std_logic;
            load	: in std_logic;
            ir_in	: in std_logic_vector(31 DOWNTO 0);
            ir_out	: out std_logic_vector(31 DOWNTO 0)
        );
    end component;

    component pc
        port(
            clk     : in  std_logic;
            rst     : in  std_logic;
            load    : in  std_logic;
            add_in  : in std_logic_vector(31 downto 0);
            add_out : out std_logic_vector(31 downto 0)      
        );
    end component;

    component add4
        port(
            pc_in   : in std_logic_vector(31 downto 0);
            add_out : out std_logic_vector(31 downto 0)
        );
    end component;


    component regfile
        port(
            clk     : in std_logic;
            sel_rd  : in std_logic_vector (4 downto 0);      
            sel_rs1 : in std_logic_vector (4 downto 0);      
            sel_rs2 : in std_logic_vector (4 downto 0);     
            data_in : in std_logic_vector (31 downto 0);   
            load    : in std_logic;               	    	   
            rs1     : out std_logic_vector (31 downto 0);     
            rs2     : out std_logic_vector (31 downto 0)  
        );
    end component;		


    component alu
        port(
            a             : in std_logic_vector (31 downto 0);
            b             : in std_logic_vector (31 downto 0);
            inst          : in std_logic_vector (16 downto 0);
            secondCycle   : in std_logic;
            y             : out std_logic_vector (31 downto 0);
            branch        : out std_logic
        );
    end component;

    component controller
        port(
            rst                     : in  std_logic;
            clk                     : in  std_logic;
            instr                   : in  std_logic_vector(31 downto 0);
            branchflag              : in  std_logic;
            wb_we                   : out std_logic;
            wb_sel                  : out std_logic_vector(3 downto 0);
            wb_stb                  : out std_logic;
            wb_cyc                  : out std_logic;
            wb_ack                  : in  std_logic;
            c_adr_pc_not_alu        : out std_logic;
            c_alu_a_not_pc          : out std_logic;
            c_alu_pc_not_branch     : out std_logic;
            c_alu_b                 : out alub_sel;
            c_ir_load               : out std_logic;
            c_pc_not_alu            : out std_logic;
            c_regsortmux            : out regsortmux_sel;
            ld_reg                  : out std_logic;
            pc_load                 : out std_logic   
        );
    end component;

    component regsortmux
        port(
            sel:        in  regsortmux_sel;
            wb_data:    in  std_logic_vector(31 downto 0);
            instr:      in  std_logic_vector(31 downto 0);
            pc:         in  std_logic_vector(31 downto 0);
            alu:        in  std_logic_vector(31 downto 0);
            outword:    out std_logic_vector(31 downto 0)
        );
    end component;
 
    -- control signals
    signal c_adr_pc_not_alu_sig, c_alu_a_not_pc_sig, c_alu_pc_not_branch_sig, c_ir_load_sig, c_pc_not_alu_sig, ld_reg_sig, pc_load_sig : std_logic;

    signal c_alu_b_sig: alub_sel;
    signal c_regsortmux_sig: regsortmux_sel;

    -- wishbone signals
    signal wb_we, wb_sel, wb_stb, wb_cyc, wb_ack: std_logic;
    signal data_i_be: std_logic_vector(31 downto 0);
    
    --  output signals from the components
    signal ir_out_sig, regfile_rs1_sig, regfile_rs2_sig, alu_out_sig, pc_out_sig, add4_out_sig : std_logic_vector(31 downto 0); 

    -- input signals from the components
    signal regfile_in_sig, pc_in_sig, alu_a_in_sig, alu_b_in_sig: std_logic_vector(31 downto 0);
    
    -- flag from alu
    signal branch_sig : std_logic;

    for IMPL_IR: ir use entity WORK.ir(RTL);
    for IMPL_REGFILE: regfile use entity WORK.regfile(RTL);
    for IMPL_ALU: alu use entity WORK.alu(RTL);
    for IMPL_PC: pc use entity WORK.pc(RTL);
    for IMPL_ADD4: add4 use entity WORK.add4(RTL);
    for IMPL_CONTROLLER: controller use entity WORK.controller(RTL);


    begin
        dbg_ld_ir <= c_ir_load_sig;
        dbg_pc_not_alu <= c_pc_not_alu_sig;
        dbg_ld_pc <= pc_load_sig;
        dbg_adr_pc_not_alu <= c_adr_pc_not_alu_sig;
    
        data_i_be <= data_i(7 downto 0) & data_i(15 downto 8) & data_i(23 downto 16) & data_i(31 downto 24);
        data_o <= regfile_rs2_sig(7 downto 0) & regfile_rs2_sig(15 downto 8) & regfile_rs2_sig(23 downto 16) & regfile_rs2_sig(31 downto 24);

        IMPL_IR: ir port map(
            clk => clk,
            ir_in => data_i_be,
            load => c_ir_load_sig,
            ir_out => ir_out_sig
        );
        
        IMPL_REGFILE: regfile port map(
            clk => clk,
            data_in => regfile_in_sig,
            sel_rd => ir_out_sig(11 downto 7),
            rs1 => regfile_rs1_sig,
            sel_rs1 => ir_out_sig(19 downto 15),
            rs2 => regfile_rs2_sig,
            sel_rs2 => ir_out_sig(24 downto 20),
            load => ld_reg_sig
        );

        IMPL_ALU: alu port map(
            a => alu_a_in_sig,
            b => alu_b_in_sig,
            inst => ir_out_sig(31 downto 25) & ir_out_sig(14 downto 12) & ir_out_sig(6 downto 0),
            secondCycle => c_alu_pc_not_branch_sig,
            y => alu_out_sig, branch => branch_sig
        );
        
        IMPL_PC: pc port map(
            clk => clk,
            rst => reset,
            load => pc_load_sig,
            add_in => pc_in_sig,
            add_out => pc_out_sig
        );

        IMPL_ADD4: add4 port map(
            pc_in => pc_out_sig,
            add_out => add4_out_sig
        );

        IMPL_CONTROLLER: controller port map(
            clk => clk,
            rst => reset,
            instr => ir_out_sig,
            branchflag => branch_sig,
            wb_we => we,
            wb_sel => sel,
            wb_stb => stb,
            wb_cyc => cyc,
            wb_ack => ack,
            c_adr_pc_not_alu => c_adr_pc_not_alu_sig,
            c_alu_a_not_pc => c_alu_a_not_pc_sig,
            c_alu_pc_not_branch => c_alu_pc_not_branch_sig,
            c_alu_b => c_alu_b_sig,
            c_ir_load => c_ir_load_sig,
            c_pc_not_alu => c_pc_not_alu_sig,
            c_regsortmux => c_regsortmux_sig,
            ld_reg => ld_reg_sig,
            pc_load => pc_load_sig
        );

        IMPL_REGSORTMUX: regsortmux port map(
            sel => c_regsortmux_sig,
            wb_data => data_i,
            instr => ir_out_sig,
            pc => add4_out_sig,
            alu => alu_out_sig,
            outword => regfile_in_sig
        );

        -- multiplexer for PC input
        process (add4_out_sig, alu_out_sig, c_pc_not_alu_sig)
        begin 
            if c_pc_not_alu_sig = '1' then
                pc_in_sig <= add4_out_sig;
            else
                pc_in_sig <= alu_out_sig;
            end if;
        end process;

        -- multiplexer for ALU input a
        process (pc_out_sig, regfile_rs1_sig, c_alu_a_not_pc_sig)
        begin 
            if c_alu_a_not_pc_sig = '1' then
                alu_a_in_sig <= regfile_rs1_sig;   
            else
                alu_a_in_sig <= pc_out_sig;      
            end if;
        end process;    

        -- multiplexer for ALU input b
        process (ir_out_sig, regfile_rs2_sig, c_alu_b_sig)
        begin 
            case c_alu_b_sig is               
                when alub_sel_rs2 =>
                    alu_b_in_sig <= regfile_rs2_sig;
                when alub_sel_imm_i =>
                    alu_b_in_sig(31 downto 12) <= (others => ir_out_sig(31));
                    alu_b_in_sig(11 downto 0) <= ir_out_sig(31 downto 20);
                when alub_sel_imm_j =>
                    alu_b_in_sig(31 downto 20) <= (others => ir_out_sig(31));
                    alu_b_in_sig(19 downto 12) <= ir_out_sig(19 downto 12);
                    alu_b_in_sig(11) <= ir_out_sig(20);
                    alu_b_in_sig(10 downto 1) <= ir_out_sig(30 downto 21);
                    alu_b_in_sig(0) <= '0';
                when alub_sel_imm_u =>
                    alu_b_in_sig <= ir_out_sig(31 downto 12) & "000000000000";
                when alub_sel_imm_b =>
                    alu_b_in_sig(31 downto 12)  <= (others => ir_out_sig(31));
                    alu_b_in_sig(11 downto 1) <= ir_out_sig(7) & ir_out_sig(30 downto 25) & ir_out_sig(11 downto 8);
                    alu_b_in_sig(0) <= '0';  
                when alub_sel_imm_s =>
                    alu_b_in_sig(31 downto 12)  <= (others => ir_out_sig(31));	
                    alu_b_in_sig(11 downto 0) <=  ir_out_sig(31 downto 25) & ir_out_sig(11 downto 7);
                when others =>
                alu_b_in_sig <= X"00000000";
            end case;
        end process;           

        
        -- multiplexer for Adressbus...
        process (alu_out_sig, pc_out_sig, c_adr_pc_not_alu_sig)
        begin 
            if c_adr_pc_not_alu_sig = '1' then 
                adr <= pc_out_sig;
            else 
                adr <= alu_out_sig;
            end if;
        end process;     

end RTL;