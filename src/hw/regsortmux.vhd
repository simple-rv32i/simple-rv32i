library ieee;
use ieee.std_logic_1164.all;
use work.simplerv32i_package.all;


entity regsortmux is
    port(
        sel:        in  regsortmux_sel;
        wb_data:    in  std_logic_vector(31 downto 0);
        instr:      in  std_logic_vector(31 downto 0);
        pc:         in  std_logic_vector(31 downto 0);
        alu:        in  std_logic_vector(31 downto 0);
        outword:    out std_logic_vector(31 downto 0)
    );
end entity regsortmux;


architecture rtl of regsortmux is
begin
    process(sel, wb_data, instr, pc, alu)
    begin
        case sel is
            when rsm_sel_alu =>
                outword <= alu;
            when rsm_sel_lui =>
                outword(31 downto 12) <= instr(31 downto 12);
                outword(11 downto 0)  <= (others => '0');
            when rsm_sel_ld_w =>
                outword <= wb_data(7 downto 0) & wb_data(15 downto 8) & wb_data(23 downto 16) & wb_data(31 downto 24);
            when rsm_sel_ld_h =>
                outword(31 downto 16) <= (others => wb_data(23));
                outword(15 downto 0)  <= wb_data(23 downto 16) & wb_data(31 downto 24);
            when rsm_sel_ld_b =>
                outword(31 downto 8) <= (others => wb_data(31));
                outword(7 downto 0)  <= (wb_data(31 downto 24));
            when rsm_sel_ld_hu =>
                outword(31 downto 16) <= (others => '0');
                outword(15 downto 0)  <= wb_data(23 downto 16) & wb_data(31 downto 24);
            when rsm_sel_ld_bu =>
                outword(31 downto 8) <= (others => '0');
                outword(7 downto 0)  <= (wb_data(31 downto 24));
            when rsm_sel_pc =>
            outword <= pc;
        end case;
    end process;
end architecture rtl;