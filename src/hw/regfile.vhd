library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity regfile is
		port (
					 clk: in std_logic;
					 sel_rd: in std_logic_vector (4 downto 0);       -- destination register
					 sel_rs1: in std_logic_vector (4 downto 0);      -- select source 1 register
					 sel_rs2: in std_logic_vector (4 downto 0);      -- select source 2 register
					 data_in: in std_logic_vector (31 downto 0);     -- data input 
					 load: in std_logic;               	    	    -- load data into rd
					 rs1: out std_logic_vector (31 downto 0);        -- data output 1
					 rs2: out std_logic_vector (31 downto 0)         -- data output 2
			 );
end regfile;

architecture rtl of regfile is
		type t_regfile is array (1 to 31) of std_logic_vector(31 downto 0); 
		signal reg: t_regfile; 
begin
		-- process to load regfile
		process (clk)
		begin 
				if rising_edge (clk) then
						if load = '1' and to_integer(unsigned(sel_rd)) > 0 then
								reg(to_integer (unsigned (sel_rd))) <= data_in;
						end if;
				end if;
		end process;

		-- process to set the output
		process (sel_rs1, sel_rs2, reg)
		begin
			if to_integer(unsigned(sel_rs1)) > 0 then
				rs1 <= reg(to_integer(unsigned(sel_rs1)));
			else
				rs1 <= X"0000_0000";
			end if;
			if to_integer(unsigned(sel_rs2)) > 0 then
				rs2 <= reg(to_integer(unsigned(sel_rs2)));
			else
				rs2 <= X"0000_0000";
			end if;
		end process;

end rtl;
