library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity alu is
    port (
        a   : in std_logic_vector (31 downto 0);	-- data input rs1
        b   : in std_logic_vector (31 downto 0);	-- data input rs2
        inst : in std_logic_vector (16 downto 0); 	-- instruction input
		secondCycle: in std_logic; 					-- flag for second Cycle
        y   : out std_logic_vector (31 downto 0);	-- data output rd
        branch: out std_logic						-- branch signal
    );
end alu;


architecture rtl of alu is
 
    -------------------------------------------------------------------------------------------------------------------
    --/ funct7 (31:25)) / rs2 (24:20) / rs1 (19:15) / functf3 (14:12) /      rd (11:7)      / opcode (6:0) /--   R-type  
    -------------------------------------------------------------------------------------------------------------------
    --/       imm (31:20)[11:0]       / rs1 (19:15) / functf3 (14:12) /     rd (11:7)       / opcode (6:0) /--   I-type
    -------------------------------------------------------------------------------------------------------------------
    --/ imm (31:25))[11:5] / rs2 (24:20) / rs1 (19:15) / functf3 (14:12) / imm (11:7)[4:0]  / opcode (6:0) /--   S-type
    -------------------------------------------------------------------------------------------------------------------
    --/imm (31:25))[12|10:5]/rs2 (24:20)/rs1 (19:15)/ functf3 (14:12) / imm (11:7)[4:1|11]  / opcode (6:0) /--   B-type
    -------------------------------------------------------------------------------------------------------------------
    --/                         imm (31:12)[31:12]                              / rd (11:7) / opcode (6:0) /--   U-type
    -------------------------------------------------------------------------------------------------------------------
    --/                         imm (31:12)[20|10:1|11|19:12]                   / rd (11:7) / opcode (6:0) /--   J-type
    ---------------------------------------------------------------------------------.........-------------------------
    
     
     --opcode
    constant OPC_NIM: std_logic_vector(6 downto 0) := "0110011";
    constant OPC_IM: std_logic_vector(6 downto 0) := "0010011";
    constant OPC_LD: std_logic_vector(6 downto 0) := "0000011";
    constant OPC_STR: std_logic_vector(6 downto 0) := "0100011";
    constant OPC_BR: std_logic_vector(6 downto 0) := "1100011";
    constant OPC_JAL: std_logic_vector(6 downto 0) := "1101111";
    constant OPC_JALR: std_logic_vector(6 downto 0) := "1100111";
    constant OPC_AUIPC: std_logic_vector(6 downto 0) := "0010111";


 
    --funct7 (31:25 => 7 Bit) & funct3 (14:12 => 3 Bit) 
    constant ADD_OPC: std_logic_vector(9 downto 0) := "0000000000";
    constant SUB_OPC: std_logic_vector(9 downto 0) := "0100000000"; 
    constant XOR_OPC: std_logic_vector(9 downto 0) := "0000000100";
    constant OR_OPC: std_logic_vector(9 downto 0) := "0000000110";
    constant AND_OPC: std_logic_vector(9 downto 0) := "0000000111";
    constant SLL_OPC: std_logic_vector(9 downto 0) := "0000000001"; 
    constant SRL_OPC: std_logic_vector(9 downto 0) := "0000000101";
    constant SRA_OPC: std_logic_vector(9 downto 0) := "0100000101";
    constant SLT_OPC: std_logic_vector(9 downto 0) := "0000000010";
    constant SLTU_OPC: std_logic_vector(9 downto 0) := "0000000011";

	constant INST_ADDI: std_logic_vector(2 downto 0) := "000"; --0x0
	constant INST_XORI: std_logic_vector(2 downto 0) := "100"; --0x4
	constant INST_ORI: std_logic_vector(2 downto 0) := "110"; --0x6
	constant INST_ANDI: std_logic_vector(2 downto 0) := "111"; --0x7
	constant INST_SLLI: std_logic_vector(2 downto 0) := "001"; --imm[5:11]=0x00, 0x1
	constant INST_SRXI: std_logic_vector(2 downto 0) := "101"; --imm[5:11]=0x00, 0x5
	constant INST_SLTI: std_logic_vector(2 downto 0) := "010"; -- 0x2
	constant INST_SLTIU: std_logic_vector(2 downto 0) := "011"; --0x3
    
    --branch
    --funct7 (14:12 => 3 Bit)
    constant BEQ_OPC: std_logic_vector(2 downto 0) := "000";
    constant BNE_OPC: std_logic_vector(2 downto 0) := "001";
    constant BLT_OPC: std_logic_vector(2 downto 0) := "100";
    constant BGE_OPC: std_logic_vector(2 downto 0) := "101";
    constant BLTU_OPC: std_logic_vector(2 downto 0) := "110";
    constant BGEU_OPC: std_logic_vector(2 downto 0) := "111";

    --temp buffer
    signal a_sig: std_logic_vector(31 downto 0);
    signal b_sig: std_logic_vector(31 downto 0);
    
    signal sel_opc: std_logic_vector(6 downto 0);
    signal sel_func3: std_logic_vector(2 downto 0);
	signal sel_func7: std_logic_vector(6 downto 0);
	--signal secondCycle: std_logic := '0';

begin 
	sel_func3 <= inst(9 downto 7);
	sel_func7 <= inst(16 downto 10);
    OPERATION_MUX: process(a, b, a_sig, b_sig, inst, sel_opc, sel_func3, sel_func7, secondCycle)

    begin
		sel_opc <= inst(6 downto 0);
		y <= (others => '0');
		branch <= '1';
		case sel_opc is
			when OPC_NIM =>
					
				case sel_func7 & sel_func3 is
					when ADD_OPC =>
						y <= std_logic_vector(signed(a) + signed(b));
					when SUB_OPC =>
						y <= std_logic_vector(signed(a) - signed(b));
					when XOR_OPC =>
						y <= a xor b;
					when OR_OPC =>
						y <= a or b;
					when AND_OPC =>
						y <= a and b;          
					when SLL_OPC =>
						y <= std_logic_vector(shift_left(unsigned(a), to_integer(unsigned(b(4 downto 0)))));
					when SRL_OPC =>
						y <= std_logic_vector(shift_right(unsigned(a), to_integer(unsigned(b(4 downto 0)))));
					when SRA_OPC =>
						y <= std_logic_vector(shift_right(signed(a), to_integer(unsigned(b(4 downto 0)))));
					when SLT_OPC =>
						if(signed(a) < signed(b)) then
							y <= X"00000001";
							
						else
							y <= X"00000000";
						end if;
					when SLTU_OPC =>
						if(unsigned(a) < unsigned(b)) then
							y <= X"00000001";
						else
							y <= X"00000000";
						end if;
					when others =>
						y <= X"00000000";
				end case;
			
			when OPC_IM =>
					
				case sel_func3 is
					when INST_ADDI =>
						y <= std_logic_vector(signed(a) + signed(b));
					when INST_XORI =>
						y <= a xor b;
					when INST_ORI =>
						y <= a or b;
					when INST_ANDI =>
						y <= a and b;          
					when INST_SLLI =>
						if sel_func7 = B"000_0000" then
							y <= std_logic_vector(shift_left(unsigned(a), to_integer(unsigned(b(4 downto 0)))));
						end if;
					when INST_SRXI =>
						if sel_func7 = B"000_0000" then
							y <= std_logic_vector(shift_right(unsigned(a), to_integer(unsigned(b(4 downto 0)))));
						elsif sel_func7 = B"010_0000" then
							y <= std_logic_vector(shift_right(signed(a), to_integer(unsigned(b(4 downto 0)))));
						end if;
					when INST_SLTI =>		
						if(signed(a) < signed(b)) then
							y <= X"00000001";
						else
							y <= X"00000000";
						end if;
					when INST_SLTIU =>
						if(unsigned(a) < unsigned(b)) then
							y <= X"00000001";
						else
							y <= X"00000000";
						end if;
					when others =>
						y <= X"00000000";
				end case;
			
			when OPC_LD =>				
				y <= std_logic_vector(signed(a) + signed(b));	
			when OPC_STR =>	
				y <= std_logic_vector(signed(a) + signed(b));
			when OPC_BR =>
				y <= X"00000000";
				
				if secondCycle = '0' then
					case sel_func3 is
						when BEQ_OPC =>
							if(a = b) then
								branch <= '1';
							else
								branch <= '0';
							end if;
						when BNE_OPC =>
							if(a /= b) then
								branch <= '1';
							else 
								branch <= '0';
							end if;
						when BLT_OPC =>
							if(signed(a) < signed(b)) then
								branch <= '1';	
							else
								branch <= '0';
							end if;    	    
						when BGE_OPC =>
							if(signed(a) >= signed(b)) then
								branch <= '1';                
							else
								branch <= '0';
							end if;
						when BLTU_OPC =>
							if(unsigned(a) < unsigned(b)) then
								branch <= '1';
							else 
								branch <= '0';
							end if;
						when BGEU_OPC =>
							if(unsigned(a) >= unsigned(b)) then
								branch <= '1';
							else 
								branch <= '0';
							end if;
						when others =>
							branch <= '0';
					end case;
				else
					branch <= '0';
					y <= std_logic_vector(signed(a) + signed(b));
				end if;
			when OPC_JAL => 				
				y <= std_logic_vector(signed(a) + signed(b));	      
			when OPC_JALR =>   			
				y <= std_logic_vector(signed(a) + signed(b));
			when OPC_AUIPC =>   			
				y <= std_logic_vector(signed(a) + signed(B));
			when others =>
				y <= X"00000000";     
		end case;
    end process;    
end;

