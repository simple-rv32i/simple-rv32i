# Arithmetic Logic Unit (ALU)
[Author: Markus Wagner]
[Status: Draft, not approved]

## Interface description

![ALU entity](graphics/alu.svg)

```vhdl
library ieee;
use ieee.std_logic_1164.all;
-- <cut out>
entity alu is
    port(
        a, b:   in  std_logic_vector(31 downto 0);
        instr:  in  std_logic_vector(31 dwonto 0);
        output: out std_logic_vector(31 downto 0);
        branch: out std_logic
    );
end alu;
```

The `ALU` does not distinguish between immediate and normal operations. The input multiplexers must take this role (controlled by the controller) and also must fill in missing zeros or ones depending on the `MSB` of the immediate and the instruction.

[Instruction to operation mapping of `ALU`]
| instruction(s)| output    | branch    |
|---------------|-----------|-----------|
| `ADD`/`ADDI ` | a + b     | `'X'`     |
| `SUB`/`SUBI`  | a - b     | `'X'`     |
| `XOR`/`XORI`  | a ^ b     | `'X'`     |
| `OR`/`ORI`    | a \| b    | `'X'`     |
| `AND`/`ANDI`  | a & b     | `'X'`     |
| `SLL`/`SLLI`  | a << b    | `'X'`     |
| `SRL`/`SRLI`  | a >> b    | `'X'`     |
| `SRA`/`SRAI`  | a >> b, will with `1` if negative, else `0` | `'X'` |
| `SLT`/`SLTI`  | (a < b) ? `X"0001"`:`X"0000"` (signed compare)| `'X'` |
| `SLTU`/`SLTUI`| (a < b) ? `X"0001"`:`X"0000"` (unsigned compare)| `'X'` |
| `BEQ`         | `X"XXXX"` | (a == b) ? `1`:`0` |
| `BNE`         | `X"XXXX"` | (a != b) ? `1`:`0` |
| `BLT`         | `X"XXXX"` | (a < b) ? `1`:`0` (signed) |
| `BGE`         | `X"XXXX"` | (a >= b) ? `1`:`0` (signed) |
| `BLTU`        | `X"XXXX"` | (a < b) ? `1`:`0` (unsigned) |
| `BGEU`        | `X"XXXX"` | (a >= b) ? `1`:`0` (unsigned) |
| `LB`/`LH`/`LW`/`LBU`/`LHU`| a + b | `'X'`   |
| `SB`/`LH`/`LW`| a + b     | `'X'`     |
| `JAL`/`JALR`  | not specified yet | `'X'` |
| `LUI`/`AUIPC`  | not specified yet | `'X'` |
| `ECALL`/`EBEAK`| `X"XXXX"`| `'X'`     |
