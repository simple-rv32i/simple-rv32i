\ProvidesClass{hwsys-protocol}[2022/05/25 Protocol template for Hardware Systeme lecture]
\NeedsTeXFormat{LaTeX2e}

\LoadClass[a4paper]{article}
\RequirePackage{geometry}

\setlength{\voffset}{-0.5cm}
\setlength{\textheight}{24cm}

%\RequirePackage{fancybox}
\RequirePackage{fancyhdr}

\def\title#1{\def\title{#1}}
\def\author#1{\def\author{#1}}
\def\matrnr#1{\def\matrnr{#1}}
\def\studiengang#1{\def\studiengang{#1}}
\def\date#1{\def\date{#1}}
\def\labname#1{\def\labname{#1}}
\def\subtitle#1{\def\subtitle{#1}}
\def\reportpart#1{\def\reportpart{#1}}
\def\groupname#1{\def\groupname{#1}}
\def\supervisor#1{\def\supervisor{#1}}

\makeatletter
\renewcommand\maketitle{
	\thispagestyle{empty}
	{\raggedright % Note the extra {
			\begin{center}
				{\bfseries \sffamily \labname}\\[2ex]
				{\Huge \bfseries \sffamily \title }\\[4ex]
				{\Large \bfseries \sffamily \subtitle}\\[4ex]
				{\Large \sffamily \reportpart}\\[4ex]
				{\Large\begin{tabular}{r l}
					Name: & \hspace{1em}\author \\
					Matrikel-Nr.: & \hspace{1em}\matrnr \\
					studiengang: & \hspace{1em}\studiengang \\
					Gruppe: & \hspace{1em}\groupname \\
					Datum: & \hspace{1em}\date \\
					Betreuer: & \hspace{1em}\supervisor
				\end{tabular}}\\[6ex]
	\end{center}}} % Note the extra }
\makeatother

\pagestyle{fancy}
% clear default contents
\fancyhf{}
\fancyhead[R]{\title\space}
%\fancyhead[C]{\labname}
\fancyhead[L]{\labname}
\fancyfoot[L]{\author}
\fancyfoot[C]{\groupname}
\fancyfoot[R]{\thepage}

\renewcommand{\headrulewidth}{1pt}
\renewcommand{\footrulewidth}{1pt}

\pagenumbering{arabic}