# Entwurf Gliederung

## Einführung und Motivation

### Warum RISC-V?

* FOSS
* (Auch) als Softcore in FPGA-Projekten verwendbar
* Im Gegensatz zu VISCY vorhandene (Compiler)-Toolchain
* Warum RV32I? -> Aufwand und Platz auf FPGA

## Beschreibung von Recherche, Lösungen, Vorgehen und Ergebnisse

### Ziel

* NAME! ;)
* Minimalziel: die ersten 10 Zahlen der Fibonacci-Reihe mit einem Assembler-Programm ausrechnen auf FPGA-Board, vollständiger RV32I nicht notwendig
* Optional:
    * RV32I vollständig (vermutlich ohne ECALL und EBREAK)
    * Programm in Hochsprache implementieren (C oder Rust)

### Übersicht über RV32I ISA

* 6 Haupttypen an Befehlen
* Überblich und Priorisierung der einzelnen Befehle

### Wishbone

* Aufbau und Funktion
* Auslegung

### ParaNut Umgebung

* Adoption ausgewählter Periphrie (Speicher, ?), evtl. Simulator
* "Laufzeitumgebung" für das FPGA-Board

### Aufbau und Implementierung CPU

* Grundstruktur des VISCY
* Anpassung an Wishbone-Schnittstelle
* Erweiterung der Funktionalität für RV32I ISA
* Zu jeder Komponente Testbench
* CPU-Testbench

#### Gesamtstruktur

* TB
* Impl
* Wishbone-Anbindung (Bustakt = Systemtakt)
* Multiplexer in Datenleitungen

#### PC

#### IR

#### ALU

#### Statusregister (nur zum Debuggen der Hardware)?

#### Register

#### Steuerwerk

* Wishbone-Anbindung

## Vorführung/ Demo

## Zusammenfassung und Fazit

## Literatur

* Links zu RISC-V Spezification, Wishbone, etc.