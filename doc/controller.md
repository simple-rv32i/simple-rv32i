# Controller

## Instruction cycles

### ALU

![ALU instruction cycle](graphics/generated/instrcyc_alu.svg)

### Load

![Load instruction cycle](graphics/generated/instrcyc_load.svg)

### Store

![Store instruction cycle](graphics/generated/instrcyc_store.svg)

### Branch

![Branch (no jump) instruction cycle](graphics/generated/instrcyc_branchfalse.svg)
![Branch (jump) instruction cycle](graphics/generated/instrcyc_branchtrue.svg)

### Jump and link

![Jump and link instruction cycle](graphics/generated/instrcyc_jal.svg)

### Jump and link register

![Jump and link register instruction cycle](graphics/generated/instrcyc_jalr.svg)

### Load upper immediate

![LUI instruction cycle](graphics/generated/instrcyc_lui.svg)

### Add upper immediate to program counter

![AUIPC instruction cycle](graphics/generated/instrcyc_auipc.svg)
