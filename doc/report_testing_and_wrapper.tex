% !TeX spellcheck = de_DE_frami
\documentclass{hwsys-protocol}
\usepackage[ngerman]{babel}
\usepackage[autostyle=true,german=guillemets]{csquotes}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{pifont}
\usepackage{graphicx}
\graphicspath{ {./graphics/} }
\usepackage{svg}
\usepackage{pdfpages}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{siunitx}
\usepackage[
type={CC},
modifier={by-sa},
version={4.0},
]{doclicense}
\usepackage[hidelinks]{hyperref}
\usepackage{acronym}
\usepackage[backend=bibtex,style=ieee]{biblatex}
\bibliography{IEEEabrv,report_testing_and_wrapper}
\usepackage{tikz}
\usetikzlibrary{automata, positioning, arrows}
\title{RISC-V: eigene Implementierung}
\subtitle{Der RISCY-Prozessor}
\labname{Hardware-Systeme im SoSe 2022}
\reportpart{Teil 3 - Tests, Simulation und Implementierung auf einem FPGA}
\groupname{Alexander Staib, Aburrahman Celep, Markus Wagner}
\supervisor{Prof. Dr. Gundolf Kiefer}
\author{Markus Wagner}
\matrnr{2131731}
\studiengang{Master of Applied Research}
\date{\today}

\DeclareSIUnit{\bit}{Bit}
\DeclareSIUnit{\byte}{B}

\begin{document}
\maketitle

\begin{abstract}
	Um eine fehlerfreie Funktionalität der CPU, die in den vorherigen Berichten vorgestellt wurde, zu gewährleisten, sind Tests in allen Stadien der Entwicklung notwendig. Diese wurden sowohl auf Komponenten, als auch Systemebene umgesetzt und in diesem Bericht vorgestellt. Des weiteren wurde ein Simulator entwickelt, der es ermöglicht, Firmware, die später auf einem Prozessor, der diese CPU enthält, auszuführen und zu debuggen. Außerdem können alle Speicherzugriffe in eine Datei exportiert werden, die für die Tests auf Systemebene verwendet werden können. Zuletzt wurde ein minimalistischer Prozessor auf Basis dieser CPU entwickelt, der als Softcore auf auf einem Xilinx Spartan 7 FPGA läuft. Neben der CPU enthält dieser Prozessor dabei noch ein Speichermodul, das \ac{rom}, \ac{ram} und 4 LEDs im Adressraum bereit stellt, sowie eine Resetmodul.
\end{abstract}

\tableofcontents

\clearpage

\section*{Abkürzungsverzeichnis}

\begin{acronym}
	\acro{alu}[ALU]{Arithmetic Logic Unit}
	\acro{cli}[CLI]{Commandline-Interface}
	\acro{cpu}[CPU]{Central Processing Unit}
	\acro{fpga}[FPGA]{Field Programmable Gate Array}
	\acro{ram}[RAM]{Random Access Memory}
	\acro{rom}[ROM]{Read-Only Memory}
	\acro{uut}[UUT]{Unit-under-Test}
\end{acronym}

\mbox{}
\vfill

\doclicenseThis

\clearpage

\section{Motivation}

In früheren Praktikumsterminen wurde mit dem \textit{VISCY} ein einfacher Prozessor entwickelt. Dieser verwendet einen eigenen Befehlssatz. Während dieser sehr übersichtlich und somit vergleichsweise einfach zu implementieren ist, benötigt er allerdings eine eigene Toolchain, die momentan nur einen Assembler, aber keinen Compiler und Linker enthält, die zum Übersetzen einer Hochsprache wie \textit{C} notwendig sind.

Daher stellte sich die Frage, wie viel Mehraufwand es bedeutet, einen Prozessor zu implementieren, der einen Befehlssatz unterstützt, der mit einem Compiler wie z.B. \textit{GCC} kompatibel ist. Gewählt wurde der RV32I-Befehlssatz des RISC-V Projekts. Dieses definiert Befehlssätze für \SI{32}{\bit}, \SI{64}{\bit} und \SI{128}{\bit}-Prozessoren. Des weiteren gibt es diverse Erweiterung, z.B. für Multiplikation oder Gleitkommazahlen. Diese wurden hier bewusst ausgelassen, um einfaches System zu ermöglichen. Es gibt aber andere freie Implementierungen, die Erweiterungen unterstützen und gegebenenfalls auch mehrere Kerne verwenden, wie z.B. der an der Hochschule Augsburg entwickelte Paranut \cite{paranut} oder der mit dem Rocket Chip Generator \cite{rocketchip} der Universität Barkley erzeugte Prozessoren.

\section{Projektübersicht}

Ziel des Projektes war es, eine \ac{cpu} mit möglichst vollständiger Implementierung des RV32I-Befehlssatzes nach \cite{rv32spec} in VHDL zu implementieren. Die Befehle \textit{ebreak} und \textit{ecall} wurden nicht implementiert, weil nur eine kleine einkernige \ac{cpu} implementiert werden soll, auf der auch kein Betriebssystem laufen können muss. Für weitere Details siehe \cite{staib}. Außerdem wurde auf die Implementierung eines Debug-Controllers aus Zeitgründen verzichtet. Neben ausführlichen Testen mit Testbenches in einem VHDL-Simulator wurde auf Grundlage der \ac{cpu} eine Softcore auf einem \ac{fpga} implementiert. Dies bietet eine zusätzliche Möglichkeit der Verifikation und weißt zusätzlich die Synthetisierbarkeit der \ac{cpu} nach.

Das Projekt mit dem Namen \textit{RISCY}, entwickelt unter dem Projektnamen \textit{simple-rv32i}, besteht aus folgenden Komponenten:

\begin{itemize}
	\item \textbf{simple-rv32i} ist das Hauptprojekt, in dem auch die komplette \ac{cpu} sowie alle Tests und die meiste Dokumentation enthalten ist. Ein zentrales (Python-)Skript mit einem \ac{cli} bietet dabei die Möglichkeit, alle notwendigen Aufgaben auszuführen, z.B. Starten von Tests oder Generieren unterschiedlicher Dokumentationen. Das Hauptprojekt hat dabei folgende notwendige Abhängigkeiten: \textit{Python3}, einen vom \textit{VUnit}-Framework unterstützten Simulator (bevorzugt \textit{ghdl} mit \textit{gtkwave}), alle in \textit{requirements.txt} aufgeführten Python-Bibliotheken, die RISC-V GCC Toolchain \textit{riscv64-unknown-none-elf}, sowie \textit{simple-rv32i-simulator}.
	\item \textbf{simple-rv32i-simulator} enthält einen eigens für dieses Projekt entwickelten Simulator, der auch für die Gesamttests des Hauptprojekts, sowie Testen der Firmware benötigt wird. Eine detaillierte Beschreibung folgt in Abschnitt \ref{sec:sim}. Das Projekt hängt von der Rust-Toolchain, dessen Erweiterung \textit{cargo-deb}, sowie den in \textit{Cargo.toml} angegebenen Rust-Bibliotheken ab.
	\item \textbf{simple-rv32i-spartan-7} Implementiert einen Prozessor als Softcore auf Basis der \ac{cpu}. Der Softcore wird dabei auf einem Xilinx Spartan 7 ausgeführt. Als kombinierter \ac{ram}/\ac{rom}-Speicher dient dabei ein \SI{36}{\kilo\bit} großes Block-\ac{ram}, von dem \SI{4}{\mega\byte} verwendet werden können. Des weiteren können 4 LEDs angesteuert werden. Eine genauer Beschreibung folgt in Abschnitt \ref{sec:impl}. Es bestehen bis auf den Simulator dieselben Abhängigkeiten wie beim Hauptprojekt. Des weiteren wird Xilinx Vivado benötigt, inklusive der VHDL-Simulationsbibliotheken \textit{unisim} und \textit{unimacro}.
	\item \textbf{simple-rv32i-firmware} Hier sind unterschiedliche Programme enthalten, die entweder im Simulator oder auf dem Softcore ausgeführt werden können. Ein Programm kann beispielsweise die ersten 10 Zahlen der Fibonacci-Reihe berechnen. Um weitere Programme zu erstellen, kann eines der Projekte als Vorlage verwendet werden. Einzige Abhängigkeiten sind die Toolchain \textit{riscv64-unknown-none-elf} und \textit{make}.
\end{itemize}

\section{Simulator}
\label{sec:sim}

Es wurde ein eigener Simulator entwickelt, der \textit{elf} bzw. \textit{bin}-Daten ausführen kann und speziell auf das Memorylayout der Implementierung auf einem \ac{fpga} aus Abschnitt \ref{sec:impl}, sowie den umgesetzten Befehlssatz angepasst wurde. Zum einen dient der Simulator zum Debuggen und Testen der Firmware. Zum anderen kann er Tracedaten aller Speicherzugriffe beim Ausführen einer Firmware generieren. Folgende Daten werden zu jedem Speicherzugriff in die Tracedatei geschrieben:

\begin{enumerate}
	\item Speicheradresse
	\item Art des Zugriffs (Speicher/ Lesen)
	\item Die Datenbits (bzw. Don't Cares, wenn nicht alle 32 Bit verwendet wird)
\end{enumerate}

Die Tracedaten sind für die Systemtests in Abschnitt \ref{sec:tests} nötig.

Der Simulator lässt sich über ein \ac{cli} starten. Er führt das Programm so lange aus, bis er eine leere Dauerschleife erkennt oder ein Fehler auftritt. Alternativ kann die Anzahl der auszuführenden Instructions-Cycles angegeben werden. Zusätzlich verfügt der Simulator über einen interaktiven Debugging-Modus.

Implementiert wurde der Simulator in Rust mithilfe einer Bibliothek zum Simulieren von RV32GC-Prozessoren. Deshalb muss nach jedem Cycle überprüft werden, ob die aktuelle Instruction auch im durch diesen Prozessor unterstützen Befehlssatz enthalten ist. Das Rustprojekt bietet zudem die Möglichkeit, ein Debian-Paket zu erzeugen, sofern die Erweiterung \textit{cargo-deb} des Rust eigenen Buildsystems \textit{cargo}  installiert ist.

\section{Testumgebung}
\label{sec:tests}

Zum Testen der \ac{cpu} wird zwischen Komponenten-Tests und Gesamttests unterschieden. Beiden gemein ist, dass sie sich über ein gemeinsames Python-Skript mit einem \ac{cli} starten lassen. Beide verwenden dabei das Testframework \textit{VUnit}.

\subsection{VUnit}

\textit{VUnit} \cite{vunit} ist ein Open-Source Testframework für Komponenten-Tests. Zum einen stellt es eine, allerdings erst mit VHDL 2008 vollständig kompatible, VHDL-Bibliothek zum Testen und Logging zur Verfügung. Innerhalb der VHDL-typischen Testbenches, können mehrere Testcases definiert werden. Somit müssen für ein bestimmtes \ac{uut} nicht unbedingt mehrere Testbenches definiert werden. Zum anderen gibt es eine Pythonbibliothek, die sich in eine Konfigurations- und eine Ausführungsphase gliedern lässt, als zentralen Einstiegspunkt. Zunächst müssen alle notwendigen VHDL-Sourcen, also notwendige Packages, externe auch vorkompilierte Bibliotheken und Testbenches, registriert werden. Danach können optional für einzelne Testfälle mehrere Konfigurationen konfiguriert werden. Im wesentlichen handelt es sich für unterschiedliche Werte, die als \textit{Generics} der Testbench übergeben werden. Die Ausführungsphase wird über die \textit{main}-Methode eines Objekts der Hauptklasse der Bibliothek gestartet. Diese wertet die \ac{cli}-Parameter aus und sucht nach einem kompatiblen Simulator. Je nach \ac{cli}-Parametern werden die notwendigen Bibliotheken und Dateien kompiliert bzw. elaboriert und die ausgewählten Tests gestartet. Optional können die Signale der Testbench in der GUI des Simulators angezeigt werden. Außerdem ist es möglich einen Testreport im JUnit-Format zu erstellen.

\subsection{Komponenten-Tests}
\label{sec:comptest}

Alle Komponententests wurden mit Hilfe von VUnit erstellt, wobei Komponente und der dazugehörige Tests von unterschiedlichen Personen implementiert wurden. Für jede Komponente, mit Ausnahme mancher Multiplexer in der Gesamtstruktur der \ac{cpu}, wurde eine Testbench angelegt, die wiederum mehrere Testcases enthält. Z.B. gibt es in der Testbench für die \ac{alu} für jede einzelne Operation, wie z.B. \textit{add}, \textit{sub}, \textit{blt}, etc., einen eigenen Testcase. Da jeder einzelne Testcase unabhängig ausgeführt werden kann, ermöglichte dies häufig eine schnellere Fehlersuche. Eine Übersicht über die Anzahl der Tests je Komponente gibt Tabelle \ref{tab:tccount}.

\begin{table}[h!]
	\centering
	\caption{Anzahl der Testcases je Komponente}
	\label{tab:tccount}
	\begin{tabular}{|c  | c |} 
		\hline
		Komponente & Anzahl der Testcases \\ [0.5ex] 
		\hline\hline
		Regfile & 3\\
		\hline 
		Instruction register & 3\\
		\hline
		Controller & 17\\
		\hline
		Add4 & 3 \\
		\hline
		Program counter & 3 \\
		\hline
		"Regsortmux" & 7 \\
		\hline
		\ac{alu} & 36 \\
		\hline\hline
		insgesamt & 72 \\
		\hline
	\end{tabular}
\end{table}

\subsection{Gesamttests}
\label{sec:systests}

Auch die vollständige \ac{cpu} soll im Simulator getestet werden, wobei möglichst viele Funktionalitäten abgedeckt werden sollen. Das Konzept aus \ref{sec:comptest}, alle Tests per Hand zu implementieren wurde aus mehreren Gründen als ungeeignet betrachtet: Alle Stimuli und Checks per Hand zu entwerfen und in einer VHDL-Testbench zu implementieren, wäre vergleichsweise aufwendig, zum anderen aber auch fehleranfällig. 

Stattdessen wurde folgendes Konzept entwickelt: Es werden mehrere Assemblerprogramme entworfen, dessen korrekte Ausführung im VHDL-Simulator anhand aller zu erwartenden Speicherzugriffe überprüft werden soll. Welche Speicherzugriffe zu erwarten sind, wird dabei idealerweise nicht "`per Hand"' vom Testentwickler bestimmt, sondern durch einen Simulator, der für dieses Projekt entwickelt und in Abschnitt \ref{sec:sim} beschrieben wurde. Um ein System zu erhalten, das Firmware ausführen kann, wird neben der \ac{cpu} noch ein Speicher benötigt. Deshalb wurde in VHDL eine eigene Speicherkomponente entwickelt, dessen Inhalt sich mit einer speziellen Textdatei initialisieren lassen, wobei in der ersten Zeile die Anzahl der Bytes, die in der Textdatei enthalten sind, steht, in den folgenden Zeilen jeweils ein Byte, dessen Wert binär mit den ASCII-Zeichen \textit{0} bzw. \textit{1} notiert ist. Der Speicher ist funktional kompatibel mit dem Speicher der Beispielimplementierung, der in Abschnitt \ref{sec:fpgamem} beschrieben ist und mit derselben Testbench getestet wurde. Aus Performancegründen und um vendorspezifische Abhängigkeiten im Hauptprojekt zu vermeiden, wurde diese Verifikationskomponente entwickelt, statt die Speicherkomponente des \ac{fpga}-Projekts zu verwenden.

Die Ausführung eines oder mehrerer Tests läuft dabei durch ein Pythonskript gesteuert folgendermaßen ab, wie in Abbildung \ref{fig:systest} skizziert ist:

\begin{figure}[h]
	\centering
	\includesvg[width=0.9\textwidth]{testconcept}
	\caption{Ablauf der Simulation}
	\label{fig:systest}
\end{figure}

\begin{enumerate}
	\item Zunächst wird aus den Assemblerprogrammen mithilfe der \textit{riscv64-unkown-none-elf}-Toolchain Maschinencode erzeugt, der sowohl als \textit{ELF}-Datei, als auch als rohe Binärdatei gespeichert wird. Die Binärdatei wird anschließend durch das Pythonskript noch in die für das Testmemory notwendige Textformat konvertiert.
	\item Anschließend werden die \textit{ELF}-Dateien jeweiels durch den Simulator ausgeführt, wobei dieser alle Speicherzugriffe in eine Textdatei loggt. Der Simulator stoppt, sobald er eine leere Dauerschleife findet.
	\item Nun wird die Testbench für das Gesamtsystem durch VUnit compiliert und gestartet. In der Testbench wird solange auf den nächsten Speicherzugriff gewartet und dieser sofort anhand der Logdatei überprüft, bis alle Einträge der Logdatei abgearbeitet sind. Bei einem falschen Speicherzugriff läuft der Simulator zwar zunächst weiter, es wird allerdings der Fehler ausgegeben und der Hauptprozessor von VUnit wird bei Beendigung trotzdem einen Fehlercode zurückgeben
	\item Zuletzt wird, falls per \ac{cli} ausgewählt, das GUI des Simulators gestartet. Damit kann auch eine zusätzliche Kontrolle von Hand stattfinden oder ein Fehler gesucht werden.
\end{enumerate}

Alle Assemblerprogramme wurden dabei zusätzlich manuell im Simulator auf das korrekte Verhalten überprüft.

\section{Verwendung als Softcore auf einem Spartan 7 FPGA}
\label{sec:impl}

Zuletzt soll nachgewiesen werden, dass die \ac{cpu} auch in Hardware funktioniert. Dazu wurde ein Xilinx Spartan 7 \ac{fpga} auf einem Digilent Cmod S7 Board \cite{s7data} gewählt. Zum Generieren der Bitstreamdatei und zum flashen dieser Datei in das \ac{rom} des \ac{fpga}s, wurde die Entwicklungsumgebung Xilinx Vivado gewählt. Die Projektdatei für die IDE kann dabei durch das Python-Wrapperskript des \textit{simple-rv32i-spartan-7} erzeugt werden.

\subsection{Systemübersicht}

Das Gesamtsystem, wie in Abbilung \ref{fig:s7sys} zu sehen, enthält neben der \ac{cpu} noch ein kombinierten \ac{ram}/\ac{rom}-Speicher mit synchronem Wishboneclientinterface. Ebenfalls in der Speicherkomponente ist das Interface zu den vier Ausgängen zu den LEDs enthalten. Des weiteren sind mehrere Signale der \ac{cpu} auf digitale Ausgänge des \ac{fpga} geführt, um ein Debugging direkt auf der Hardware zu ermöglichen. Ein Mitschnitt eines Programms, dass aus einer minimalen Startup-Sequenz besteht, anschließend ein einzeles Word in das \ac{ram} schreibt und schließlich in einer leeren Dauerschleife verweilt, ist in Abbildung \ref{fig:oszitrace} zu sehen. Zur Messung wurde ein Mixed-Signal Oszilloskop verwendet. Da dieses nur über 16 digitale Eingänge verfügt, wurden bei Adresse und Daten nur die jeweils untersten 4 Bit aufgezeichnet, das Reset-Signal aus demselben Grund mit einem analogen Kanal.

\begin{figure}[h]
	\centering
	\includesvg[width=0.9\textwidth]{fpgadesign}
	\caption{Blockschaltbild des Systems}
	\label{fig:s7sys}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{simple_load_store_w_inv}
	\caption{Test des Prozessors mit einem Mixed-Signal-Oszilloskop}
	\label{fig:oszitrace}
\end{figure}

\subsubsection{Speicher und LEDs}
\label{sec:fpgamem}

Das System verfügt über einen \SI{4}{\kilo\byte} großen Speicher, der sowohl die Funktionalität eines \ac{ram}s, als auch eines \ac{rom}s übernimmt. Dazu wird ein \SI{36}{\kilo\bit} Block-\ac{ram}, wie es in allen Xilinx 7-Serie \acp{fpga} enthalten ist, verwendet. Vorteil dieses \ac{ram}s ist, dass es beim Start des \ac{fpga}s automatisch mit Daten befüllen lässt. Da dieses Xilinx-Blockram selbst nicht über ein Wishboneinterface und zudem nur \SI{32}{\bit} Adressierung verfügt, wurde das Wishboneinterface mithilfe eines Zustandsautomaten implementiert.

Direkt oberhalb des Adressbereichs des Speichers können zudem noch 32 digitale Ausgänge geschrieben und die zuletzt geschriebenen Werte ausgelesen werden. Allerdings sind nur 4 der 32 Ausgänge in Verwendung und mit den 4 grünen LEDs des Cmod S7-Board verbunden.

Damit Daten wie der Maschinencode oder Konstanten in den Speicher gelangen, wird die entsprechende VHDL-Datei durch ein Pythonskript generiert. Dieses liest dazu die von der \textit{riscv64-unknown-none-elf} generierte Binärdatei ein.

Um die Funktionalität der Speicherkomponente mit dem Wishboneinterface sicherzustellen, wird diese mit einer Testbench getestet. Die Testbench ist dabei im wesentlichen dieselbe, die auch den virtuellen Speicher aus Abschnitt \ref{sec:systests} überprüft, allerdings um ein paar zusätzliche Tests erweitert. Auch hier wird, wie die Tests in Abschnitt \ref{sec:comptest}, auf Basis des \textit{VUnit}-Frameworks entwickelt. Allerdings macht das Block-\ac{ram} ein paar Anpassungen notwendig: Bei der Implementierung des Speichers für den Spartan 7 wurde ein Xilinx-spezifisches Makro verwendet. Um dieses zu simulieren, sind die beiden Xilinx-Bibliotheken \textit{Unisim} und \textit{Unimacro} notwendig. Diese werden in der Konfigurationsphase der Simulation als externe Bibliotheken über das Pythoninterface von VUnit registriert. Xilinx liefert mit Vivado allerdings nur die Sourcen dieser beiden Bibliotheken aus. In Vivado gibt es die Möglichkeit diese Bibliotheken zur Verwendung in Simulatoren vorzubereiten. Allerdings betrifft das nur kommerzielle Produkte wie z.B. ModelSim oder Riviera Pro. Um für GHDL eine vorkompilierte Bibliothek zu erzeugen, enthält dessen Git-Repository für diese Aufgabe Bash-Skripte. Die Debian- und auch Ubuntu-Packetquellen enthalten allerdings nur eine abgespeckte Version, die zum einen diese Skripte nicht enthält. Zum anderen ist die IEEE-Bibliothek nur in eine abgespeckten Variante enthalten, wie in \cite{vitalentities} beschrieben. Installiert man sich nur die zum Kompilieren notwendigen Skripte aus dem Git-Repository nach, scheitert der Build-Prozess aufgrund dieser Einschränkungen. Deshalb wurde GHDL in Version 2.0 direkt von seinen Quellen installiert, was eine Verwendung der Xilinx-Makro in der Simulation ermöglichte.

\subsubsection{CPU}

Die \ac{cpu}, als Kernstück des Projekts, wurde bereits in \cite{celep} behandelt. Sie besitzt eine Wishbone Masterinterface, mit dem sie den Speicher anspricht.

\subsubsection{Reset und Clock}

Das Cmod-S7 Board enthält eine \SI{12}{\mega\hertz}-Clock. Um auch Debugging per Hand über die LEDs statt eines Logikanalysators zu ermöglichen, ist ein Clockdevider enthalten. Dieser kann, konfiguriert über einen generischen Parameter alle durch \eqref{eq:clk} beschriebenen Frequenzen erzeugen:

\begin{equation}\label{eq:clk}
	f_\mathrm{div}=\frac{\SI{12}{\mega\hertz}}{2^\mathrm{N}}, N\in\mathbb{N}
\end{equation}

Dieser mit Hilfe eines Zählers erzeugte Addierer findet im aktuellen Zustand des Projekts keine Verwendung, lässt sich bei Bedarf aber wieder einkommentieren.

Außerdem ist noch ein Reset-Generator enthalten. Dieser erzeugt ein synchrones Resetsignal (bezogen auf die langsame Clock, wenn der Frequenzteiler verwendet wurde), wenn der FPGA startet oder, wenn ein bestimmter Knopf auf dem \ac{fpga}-Board gedrückt wird.

\subsection{Timing und Ressourcenverbrauch}

Da das Cmod-S7 Board nur mit \SI{12}{\mega\hertz} betrieben wird, werden vergleichsweise geringe Anforderungen an das Timing gestellt. Eine Methode, um inkrementell das maximal mögliche Timing zu bestimmen, ist in \cite[S.207-215]{ug949} genauer beschrieben. Für den Prozessor wurde nur eine grobe Abschätzung durchgeführt. Dazu wurde im Vivado Projekt vorübergehend jeweils \SI{50}{\mega\hertz} bzw. \SI{100}{\mega\hertz} aus Taktfrequenz vorgegeben. Während es bei letzterem zu Timingverletzungen kam, war ersteres möglich. Die schlechtesten Timingparameter für \SI{12}{\mega\hertz} und \SI{50}{\mega\hertz} sind in Tabelle \ref{tab:timing} aufgeführt. Dabei ist zu sehen, dass jeweils sehr unterschiedliche Ergebnisse entstanden, was vermuten lässt, dass die maximale Taktfrequenz bei der Optimierung des Designs in Vivado beachtet wird.

\begin{table}[h]
	\centering
	\caption{Timing des Designs bei unterschiedlichen Zieltaktraten}
	\label{tab:timing}
	\begin{tabular}{| c | c | c |}
		\hline
		Schlechtester Wert & \SI{12}{\mega\hertz} & \SI{50}{\mega\hertz} \\
		\hline\hline
		Setup Slack & \SI{58,611}{\nano\second} & \SI{3,668}{\nano\second} \\
		\hline
		Hold Slack & \SI{0,071}{\nano\second} & \SI{0,039}{\nano\second} \\
		\hline
		Pulse Width Slack & \SI{40,410}{\nano\second} & \SI{8,750}{\nano\second} \\
		\hline
	\end{tabular}
\end{table}

Neben dem Timing ist auch der Platzverbrauch des Designs interessant. Alle verwendeten Ressourcen sind in Tabelle \ref{tab:usedres} aufgeführt und mit den Ressourcenverbrauch des VISCY Prozessors, den der Autor dieses Berichts ebenfalls auf einem Spartan 7 als Softcore implementiert hatte, verglichen.

\begin{table}[h]
	\centering
	\caption{Benötigte Ressourcen}
	\label{tab:usedres}
	\begin{tabular}{| c | c | c |}
		\hline
		Ressource & Anzahl VISCY & Anzahl RISCY \\
		\hline\hline
		LUT & 191 & 1335 \\
		\hline
		LUT\ac{ram} & 64 & 44 \\
		\hline
		FF & 54 & 108 \\
		\hline
		B\ac{ram} & 1 & 1 \\
		\hline
		IO & 6 & 23 \\
		\hline
		BUFG & 2 & 1 \\
		\hline
	\end{tabular}
\end{table}

\section{Fazit}

Tatsächlich war es möglich, eine RISC-V \ac{cpu} zu entwickeln, die fast alle Befehle des minimalen Befehlssatz RV32I unterstützt und auch in der Programmiersprache \textit{C} geschriebene und mit einer unmodifizierten \textit{riscv64-unknown-none-elf} Toolchain übersetzten Programme auszuführen. Allerdings sind die \ac{cpu} und auch der gesamte Prozessor in ihrer jetzigen Form nicht durch Parameter anpassbar, um sie innerhalb ihres Einsatzgebiets als platzsparender Softcore auf bestimmte Parameter, wie Platz, Geschwindigkeit oder bestimmter zusätzlicher Funktionalität, hin zu optimieren. Folgende weitere Verbesserungen sind denkbar:

\begin{itemize}
	\item Eine genaue Analyse, welche Komponenten wie viele Ressourcen benötigen, steht noch aus. Anhand dieser Analyse könnten gezielte Optimierungen umgesetzt werden.
	\item Das Abarbeiten einzelner Befehle benötigt bei optimal schnellem Speicher, zwischen 4 und 6 Taktzyklen, ein Speicherzugriff allerdings nur zwei, wenn mehr als \SI{1}{\byte} am Stück gelesen wird, alle weiteren Bytes sogar nur einen. Sieht man von den Befehlen ab, die selbst auf den Speicher zugreifen können, könnte durch den Einsatz einer Pipeline die Geschwindigkeit verbessert werden, allerdings zu Lasten des geringen Platzbedarfs.
	\item Bisher ist kein Debugcontroller enthalten. Dieser wäre, wiederum auf Kosten des Platzbedarfs, sinnvoll, um den Firmwareentwicklern die Entwicklung zu erleichtern.
	\item Je nach Einsatzgebiet des Prozessors ist auch eine optionale Erweiterung des Befehlssatz um die ebenfalls in \cite{rv32spec} spezifizierte Erweiterung \textit{M} zur Multiplikation und Division von Ganzzahlen sinnvoll.
	\item Insbesondere für die Gesamttests gibt es noch erhebliches Verbesserungspotential. Bisher basieren alle Tests auf selbst geschriebenen Assemblerprogrammen. Um die Wahrscheinlichkeit von Fehlern zu reduzieren, sollte allerdings immer, wenn möglich, Software eingesetzt werden, die eine weite Verbreitung hat. Ein passendes Projekt, das eine Reihe von Gesamttests bereitstellt, ist \cite{riscv-isa-test}
	\item Bezogen auf die hier beschriebene Beispielimplementierung ist zwar denkbar, statt eines einzelnen Blockrams, mehrere zu verwenden, um die Größe des zur Verfügung stehenden Speichers zu erhöhen. Allerdings sind diese auch eine nur begrenzt verfügbare Ressource. Alternativ könnte der Flashspeicher, aus dem der hier verwendete \ac{fpga} seine Konfigurationsdaten liest, auch zusätzlich als \ac{rom} für den Prozessor dienen. Neben dem erhöhten Platzbedarf gilt es hier auch die Geschwindigkeit und Latenz des Flash-Speichers zu betrachten, was wahrscheinlich ein zusätzliches Caching notwendig machen könnte.
	\item Ebenfalls für die Beispielimplementierung wären zusätzliche Register für z.B. GPIO denkbar.
\end{itemize}

Zuletzt soll auch ein Vergleich mit dem eingangs erwähnten \textit{VISCY} nicht fehlen: Die grundlegende Struktur des VISCY konnte beibehalten werden, allerdings machte der umfangreichere RV32I-Befehlssatz ein paar zusätzliche Multiplexer notwendig, insbesondere durch die meist relative Adressierung in den Befehlen und den Immediate-Befehlen, zu denen es kein Äquivalent im VISCY gibt. Insbesondere \ac{alu}, Controller und die Gesamtstruktur deutlich aufwendiger zu implementieren und testen, was sich auf ihren erhöhten Funktionsumfang zurückführen lässt. Während der VISCY mit seinem geringeren Funktionsumfang einen deutlich geringeren Ressourcenverbrauch aufweist, hat er gleichzeitig einen entscheidenden Nachteil: Es gibt zwar ebenfalls einen Simulator und Assembler für dessen Befehlssatz, allerdings fehlt ihm als rein didaktisches Projekt die Unterstützung durch eines der großen Compilerprojekte, wie z.B. GCC. Auf diese wiederum kann der hier entwickelte Prozessor durch die Verwendung des verbreiteten RV32I-Befehlssatzes zurückgreifen. Er ist somit einem tatsächlich verwendbaren Softcore näher gekommen. Allerdings besteht noch weiterer Verbesserungsbedarf, sowohl, was die Funktionalität, als auch die Testabdeckung betrifft, wenn man den RISCY in einem größeren Projekt als Softcore einsetzen wollte.

\clearpage

\printbibliography

\end{document}