library vunit_lib;
context vunit_lib.vunit_context;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use std.textio.all;
use work.verificationutils.all;


entity TB_RUNFWBIN is
    generic (
        runner_cfg : string;
        loadfile: string;
        checkfile: string
    );
end entity;


architecture TB of TB_RUNFWBIN is
    signal clk, rst: std_logic := '0';
    signal wb_we, wb_stb, wb_cyc, wb_ack: std_logic;
    signal adr, miso, mosi: std_logic_vector(31 downto 0);
    signal wb_sel : std_logic_vector(3 downto 0);
    -- check helpers
    signal is_wb_read, is_wb_write: std_logic;
begin
    is_wb_read <= (not wb_we) and wb_stb;
    is_wb_write <= wb_we and wb_stb;
    clk <= not clk after CLK_PERIOD / 2;

    uut : entity work.RISCY_CPU port map(
        clk => clk,
        reset => rst,
        adr => adr,
        data_i => miso,
        data_o => mosi,
        we => wb_we,
        sel => wb_sel,
        stb => wb_stb,
        cyc => wb_cyc,
        ack => wb_ack
    );

    vcmem : entity work.vcwbmemory generic map(
        loadfile => loadfile
    )
    port map(
        rst => rst,
        clk => clk,
        adr => adr,
        data_i => mosi,
        data_o => miso,
        we => wb_we,
        sel => wb_sel,
        stb => wb_stb,
        ack => wb_ack,
        cyc => wb_cyc
    );

    main: process
        procedure check_mem_rd_adr(
            instruction_adr: in natural
        ) is
        begin
            wait_on_clk_timeout(clk, is_wb_read, '1', 50);
            wait until falling_edge(clk);
            check(
                adr = std_logic_vector(to_unsigned(instruction_adr, 32)),
                "wrong read address, expected " & integer'image(instruction_adr) &
                ", is " & integer'image(to_integer(unsigned(adr)))
            );
            wait_on_clk_timeout(clk, wb_cyc, '0', 10);
            wait for 0.1 ns;
        end procedure;

        procedure check_mem_rd(
            instruction_adr: in natural;
            data: in std_logic_vector(31 downto 0)
        ) is
            variable check_ok: boolean := True;
        begin
            wait_on_clk_timeout(clk, is_wb_read, '1', 50);
            wait until falling_edge(clk);
            check(
                adr = std_logic_vector(to_unsigned(instruction_adr, 32)),
                "wrong read address, expected " & integer'image(instruction_adr) &
                ", is " & integer'image(to_integer(unsigned(adr)))
            );
            wait_on_clk_timeout(clk, wb_ack, '1', 10);
            check_ok := True;
            for idx in 0 to 31 loop
                if not (data(idx) = 'X') then
                    if data(idx) /= miso(idx) then
                        check_ok := False;
                    end if;
                end if;
            end loop;
            check(check_ok, "wrong read data, is """ & std_logic_vector2bin(miso) &
                """, but should be """ & std_logic_vector2bin(data) & """!");
            wait_on_clk_timeout(clk, wb_cyc, '0', 10);
            wait for 0.1 ns;
        end procedure;

        procedure check_mem_wr(
            instruction_adr: in natural;
            data: in std_logic_vector(31 downto 0)
        ) is
            variable check_ok: boolean := True;
        begin
            wait_on_clk_timeout(clk, is_wb_write, '1', 50);
            wait until falling_edge(clk);
            check(
                adr = std_logic_vector(to_unsigned(instruction_adr, 32)),
                "wrong write address, expected " & integer'image(instruction_adr) &
                ", is " & integer'image(to_integer(unsigned(adr)))
            );
            wait_on_clk_timeout(clk, wb_ack, '1', 10);
            check_ok := True;
            for idx in 0 to 31 loop
                if not (data(idx) = 'X') then
                    if data(idx) /= mosi(idx) then
                        check_ok := False;
                    end if;
                end if;
            end loop;
            check(check_ok, "wrong write data, is """ & std_logic_vector2bin(mosi) &
                """, but should be """ & std_logic_vector2bin(data) & """!");    
            wait_on_clk_timeout(clk, wb_cyc, '0', 10);
            wait for 0.1 ns;
        end procedure;

        file checkdatafile : text open read_mode is checkfile;
        variable accessline : line;
        variable read_ok : boolean;
        variable whitespace : character;
        variable checkadr : std_logic_vector(31 downto 0);
        variable iswrite : boolean;
        variable checkdata : std_logic_vector(31 downto 0);
    begin
        test_runner_setup(runner, runner_cfg);
        set_stop_level(failure);
        while test_suite loop
            rst <= '1';
            wait_clk_cycles(clk, 1);
            rst <= '0';
            wait_clk_cycles(clk, 0);
            if run("check_firmware") then
                readline(checkdatafile, accessline);
                memaccess_loop: while not endfile(checkdatafile) loop
                    readline(checkdatafile, accessline);
                    hread(accessline, checkadr, read_ok);
                    check(read_ok, "ill formated address in checkfile");
                    -- ship whitespaces
                    while accessline'length > 0
                        and (accessline(accessline'left) = ' '
                            or accessline(accessline'left) = HT) loop
                        read(accessline, whitespace);
                    end loop;
                    read(accessline, iswrite, read_ok);
                    check(read_ok, "ill formated iswrite in checkfile");
                    while accessline'length > 0
                        and (accessline(accessline'left) = ' '
                            or accessline(accessline'left) = HT) loop
                        read(accessline, whitespace);
                    end loop;
                    hread(accessline, checkdata, read_ok);
                    check(read_ok, "ill formated checkdata in checkfile");
                    if iswrite then
                        check_mem_wr(to_integer(unsigned(checkadr)), checkdata);
                    else
                        check_mem_rd(to_integer(unsigned(checkadr)), checkdata);
                    end if;
                end loop memaccess_loop;
                wait_clk_cycles(clk, 20);
            end if;
        end loop;
        test_runner_cleanup(runner);
    end process;
end;