puts "cleaning view"
set displaylist [ gtkwave::getDisplayedSignals ]
puts "$displaylist"
set num_deleted [ gtkwave::deleteSignalsFromListIncludingDuplicates $displaylist ]
puts "setting up view"
set label_stimuli [list]
lappend label_stimuli "stimuli"
set num_added [ gtkwave::addCommentTracesFromList $label_stimuli ]
set stimuli_traces [list]
lappend stimuli_traces "top.tb_runfwbin.clk"
lappend stimuli_traces "top.tb_runfwbin.rst"
lappend stimuli_traces "top.tb_runfwbin.adr"
lappend stimuli_traces "top.tb_runfwbin.miso"
lappend stimuli_traces "top.tb_runfwbin.mosi"
lappend stimuli_traces "top.tb_runfwbin.wb_sel"
lappend stimuli_traces "top.tb_runfwbin.wb_cyc"
lappend stimuli_traces "top.tb_runfwbin.w_stb"
lappend stimuli_traces "top.tb_runfwbin.wb_we"
lappend stimuli_traces "top.tb_runfwbin.wb_ack"
set num_added [ gtkwave::addSignalsFromList $stimuli_traces ]

set label_cpu_basic [list]
lappend label_cpu_basic "cpu basic"
set num_added [ gtkwave::addCommentTracesFromList $label_cpu_basic ]
set cpu_basic_traces [list]
lappend cpu_basic_traces "top.tb_runfwbin.uut.impl_ir.instruction_registers"
lappend cpu_basic_traces "top.tb_runfwbin.uut.impl_pc.add_cur"
lappend cpu_basic_traces "top.tb_runfwbin.uut.impl_controller.curr_ctrl_state"
lappend cpu_basic_traces "top.tb_runfwbin.uut.impl_controller.next_ctrl_state"
for {set x 1} {$x<=32} {incr x} {
    lappend cpu_basic_traces "top.tb_runfwbin.uut.impl_regfile.reg\[$x\]"
}
set num_added [ gtkwave::addSignalsFromList $cpu_basic_traces ]

puts "unzooming time"
set max_time [ gtkwave::getMaxTime ]
puts "$max_time"
gtkwave::setZoomRangeTimes 0 $max_time